# WINDOWS用ゲームソフトウェア「FLAPPY WORLD」の移植

## 開発環境
### OS
Windows10,Linux(Ubuntu22.04)
### ゲームエンジン
Unity
### エディタ
Visual Studio, Visual Studio Code

[公式リンク](https://flappy.netfarm.ne.jp/)

## 著作権関連
[公式のガイドラインを参照してください](https://flappy.netfarm.ne.jp/guidline.htm)

## 開発情報
パズル画面のメニューのUIは未完成。ゲームの速度を変更できるようにする予定。

## 操作方法
・十字キー...上下左右の移動  

・Rキー...ステージのやり直し
・Pキー...メニュー画面を呼び出す
