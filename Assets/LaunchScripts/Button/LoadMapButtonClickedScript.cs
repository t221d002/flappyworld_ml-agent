using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadMapButtonClickedScript : MonoBehaviour
{
    public void OnClickLoadMapButton()
    {
        SceneManager.LoadScene("LoadMapListScene");
    }
}