using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateStageButtonClickedScript : MonoBehaviour
{
    public void OnClickCreateButton()
    {
        CanvasObjectManager_Launch.CheckCreateStageDialogContainer.SetActive(true);
    }
}
