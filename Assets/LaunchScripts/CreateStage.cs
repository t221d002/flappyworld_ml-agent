using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;
using UnityEngine.UI;
using TMPro;


public class CreateStage : MonoBehaviour
{
    public static void CreateStageFile()
    {
        GameObject objInputFieldText = CanvasObjectManager_Launch.InputStageFileNameDialogueContainer.transform.Find("InputField").gameObject;
        string mapFolderPath = "Assets/Field Parts/LoadingScenePatrs/StageTextFiles";
        string inputFieldText = objInputFieldText.transform.GetChild(0).transform.Find("Text").GetComponent<TextMeshProUGUI>().text;
        string mapFilePath = mapFolderPath + "/" + inputFieldText + ".txt";

        try
        {
            using (StreamWriter writer = new StreamWriter(mapFilePath, false))
            {
                writer.WriteLine("//stage-Name");
                writer.WriteLine("//stage-Size_9_9");
                writer.WriteLine("//comment");

                for (int n = 3; n < 12; n++)
                {
                    if (n == 3 || n == 11)
                        writer.WriteLine("@@@@@@@@@");
                    else
                        writer.WriteLine("@.......@");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }
}
