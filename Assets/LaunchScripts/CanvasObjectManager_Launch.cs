using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;
using LoadedData;

public class CanvasObjectManager_Launch : MonoBehaviour
{
    public static GameObject CheckCreateStageDialogContainer;
    public static GameObject InputStageFileNameDialogueContainer;
    public static GameObject InformCreatedStageFileDialogContainer;
    private string mlAgent_LoadSettingPass;
    private bool mlAgentsMode = false;
    private bool stageMode = false;
    private bool mapMode = false;
    private string loadFileName;
    public static List<string> stageNameList;
    public static TransitionManager transitionManager = new TransitionManager();

    void Start()
    {
        CheckCreateStageDialogContainer = GameObject.FindWithTag("Check");
        InputStageFileNameDialogueContainer = GameObject.FindWithTag("Input");
        CheckCreateStageDialogContainer.SetActive(false);
        InputStageFileNameDialogueContainer.SetActive(false);
        InformCreatedStageFileDialogContainer = GameObject.FindWithTag("Complete");
        InformCreatedStageFileDialogContainer.SetActive(false);

        mlAgent_LoadSettingPass = "Assets/LoadStageData_for_ml-agent/loadsettings.conf";
        CheckLoadSettings();
    }

    void CheckLoadSettings()
    {
        try
        {
            using (StreamReader sr = new StreamReader(mlAgent_LoadSettingPass, Encoding.GetEncoding("utf-8")))
            {
                int processStatus = 0;//処理の状況を表す。loadsettings.confに記述された3つの設定項目を読み込んで処理を行う。
                while (0 <= sr.Peek())
                {
                    string lineText = sr.ReadLine();
                    if (lineText.Contains("#"))
                        lineText = sr.ReadLine();

                    if (lineText.Contains("ml-agentsMode"))
                    {
                        processStatus++;
                        if (lineText.Contains("on"))
                            mlAgentsMode = true;
                        else if (lineText.Contains("off"))
                            mlAgentsMode = false;
                        else
                            return;
                    }
                    if (lineText.Contains("loadMode"))
                    {
                        processStatus++;
                        if (lineText.Contains("map"))
                            mapMode = true;
                        else if (lineText.Contains("stage"))
                            stageMode = true;
                        else
                            return;
                    }
                    if (lineText.Contains("loadFileName") && CheckFileNameContainsInLine(lineText))
                    {
                        string commonPass = "Assets/Field Parts/LoadingScenePatrs";
                        string fileNamePass = commonPass + "/LoadingScenePatrs";
                        processStatus++;

                        if (mapMode)
                        {
                            fileNamePass = commonPass + "/MapFiles";
                            fileNamePass += "/" + loadFileName + ".map";
                        }
                        else if (stageMode)
                        {
                            fileNamePass = commonPass + "/StageTextFiles";
                            fileNamePass += "/" + loadFileName + ".txt";
                        }

                       if (File.Exists(fileNamePass))
                        {
                            if (mapMode)
                            {
                                List<string> textsLineList = new List<string>();
                                try
                                {
                                    using (StreamReader sr2 = new StreamReader(fileNamePass, Encoding.GetEncoding("utf-8")))
                                    {
                                        while (0 <= sr2.Peek())
                                        {
                                            textsLineList.Add(sr2.ReadLine());
                                        }
                                    }
                                }
                                catch { }

                                stageNameList = textsLineList;
                                LoadedData.StageFileLoader.ReadStageDataFile("Assets/Field Parts/LoadingScenePatrs/StageTextFiles"
                                + "/" + stageNameList[0] + ".txt");

                                transitionManager.MapStageList = textsLineList;
                                transitionManager.enteredFromMapLoad = true;
                                transitionManager.mapStageCount = 0;
                            }
                            else if (stageMode)
                            {
                                StageFileLoader.ReadStageDataFile(fileNamePass);
                            }
                            SceneManager.LoadScene("PlayLoadedStageScene");
                        }

                        //ここの処理を記述。名前のフォルダが存在するか判別。
                    }
                    if (processStatus == 3)
                        return;

                }
            }
        }
        catch (Exception ex)
        {
            return;
        }

    }

    bool CheckFileNameContainsInLine(string filePass)
    {
        var Matches = new Regex(@"\{(.+?)\}").Matches(filePass);
        loadFileName = Matches[0].Value;
        loadFileName = loadFileName.Trim('{', '}');

        if (loadFileName == null)
            return false;
        else 
            return true;
    }
}
