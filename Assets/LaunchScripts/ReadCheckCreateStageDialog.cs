using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ReadCheckCreateStageDialog : MonoBehaviour
{
    public enum DialogResult
    {
        OK,
        NO,
    }

    // ダイアログが操作されたときに発生するイベント
    public Action<DialogResult> FixDialog { get; set; }

    // OKボタンが押されたとき
    public void OnOk()
    {
        this.FixDialog?.Invoke(DialogResult.OK);
        CanvasObjectManager_Launch.CheckCreateStageDialogContainer.SetActive(false);
        CanvasObjectManager_Launch.InputStageFileNameDialogueContainer.SetActive(true);
    }
    public void OnNo()
    {
        this.FixDialog?.Invoke(DialogResult.NO);
        CanvasObjectManager_Launch.CheckCreateStageDialogContainer.SetActive(false);
    }

}
