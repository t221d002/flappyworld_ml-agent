using LoadedData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TMPro;
using UnityEngine;

public class ReadInputStageFileNameDialogue : MonoBehaviour
{
    public enum DialogResult
    {
        Cancel,
        OK,
    }

    // ダイアログが操作されたときに発生するイベント
    public Action<DialogResult> FixDialog { get; set; }

    public void OnClose()
    {
        this.FixDialog?.Invoke(DialogResult.Cancel);
        CanvasObjectManager_Launch.InputStageFileNameDialogueContainer.SetActive(false);

    }
    public void OnOK()
    {
        this.FixDialog?.Invoke(DialogResult.OK);
        CreateStage.CreateStageFile();
        CanvasObjectManager_Launch.InputStageFileNameDialogueContainer.SetActive(false);
        CanvasObjectManager_Launch.InformCreatedStageFileDialogContainer.SetActive(true);
    }

}
