using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadInformCreatedStageFileDialog : MonoBehaviour
{
    public enum DialogResult
    {
        OK,
    }

    // ダイアログが操作されたときに発生するイベント
    public Action<DialogResult> FixDialog { get; set; }

    // OKボタンが押されたとき
    public void OnOk()
    {
        this.FixDialog?.Invoke(DialogResult.OK);
        CanvasObjectManager_Launch.InformCreatedStageFileDialogContainer.SetActive(false);
    }
}
