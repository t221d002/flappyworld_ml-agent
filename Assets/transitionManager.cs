using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionManager
{
    public bool enteredFromMapLoad { get; set; } = false;
    public List<string> MapStageList { get; set; }
    public int mapStageCount { get; set; } = 0;
}
