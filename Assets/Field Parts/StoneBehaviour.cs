using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneBehaviour : MonoBehaviour
{
    Animator animator;
    [SerializeField]
    public bool breakingProcess1st;
    public bool breakingProcess2nd;
    public bool breakingProcess3rd;
    public bool breakingProcessFinal;
    public bool fallingStatus = false;
    public bool breakingNotFallFlag = false;

    public void BreakingProcess(GameObject go)
    {

        animator = go.GetComponent<Animator>();
        if (!animator.GetBool("c_sw1") && !animator.GetBool("c_sw2") && !animator.GetBool("c_sw3"))
        {
            animator.SetBool("c_sw1", true);
            breakingProcess1st = true;
        }
        else if (animator.GetBool("c_sw1"))
        {
            animator.SetBool("c_sw1", false);
            animator.SetBool("c_sw2", true);
            breakingProcess2nd = true;
        }
        else if (animator.GetBool("c_sw2"))
        {
            animator.SetBool("c_sw2", false);
            animator.SetBool("c_sw3", true);
            breakingProcess3rd = true;
        }
        else if (animator.GetBool("c_sw3"))
        {
            breakingProcessFinal = true;
        }
        Debug.Log("break =" + breakingProcessFinal);
    }

    public void SwitchOnFallingStatus()
    {
        this.fallingStatus = true;
    }

    public void SwitchOffFallingStatus()
    {
        this.fallingStatus = false;
    }

    public void SwitchOnBreakingNotFallFlag()
    {
        this.breakingNotFallFlag = true;
    }

    public void SwitchOffBreakingNotFallFlag()
    {
        this.breakingNotFallFlag = false;
    }

}
