using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Field : MonoBehaviour
{
    int pos { get; set; }
    int width;
    int height;
    int objectID;
    [SerializeField] bool rightArrow; [SerializeField] bool leftArrow;
    [SerializeField] bool upArrow; [SerializeField] bool downArrow;
    IPlayer player;
    public bool actioned;//プレイヤーが行動したかどうか判定
    public static float time;
    public static Field fieldInstance;

    void Start()
    {

    }

    void Update()
    {
        setPushingArrowkey();
        time += Time.deltaTime;
    }

    void setPushingArrowkey()
    {
        if (Input.GetKey("up"))
            upArrow = true;
        else
            upArrow = false;

        if (Input.GetKey("down"))
            downArrow = true;
        else
            downArrow = false;

        if (Input.GetKey("right"))
            rightArrow = true;
        else
            rightArrow = false;

        if (Input.GetKey("left"))
            leftArrow = true;
        else
            leftArrow = false;

    }


}

