using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class b_CrystalGenerator : MonoBehaviour
{
    public GameObject b_CrystalPrefab;
    public static GameObject blueCrystal;

    void Start()
    {
        blueCrystal = Instantiate(b_CrystalPrefab);
        blueCrystal.transform.position = new Vector3(2, 5, 0);
        blueCrystal.name = "BlueCrystal";
    }

}
