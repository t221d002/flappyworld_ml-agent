using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IEnemy 
{
    int posX{get; set;}
    int posY{get; set;}
    void move();
    void die();
}

public class Enemy : IEnemy
{
    public int posX { get; set; }
    public int posY { get; set; }

    public void move() 
    {
        
    }

    public void die()
    {
        
    }
}
