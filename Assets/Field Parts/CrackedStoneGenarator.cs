using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackedStoneGenarator : MonoBehaviour
{
    public GameObject crackedStonePrefab;
    void Start()
    {
        GameObject go = Instantiate(crackedStonePrefab);
        go.transform.position = new Vector3(0, 8, 0);
        go.name = "CrackedStone";
    }
}
