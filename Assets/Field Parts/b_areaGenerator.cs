using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class b_areaGenerator : MonoBehaviour
{
    public GameObject b_areaPrefab;
    public static GameObject blueArea;

    void Start()
    {
        blueArea = Instantiate(b_areaPrefab);
        blueArea.transform.position = new Vector3(2, 3, 0);
        blueArea.name = "blueArea";
    }

}
