using UnityEngine;
using System;

namespace LoadedData
{
    public class ReadErrorDialog : MonoBehaviour
    {
        public enum DialogResult
        {
            OK, NO
        }

        // ダイアログが操作されたときに発生するイベント
        public Action<DialogResult> FixDialog { get; set; }

        // OKボタンが押されたとき
        public void OnOk()
        {
            this.FixDialog?.Invoke(DialogResult.OK);
            DisplayLoadedData.ErrorDialogContainer.SetActive(false);
        }
    }
}