﻿using LoadedData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class ReadCheckArrangeOrderStageDialogue : MonoBehaviour
    {
        public enum DialogResult
        {
            OK,
            NO,
        }

        // ダイアログが操作されたときに発生するイベント
        public Action<DialogResult> FixDialog { get; set; }

        public void OnOk()
        {
            this.FixDialog?.Invoke(DialogResult.OK);
        }
        public void OnNo()
        {
            this.FixDialog?.Invoke(DialogResult.NO);
            DisplayLoadedData.CheckArrangeOrderStageDialogueContainer.SetActive(false);
            DisplayLoadedData.InputMapFileNameDialogueContainer.SetActive(true);
        }

    }
}
