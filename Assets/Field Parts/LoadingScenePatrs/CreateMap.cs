using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
using TMPro;

namespace LoadedData
{
    public class CreateMap
    {
        public static void CreateMapFile()
        {
            GameObject objInputFieldText = DisplayLoadedData.InputMapFileNameDialogueContainer.transform.Find("InputField").gameObject;
            Debug.Log(objInputFieldText.transform.GetChild(0).transform.Find("Text"));
            string mapFolderPath = "Assets/Field Parts/LoadingScenePatrs/MapFiles";
            String inputFieldText = objInputFieldText.transform.GetChild(0).transform.Find("Text").GetComponent<TextMeshProUGUI>().text;
            string mapFilePath = mapFolderPath + "/" + inputFieldText + ".map";

            try
            {
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(mapFilePath)) ;

                using (StreamWriter writer = new StreamWriter(mapFilePath, false))
                {
                    for (int n = 0; n < DisplayLoadedData.GetSelectedDataNameLength(); n++)
                    {
                        byte[] info = new UTF8Encoding(true).GetBytes(DisplayLoadedData.GetSelectedDataName(n));
                        // Add some information to the file.
                        writer.WriteLine(DisplayLoadedData.GetSelectedDataName(n));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }
    }
}
