using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LoadedData
{
    public class BackButtonClickedScript_LoadData : MonoBehaviour
    {
        public void OnClickBackButton()
        {
            SceneManager.LoadScene("LaunchScene");
        }
    }
}
