using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class b_CrystalGenerator : MonoBehaviour
    {
        public GameObject b_CrystalPrefab;
        public static GameObject blueCrystal;
        public static Vector3 pos;

        void Start()
        {
            if (StageFileLoader.blueCrystalPos != new Vector3(0, 0, 0))
            {
                blueCrystal = Instantiate(b_CrystalPrefab);
                blueCrystal.transform.position = StageFileLoader.blueCrystalPos;
                blueCrystal.name = "BlueCrystal";
                pos = blueCrystal.transform.position;
                pos.z = 0.25f;
                blueCrystal.transform.position = pos;
            }

            GamePlayAgents.blueCrystal = blueCrystal;
        }
         
        public static void InitializeBlueCrystalPos()
        {
            blueCrystal.transform.position = pos;
        }
    }
}

