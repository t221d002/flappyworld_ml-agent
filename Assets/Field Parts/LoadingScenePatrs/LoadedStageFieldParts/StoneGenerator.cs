using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class StoneGenerator : MonoBehaviour
    {
        public static List<GameObject> stoneList = new List<GameObject>();
        public GameObject stonePrefab;
        GameObject[] stoneObj = new GameObject[StageFileLoader.stonePosList.Count];
        List<Vector3> stonePosList = StageFileLoader.stonePosList;

        void Start()
        {
            for (int n = 0; n < StageFileLoader.stonePosList.Count; n++)
            {
                stoneObj[n] = Instantiate(stonePrefab);
                stoneObj[n].transform.position = stonePosList[n];
                stoneObj[n].name = "Stone" + n;
                Vector3 pos = stoneObj[n].transform.position;
                pos.z = 0.75f;
                stoneObj[n].transform.position = pos;
            }
            stoneList.AddRange(stoneObj);
        }
    }
}

