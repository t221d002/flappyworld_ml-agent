using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class b_area_halfGenerator : MonoBehaviour
    {
        public GameObject b_area_halfPrefab;
        public static GameObject blueArea_half;

        void Start()
        {
            if (StageFileLoader.blueArea_halfPos != new Vector3(0, 0, 0))
            {
                blueArea_half = Instantiate(b_area_halfPrefab);
                blueArea_half.transform.position = StageFileLoader.blueArea_halfPos;
                blueArea_half.name = "blueAreahalf";
            }
        }
    }
}
