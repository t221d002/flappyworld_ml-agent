using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class Wall_halfGenerator : MonoBehaviour
    {
        public static List<GameObject> wall_halfList = new List<GameObject>();
        public GameObject wall_halfPrefab;
        List<Vector3> wall_halfPosList = StageFileLoader.wall_halfPosList;
        GameObject[] wallHalfObj = new GameObject[StageFileLoader.wall_halfPosList.Count];

        void Start()
        {
            for (int n = 0; n < wall_halfPosList.Count; n++)
            {
                wallHalfObj[n] = Instantiate(wall_halfPrefab);
                wallHalfObj[n].transform.position = wall_halfPosList[n];
                wallHalfObj[n].name = "wall_half" + n;
            }
            wall_halfList.AddRange(wallHalfObj);
        }

    }
}
