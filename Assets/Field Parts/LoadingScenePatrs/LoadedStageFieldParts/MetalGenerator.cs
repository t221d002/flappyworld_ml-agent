using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class MetalGenerator : MonoBehaviour
    {
        public static List<GameObject> metalList = new List<GameObject>();
        public GameObject metalPrefab;
        GameObject[] metalObj = new GameObject[StageFileLoader.metalPosList.Count];
        List<Vector3> metalPosList = StageFileLoader.metalPosList;

        void Start()
        {
            for (int n = 0; n < StageFileLoader.metalPosList.Count; n++)
            {
                metalObj[n] = Instantiate(metalPrefab);
                metalObj[n].transform.position = metalPosList[n];
                metalObj[n].name = "Metal" + n;
                Vector3 pos = metalObj[n].transform.position;
                pos.z = 0.5f;
                metalObj[n].transform.position = pos;
            }
            metalList.AddRange(metalObj);
        }
    }
}
