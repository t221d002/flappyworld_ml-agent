using System.Collections.Generic;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Policies;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.MLAgents;
using System.Threading;
using JetBrains.Annotations;
using TMPro;
using System;
using Unity.VisualScripting;
using UnityEditor;

namespace LoadedData
{
    public class GamePlayAgents : Agent
    {
        private float waitTime = PlayerController.getWaitTime();//プレイヤーが移動してから静止するまでの時間
        private float moveTime = PlayerController.getWaitTime(); //プレイヤーが静止するまでの残り時間
        bool updateFlag = false;
        bool gameEnd = false;
        int maxPhase;
        int movePhase;
        bool running = false;
        bool unsolvableGame = false;
        public static bool unmoved = false;
        public static bool approached = false;
        public static bool backed = false;
        public static int actionHistory = 0;

        public static bool firstFall = false;
        public static GameObject player;
        public static GameObject blueCrystal;
        public static GameObject blueArea;
        public static GameObject EventSystem;
        float blueCrystalDistanceToBlueCrystal_before;
        float blueCrystalDistanceToGoal_after;
        float playerDistanceToBlueCrystal_before;
        float playerDistanceToBlueCrystal_after;
        int unpushedPhaseCount = 0;
        int unmovedCount = 0;
        Vector3 playerPosition_before;
        Vector3 playerPosition_after;
        List<GameObject> wallList;
        List<GameObject> stoneList;
        List<GameObject> metalList;
        static List<Vector3> wallPosList = new List<Vector3>();
        List<Vector3> stonePosList = new List<Vector3>();
        List<Vector3> metalPosList = new List<Vector3>();
        public GameObject pushedObject;
        Vector3 nextX = new Vector3(1, 0, 0);
        Vector3 nextY = new Vector3(0, 1, 0);
        Vector3 nextZ = new Vector3(0, 0, 1);
        public PlayerController playerController;
        int updatePhase = 0;
        int coolTimeCounter = 0;
        int action = 0;
        int actionLog = 0;
        int actionConsecutiveNum = 0;
        int consecutiveLimit = 5;

        int fastestPhase = 22;//クリアの理論値
        int idealPushCount = 12;
        int pushCount = 0;
        int approachedAdjacentBlockCount = 0;

        BufferSensorComponent bufferSensor;
        BehaviorParameters behaviorC;

        bool turnedUp = false;
        bool turnedDown = true;
        bool turnedRight = false;
        bool turnedLeft = false;
        public static bool moved = false;

        float leftBound = PlayerController.upperLeftCorner.x;
        float rightBound = PlayerController.upperRightCorner.x;
        public static int clearCount = 0;
        public static int clearMaxNum = 100;
        public static int mapStageCount;
        public static TransitionManager tManager = CanvasObjectManager_Launch.transitionManager;
        public static bool loadedNextStage = false;
        public static bool solved = false;

        const int moveU = 1;
        const int moveD = 2;
        const int moveR = 3;
        const int moveL = 4;

        public override void Initialize()
        {
            player = GameObject.FindWithTag("Player");
            playerController = player.GetComponent<PlayerController>();
            blueArea = GameObject.FindWithTag("BlueArea");
            EventSystem = GameObject.FindWithTag("Event");
            unsolvableGame = false;
            maxPhase = 800;
            movePhase = 0;
            moveTime = waitTime;
            gameEnd = false;
            updateFlag = true;
            blueCrystalDistanceToBlueCrystal_before = 0;
            blueCrystalDistanceToGoal_after = 0;
            playerDistanceToBlueCrystal_before = 0;
            playerDistanceToBlueCrystal_after = 0;
            playerPosition_before = Vector3.zero;
            bufferSensor = GetComponent<BufferSensorComponent>();
            behaviorC = GetComponent<BehaviorParameters>();
        }

        public override void OnActionReceived(ActionBuffers actionBuffers)
        {
            action = actionBuffers.DiscreteActions[0];
            AddReward(-0.000008f);

            if (!running && !PlayerController.GetIsRunning())
            {
                if (movePhase == 0)
                {
                    player = GameObject.FindWithTag("Player");
                    playerController = player.GetComponent<PlayerController>();
                }
                Debug.Log("playerMove104 phase = " + movePhase + " Reward = " + GetCumulativeReward());
                //if (GetCumulativeReward() > 0)
                //    AddReward(-GetCumulativeReward() / maxPhase);

                // int action = actionBuffers.DiscreteActions[0] + 1;

                if (!gameEnd)
                {
                    if (!solved)
                    {
                        blueCrystalDistanceToBlueCrystal_before = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);
                        playerDistanceToBlueCrystal_before = Vector2.Distance(player.transform.position, blueCrystal.transform.position);
                        movePhase++;
                        if (action == 0)
                        {
                            PlayerController.moved = true;
                            movePhase--;
                        }
                        if (action == 1)
                        {
                            turnedUp = true;
                            turnedDown = false;
                            turnedRight = false;
                            turnedLeft = false;
                            if (actionHistory == 1)
                                actionConsecutiveNum++;
                            else
                                actionConsecutiveNum = 0;
                            
                            playerController.WalkUp_execute();
                        }
                        else if (action == 2)
                        {
                            PlayerController.inputNum = 2;
                            turnedUp = false;
                            turnedDown = true;
                            turnedRight = false;
                            turnedLeft = false;
                            if (actionHistory == 2)
                                actionConsecutiveNum++;
                            else
                                actionConsecutiveNum = 0;

                            playerController.WalkDown_execute();
                        }
                        else if (action == 3)
                        {
                            PlayerController.inputNum = 3;
                            turnedUp = false;
                            turnedDown = false;
                            turnedRight = true;
                            turnedLeft = false;
                            if (actionHistory == 3)
                                actionConsecutiveNum++;
                            else
                                actionConsecutiveNum = 0;

                            playerController.WalkRight_execute();
                        }
                        else if (action == 4)
                        {
                            PlayerController.inputNum = 4;
                            turnedUp = false;
                            turnedDown = false;
                            turnedRight = false;
                            turnedLeft = true;
                            if (actionHistory == 4)
                                actionConsecutiveNum++;
                            else
                                actionConsecutiveNum = 0;

                            playerController.WalkLeft_execute();
                        }

                    }
                }
            }
            actionLog = action;
            Debug.Log("movePhase = " + movePhase + " action = " + action + "playerPhase = " + PlayerController.phase);
        }

        public override void CollectObservations(VectorSensor sensor)
        {
            sensor.AddObservation(player.transform.position);

            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, PlayerController.objectSize, 0f);
            bool touchingU = false;
            bool touchingD = false;
            bool touchingR = false;
            bool touchingL = false;
            bool pushable = false;

            GameObject touchingObjU = null;
            GameObject touchingObjD = null;
            GameObject touchingObjR = null;
            GameObject touchingObjL = null;


            foreach (Collider2D checkObj in collisions)
            {
                if (PlayerController.IsBlockTouchingU(player, checkObj.gameObject))
                {
                   if (touchingObjU != null)
                   {
                        if (PlayerController.IsObjectUnmovable(checkObj.gameObject))//壁ブロックの場合
                        {
                            if (PlayerController.IsObjectUnmovable(touchingObjU))
                            {
                                if (checkObj.transform.position.x < touchingObjU.transform.position.x)//片方の壁を指定
                                    touchingObjU = checkObj.gameObject;
                            }
                            else
                                touchingObjU = checkObj.gameObject;
                        }
                        else if (!PlayerController.IsObjectUnbreakable(checkObj.gameObject))
                            touchingObjU = checkObj.gameObject;
                   }
                   else 
                        touchingObjU = checkObj.gameObject;
                }
                if (PlayerController.IsBlockTouchingD(player, checkObj.gameObject))
                {
                   if (touchingObjD != null)
                   {
                        if (PlayerController.IsObjectUnmovable(checkObj.gameObject))//壁ブロックの場合
                        {
                            if (PlayerController.IsObjectUnmovable(touchingObjD))
                            {
                                if (checkObj.transform.position.x < touchingObjD.transform.position.x)//片方の壁を指定
                                    touchingObjD = checkObj.gameObject;
                            }
                            else
                                touchingObjD = checkObj.gameObject;
                        }
                        else if (!PlayerController.IsObjectUnbreakable(checkObj.gameObject))
                            touchingObjD = checkObj.gameObject;
                   }
                   else 
                        touchingObjD = checkObj.gameObject;
                }
                if (PlayerController.IsBlockTouchingR(player, checkObj.gameObject))
                {
                   if (touchingObjR != null)
                   {
                        if (PlayerController.IsObjectUnmovable(checkObj.gameObject))//壁ブロックの場合
                        {
                            if (PlayerController.IsObjectUnmovable(touchingObjR))
                            {
                                if (checkObj.transform.position.y < touchingObjR.transform.position.y)//片方の壁を指定
                                    touchingObjR = checkObj.gameObject;
                            }
                            else
                                touchingObjR = checkObj.gameObject;
                        }
                        else if (!PlayerController.IsObjectUnbreakable(checkObj.gameObject))
                            touchingObjR = checkObj.gameObject;
                   }
                   else 
                        touchingObjR = checkObj.gameObject;
                }
                if (PlayerController.IsBlockTouchingL(player, checkObj.gameObject))
                {
                   if (touchingObjL != null)
                   {
                        if (PlayerController.IsObjectUnmovable(checkObj.gameObject))//壁ブロックの場合
                        {
                            if (PlayerController.IsObjectUnmovable(touchingObjL))
                            {
                                if (checkObj.transform.position.y < touchingObjL.transform.position.y)//片方の壁を指定
                                    touchingObjL = checkObj.gameObject;
                            }
                            else
                                touchingObjL = checkObj.gameObject;
                        }
                        else if (!PlayerController.IsObjectUnbreakable(checkObj.gameObject))
                            touchingObjL = checkObj.gameObject;
                   }
                   else 
                        touchingObjL = checkObj.gameObject;
                }
            }

            if (touchingObjU != null)
            {
                touchingU = true;
                sensor.AddObservation(touchingObjU.transform.position);
            }
            else
            {
                sensor.AddObservation(Vector3.zero);
            }
            if (touchingObjD != null)
            {
                touchingD = true;
                sensor.AddObservation(touchingObjD.transform.position);
            }
            else
            {
                sensor.AddObservation(Vector3.zero);
            }
            if (touchingObjR != null)
            {
                touchingR = true;
                if (!PlayerController.IsObjectUnmovable(touchingObjR) && player.transform.position.y == touchingObjR.transform.position.y)
                    pushable = true;
                sensor.AddObservation(touchingObjR.transform.position);
            }
            else
            {
                sensor.AddObservation(Vector3.zero);
            }
            if (touchingObjL != null)
            {
                touchingL = true;
                if (!PlayerController.IsObjectUnmovable(touchingObjL) && player.transform.position.y == touchingObjL.transform.position.y)
                    pushable = true;
                sensor.AddObservation(touchingObjL.transform.position);
            }
            else
            {
                sensor.AddObservation(Vector3.zero);
            }

            if (actionHistory == 1 && touchingObjU != null)
            {
                sensor.AddObservation(touchingObjU.transform.position);
            }
            else if ((actionHistory == 2 || movePhase == 0) && touchingObjD != null)
            {
                sensor.AddObservation(touchingObjD.transform.position);
            }
            else if (actionHistory == 3 && touchingObjR != null)
            {
                sensor.AddObservation(touchingObjR.transform.position);
            }
            else if (actionHistory == 4 && touchingObjL != null)
            {
                sensor.AddObservation(touchingObjL.transform.position);
            }
            else
            {
                sensor.AddObservation(Vector3.zero);
            }

            /*
            if (touchingObjU != null)
            {
                sensor.AddObservation(touchingObjU.transform.position);
            }
            else
            {
                sensor.AddObservation(player.transform.position + nextY + nextZ);
            }
            if (touchingObjD != null)
            {
                sensor.AddObservation(touchingObjD.transform.position);
            }
            else
            {
                sensor.AddObservation(player.transform.position - nextY + nextZ);
            }
            if (touchingObjR != null)
            {
                sensor.AddObservation(touchingObjR.transform.position);
            }
            else
            {
                sensor.AddObservation(player.transform.position + nextX + nextZ);
            }
            if (touchingObjL != null)
            {
                sensor.AddObservation(touchingObjL.transform.position);
            }
            else
            {
                sensor.AddObservation(player.transform.position - nextX + nextZ);
            }
            */

            sensor.AddObservation(touchingU);
            sensor.AddObservation(touchingD);
            sensor.AddObservation(touchingR);
            sensor.AddObservation(touchingL);
            sensor.AddObservation(pushable);
            sensor.AddObservation(unpushedPhaseCount);
            sensor.AddObservation(pushCount);
            sensor.AddObservation(idealPushCount);
            sensor.AddObservation(0);
            sensor.AddObservation(0);
            sensor.AddObservation(0);
            sensor.AddObservation(0);
            sensor.AddObservation(0);

            if (blueArea != null && blueCrystal != null)
            {
                if (movePhase == 0)
                    sensor.AddObservation(Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position));
                else
                    sensor.AddObservation(blueCrystalDistanceToGoal_after);
            }
            //sensor.AddObservation(PlayerController.phase);

            if (blueCrystal != null)
            {
                sensor.AddObservation(blueCrystal.transform.position);
            }
            if (blueArea != null)
            {
                sensor.AddObservation(blueArea.transform.position);
            }


            foreach (Vector3 wallPos in wallPosList)
            {
                sensor.AddObservation(wallPos);
            }
            if (stoneList != null)
            {
                foreach (GameObject stoneObj in stoneList)
                {
                    sensor.AddObservation(stoneObj.transform.position);
                }
            }
            if (metalList != null)
            {
                foreach (GameObject metalObj in metalList)
                {
                    sensor.AddObservation(metalObj.transform.position);
                }
            }
        }

        public override void WriteDiscreteActionMask(IDiscreteActionMask actionMask)
        {
            if (actionLog == 0)
                actionMask.SetActionEnabled(0, 0, false);//落下寸前の待機を防止
            if (player.transform.position.y == blueCrystal.transform.position.y
                    && (PlayerController.IsBlockTouchingU(blueArea, blueCrystal) || Mathf.Abs(blueCrystal.transform.position.x - blueArea.transform.position.x) == 1.0f))
            {
                if (playerController.transform.position.x + 1 == blueCrystal.transform.position.x)
                {
                    Debug.Log("maskedActionwalkR289");
                    if (blueArea.transform.position.x == blueCrystal.transform.position.x)
                        actionMask.SetActionEnabled(0, moveR, false);
                    actionMask.SetActionEnabled(0, moveL, false);
                    actionMask.SetActionEnabled(0, moveU, false);
                    actionMask.SetActionEnabled(0, moveD, false);
                }
                else if (playerController.transform.position.x - 1 == blueCrystal.transform.position.x)
                {
                    Debug.Log("maskedActionwalkL296");
                    if (blueArea.transform.position.x == blueCrystal.transform.position.x)
                        actionMask.SetActionEnabled(0, moveL, false);
                    actionMask.SetActionEnabled(0, moveR, false);
                    actionMask.SetActionEnabled(0, moveU, false);
                    actionMask.SetActionEnabled(0, moveD, false);
                }
            }
            else if (actionHistory == 1 && actionConsecutiveNum > 5) 
                actionMask.SetActionEnabled(0, moveU, false);
            else if (actionHistory == 2 && actionConsecutiveNum > 5) 
                actionMask.SetActionEnabled(0, moveD, false);
            else if (actionHistory == 3 && actionConsecutiveNum > 5) 
                actionMask.SetActionEnabled(0, moveR, false);
            else if (actionHistory == 4 && actionConsecutiveNum > 5) 
                actionMask.SetActionEnabled(0, moveL, false);
         /*   if (PlayerController.touchingR || PlayerController.touchingL || PlayerController.touchingU || PlayerController.touchingD)
            {
                playerController.CheckPlayerTouchingUnmovable();

                if (PlayerController.touchingR && playerController.touchingUnmovableR)
                {
                    Debug.Log("unmovableActionMasked314");
                    actionMask.SetActionEnabled(0, moveR, false);
                }
                if (PlayerController.touchingL && playerController.touchingUnmovableL)
                    actionMask.SetActionEnabled(0, moveL, false);
                if (PlayerController.touchingU && playerController.touchingUnmovableU)
                {
                    Debug.Log("unmovableActionmaskedU321");
                    actionMask.SetActionEnabled(0, moveU, false);
                }
                if (PlayerController.touchingD && playerController.touchingUnmovableD)
                    actionMask.SetActionEnabled(0, moveD, false);
            }
         */
        }

        public override void OnEpisodeBegin()
        {
            playerController.ResetGame();
            actionHistory = 0;
            actionConsecutiveNum = 0;
            moved = false;
            blueCrystal = GameObject.Find("BlueCrystal");
            player = GameObject.FindWithTag("Player");
            playerController = player.GetComponent<PlayerController>();
            blueArea = GameObject.FindWithTag("BlueArea");
            Debug.Log("EpisodeBigin");
            moveTime = 0;
            movePhase = 0;
            PlayerController.phase = 0;
            unmovedCount = 0;
            gameEnd = false;
            updateFlag = true;
            blueCrystalDistanceToBlueCrystal_before = 0;
            blueCrystalDistanceToGoal_after = 0;
            firstFall = false;
            bool unmoved = false;
            GetObjectList();
            turnedUp = false;
            turnedDown = true;
            turnedRight = false;
            turnedLeft = false;
            action = 0;
            approachedAdjacentBlockCount = 0;
            pushCount = 0;
        }

        private void FixedUpdate()
        {
            if (updateFlag)
            {
                if (!gameEnd)
                    moveTime -= Time.deltaTime;

                if (moveTime > 0.0f || gameEnd)
                {
                    return;
                }
                if ((movePhase == 0 || (moved)) && moveTime <= 0 && !running && !PlayerController.GetIsRunning())
                {
                    if (movePhase == 0)
                    {
                        solved = false;
                    }
                    else

                    PlayerController.moved = false;
                    RequestDecision();
                    RewardProcess();
                    moveTime = waitTime;
                }
            }
        }

        public override void Heuristic(in ActionBuffers actionBuffers)
        {
            var actionsOut = actionBuffers.DiscreteActions;
            Debug.Log("isRunning = " + PlayerController.GetIsRunning());

            if (Input.GetKey("up"))
                actionsOut[0] = 1;
            else if (Input.GetKey("down"))
                actionsOut[0] = 2;
            else if (Input.GetKey("right"))
                actionsOut[0] = 3;
            else if (Input.GetKey("left"))
                actionsOut[0] = 4;
            Debug.Log("Reward = " + GetCumulativeReward());
        }

        public void EndGame()
        {
            Debug.Log("GameEnd");
            gameEnd = true;
            EndEpisode();
        }

        public void GetObjectList()
        {
            if (wallPosList.Count == 0)
            {
                List<GameObject> wallList = WallGenerator.wallList;
                List<GameObject> stoneList = StoneGenerator.stoneList;
                List<GameObject> metalList = MetalGenerator.metalList;

                foreach (GameObject wallObj in wallList)
                {
                    wallPosList.Add(wallObj.transform.position);
                }
            }
        }

        public void RewardProcess()
        {
            if (!running && !PlayerController.GetIsRunning() && action != 0)
            {
                blueCrystalDistanceToGoal_after = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);
                playerDistanceToBlueCrystal_after = Vector2.Distance(player.transform.position, blueCrystal.transform.position);
                Debug.Log("showDistance_PtoB430 = " + playerDistanceToBlueCrystal_after);
                //if (GetCumulativeReward() > 0)
                //    AddReward(-GetCumulativeReward() / maxPhase);

                running = true;

                if (firstFall)
                {
                    AddReward(0.05f);
                    firstFall = false;
                    Debug.Log("fallFirst157" + " reward = " + GetCumulativeReward());
                }
                if (PlayerController.IsBlueCrystalUnreachable)
                {
                    unsolvableGame = true;
                    Debug.Log("BlueCrystalUnreachable");
                }
                //if (playerDistanceToGoal_after < playerDistanceToGoal_before && playerDistanceToGoal_after != 0)
                //{
                //    AddReward(0.02f / (float)Math.Pow(playerDistanceToGoal_after, 2));
                //}
                //if (playerPosition_after == playerPosition_before && !PlayerController.falling)
                //{
                //    AddReward(-0.0005f);
                //    Debug.Log("reward183wall = " + GetCumulativeReward() + "  phase = " + PlayerController.phase + "positionBefore = " + playerPosition_before + " positionAfter = " + playerPosition_after);
                //}

                if (approached)
                {
                    if (blueCrystalDistanceToGoal_after != 0)
                        AddReward(0.25f / (float)Math.Pow(blueCrystalDistanceToGoal_after, 2));
                    Debug.Log("RewardAdded_distance108  " + "distanceBefore = "
                        + blueCrystalDistanceToBlueCrystal_before + " distanceAfter = " + blueCrystalDistanceToGoal_after + " Reward = " +
                        GetCumulativeReward());
                    Debug.Log("plusRewardDistance = " + 0.01f * (float)Math.Pow(blueCrystalDistanceToGoal_after, 2));
                    approached = false;
                }
                else if (backed)
                {
                    if (blueCrystalDistanceToGoal_after != 0)
                        AddReward(- 0.5f / (float)Math.Pow(blueCrystalDistanceToGoal_after, 2));
                    backed = false;
                }

                pushedObject = PlayerController.pushedObjectHistory;
                if (pushedObject != null && (pushedObject.transform.position.z == 0.25 || pushedObject.transform.position.z == 0.75))
                {
                    pushCount++;
                    if (approachedAdjacentBlockCount > 0)
                    {
                        if (approachedAdjacentBlockCount >= 7)
                        {
                            AddReward(0.008f * approachedAdjacentBlockCount);
                        }
                            approachedAdjacentBlockCount = 0;
                    }
                    if (pushedObject.tag == "Blue_C")
                    {
                        AddReward(0.05f);
                        Debug.Log("Addreward128 pushcount = " + pushCount);
                    }
                    else
                        AddReward(0.04f);
                    PlayerController.pushedObjectHistory = null;

                    unpushedPhaseCount = 0;
                }
                else if (!PlayerController.falling)
                {
                    unpushedPhaseCount++;
                }

                if (playerDistanceToBlueCrystal_after <= 1.0f && 
                    (player.transform.position.x + 1 == blueCrystal.transform.position.x 
                    || player.transform.position.x - 1 == blueCrystal.transform.position.x) && pushedObject == null)
                {
                    Debug.Log("addrewardDistance1.0_484");
                    approachedAdjacentBlockCount++;
                    if (approachedAdjacentBlockCount == 6)
                        AddReward(-0.008f * 6);
                    else if (approachedAdjacentBlockCount >= 7)
                        AddReward(-0.008f);
                    else
                        AddReward(0.008f);
                }
                else if (playerDistanceToBlueCrystal_after <= 1.25f)
                    AddReward(0.002f);
                else if (playerDistanceToBlueCrystal_after <= 1.5f)
                    AddReward(0.0015f);
                else if (playerDistanceToBlueCrystal_after <= 2.0f)
                    AddReward(0.0008f);
                else if (playerDistanceToBlueCrystal_after <= 3.0f)
                    AddReward(0.0005f);

                if (!gameEnd)
                {

                    Debug.Log("isRunning = " + PlayerController.GetIsRunning());

                    if (unsolvableGame)
                    {
                        gameEnd = true;
                        PlayerController.IsBlueCrystalUnreachable = false;
                        unsolvableGame = false;
                        AddReward(-1.0f);
                        unpushedPhaseCount = 0;
                        Debug.Log("reward96 = " + GetCumulativeReward());
                        EndEpisode();
                    }
                    else if ((player.transform.position.x < leftBound || player.transform.position.x > rightBound)
                            && (leftBound != 0 && rightBound != 0))
                    {
                        gameEnd = true;
                        AddReward(-1.0f);
                        EndEpisode();
                    }
                    else if (blueCrystalDistanceToGoal_after == 1
                            && blueArea.transform.position.y + 1 == blueCrystal.transform.position.y)
                    {
                        gameEnd = true;
                        if (tManager.enteredFromMapLoad && !loadedNextStage)
                        {
                            clearCount++;
                            if (clearCount == clearMaxNum)
                            {
                                mapStageCount++;
                            }
                        }

                        if (pushCount > idealPushCount)
                            SetReward(GetCumulativeReward() * ((float)idealPushCount / pushCount) * 0.8f);
                        AddReward(3.0f);
                        AddReward(6.0f * (maxPhase - PlayerController.phase) / maxPhase);
                        Debug.Log("StageClear Reward = " + GetCumulativeReward() + " pushcount = " + pushCount + "  phase = " + movePhase);
                        Debug.Log("bonusReward = " + 6.0f * (maxPhase - PlayerController.phase) / maxPhase);
                        playerController.ClearedProcess();
                        solved = false;
                        EndEpisode();
                    }

                    if (unpushedPhaseCount > 10 && !PlayerController.falling)
                    {
                        if (unpushedPhaseCount <= 50)
                        {
                            AddReward(-0.002f);
                        }
                        else if (unpushedPhaseCount <= 100)
                        {
                            AddReward(-0.0025f);
                        }
                        else if (unpushedPhaseCount <= maxPhase / 4)
                            AddReward(-0.003f);
                        else if (unpushedPhaseCount <= maxPhase / 2)
                            AddReward(-0.006f);
                        else if (unpushedPhaseCount <= maxPhase * 3 / 4)
                            AddReward(-0.007f);
                        else if (unpushedPhaseCount <= maxPhase)
                            AddReward(-0.008f);

                        if (unpushedPhaseCount >= 100 && blueCrystal.transform.position.y == blueArea.transform.position.y + 1
                                && Mathf.Abs(blueCrystal.transform.position.x - blueArea.transform.position.x) == 1.5f)
                        {
                            AddReward(-0.01f);
                        }
                        Debug.Log("unpushedCount = " + unpushedPhaseCount);
                    }

                }

                if (PlayerController.phase >= maxPhase)
                {
                    Debug.Log("GameEnd");
                    gameEnd = true;
                    unpushedPhaseCount = 0;
                    //AddReward(-0.001f * unpushedPhaseCount);
                    Debug.Log(" unmovedCount509 = " + unmovedCount);
                    EndEpisode();
                }
                moveTime = waitTime;

                running = false;
                if (GetCumulativeReward() > 1.0)
                    AddReward(-GetCumulativeReward() / 100);
                else if (GetCumulativeReward() > 0.5)
                    AddReward(-GetCumulativeReward() / 200);
                else if (GetCumulativeReward() > 0)
                    AddReward(-GetCumulativeReward() / 400);
                else
                    AddReward(-1.0f / maxPhase);


                if (PlayerController.phase > 0)
                {
                    playerPosition_before = playerPosition_after;
                    playerPosition_after = Vector3.zero;
                }

            }
        }
    }


}

