using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class b_areaGenerator : MonoBehaviour
    {
        public GameObject b_areaPrefab;
        public static GameObject blueArea;
        static Vector3 pos;

        void Start()
        {
            if (StageFileLoader.blueAreaPos != new Vector3(0, 0, 0))
            {
                blueArea = Instantiate(b_areaPrefab);
                blueArea.transform.position = StageFileLoader.blueAreaPos;
                blueArea.name = "blueArea";
                pos = blueArea.transform.position;
                pos.z = -0.25f;
                blueArea.transform.position = pos;
            }

            GamePlayAgents.blueArea = blueArea;
        }

    }
}
