using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace LoadedData
{
    public class ListedMenuClickedScript : MonoBehaviour
    {
        public static bool speedDefaultChanged = false;
        void Start()
        {

        }

        public static void OnClickMenuResumeButton()
        {
            //GameObject MenuButton = PlayerController.Menubutton;
            //GameObject MenuPannelContainer = PlayerController.MenuPannelContainer;


         //   MenuPannelContainer.SetActive(false);
         //   MenuButton.SetActive(true);
            Time.timeScale = 1;
        }

        public static void OnClickMenuOptionsButton()
        {
        //    GameObject MenuButton = PlayerController.Menubutton;
        //    GameObject MenuList = PlayerController.MenuList;
        //GameObject OptionsPanel = PlayerController.OptionsPanel;

        //    MenuList.SetActive(false);
        //  OptionsPanel.SetActive(true);

            GameObject[] speedSettingPanels = GameObject.FindGameObjectsWithTag("Speed");
            if (!speedDefaultChanged)
            {
                foreach (var speedSettingPanel in speedSettingPanels)
                {
                    Debug.Log(speedSettingPanel.name);
                    if (speedSettingPanel.name.Contains("Mid"))
                        speedSettingPanel.GetComponent<Outline>().enabled = !speedSettingPanel.GetComponent<Outline>().enabled;
                    speedDefaultChanged = true;
                }
            }

        }

        public static void OnClickMenuBackToMenuButton()
        {
            SceneManager.LoadScene("LoadDataListScene");
            Time.timeScale = 1;
        }

        public static void OnClickBackButton_GamePlay()//オプション画面のBackボタンで使用
        {
        //    GameObject MenuList = PlayerController.MenuList;
        //   GameObject OptionsPanel = PlayerController.OptionsPanel;

            GameObject[] speedSettingPanels = GameObject.FindGameObjectsWithTag("Speed");
            foreach (var speedSettingPanel in speedSettingPanels)
            {
                Debug.Log(speedSettingPanel);
                if (speedSettingPanel.GetComponent<Outline>().enabled)
                {
                    Debug.Log("enabledOutlinePanel = " + speedSettingPanel);
                    if (speedSettingPanel.name.Contains("Mid"))
                        PlayerController.setWaitTime(0.5f);
                    else if (speedSettingPanel.name.Contains("Fast") && !speedSettingPanel.name.Contains("Very"))
                        PlayerController.setWaitTime(0.25f);
                    else if (speedSettingPanel.name.Contains("Very"))
                        PlayerController.setWaitTime(0.1f);
                    else if (speedSettingPanel.name.Contains("Slow"))
                        PlayerController.setWaitTime(1.0f);
                }
            }
        //    MenuList.SetActive(true);
       //     OptionsPanel.SetActive(false);
        }


    }
}