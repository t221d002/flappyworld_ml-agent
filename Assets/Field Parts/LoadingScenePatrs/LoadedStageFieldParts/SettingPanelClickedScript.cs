using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using JetBrains.Annotations;

namespace LoadedData
{
    public class SettingPanelClickedScript : MonoBehaviour
    {
        public static string selectedSpeed;
        public void SpeedSettingPanelClickedScript()
        {
            GameObject speedSettingPanel = LoadedDataPanelClickedScript.GetClickedPanel("Speed");
            var outline = speedSettingPanel.GetComponent<Outline>();
            selectedSpeed = speedSettingPanel.name;

            if (speedSettingPanel != null)
            {
                if (!outline.enabled)
                {
                    bool panelselected = false;
                    GameObject[] speedSettingPanels = GameObject.FindGameObjectsWithTag("Speed");
                    foreach (var panelObj in speedSettingPanels)
                    {
                        if (panelObj.GetComponent<Outline>().enabled)
                        {
                            panelselected = true;
                            break;
                        }
                    }
                    if (panelselected)
                    {
                        foreach (var panelObj in speedSettingPanels)
                        {
                            panelObj.GetComponent<Outline>().enabled = false;
                        }
                    }
                    outline.enabled = !outline.enabled;
                }
            }
        }
    }
}