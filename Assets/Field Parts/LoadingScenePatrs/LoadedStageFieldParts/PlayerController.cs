using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace LoadedData
{
    public class PlayerController : MonoBehaviour
    {
        public static PlayerController playerInstance;
        public bool walking = false;

        [SerializeField] public List<float> operationTimeList = new List<float>();

        public GameObject player;
        public GameObject targetObj;
        public static string halfBlock = "HalfBlock";
        public static string stone = "Stone";
        public static string dummy = "dummy";
        public static string wall = "Wall";
        public static string BlueArea = "BlueArea";
        public static string BlueCrustal = "Blue_C";

        GameObject blueCrystal;
        GameObject blueArea;
        public bool touchingUnmovableR = false;
        public bool touchingUnmovableL = false;
        public bool touchingUnmovableU = false;
        public bool touchingUnmovableD = false;

        Vector3 movePosition;//移動する距離
        public float speedTime = 0.5f;

        public Vector3 startPosition;
        public static Vector3 moveY; //(1マス毎の)Y軸の移動距離
        public static Vector3 moveX; //(1マス毎の)X軸の移動距離
        bool fStepSwitch = false;//firstStepのboolを反転
        public static bool solved = false;
        int solvedTrueCount = 0;//クリア時の判定の重複を防ぐ
        public static bool adjacentToMovable = false;//可動ブロックにプレイヤーが横に向き合っているかどうか
        public static GameObject pushedObjectHistory;//直近に押したブロックを記録
        Animator animator;

        Vector3 playerPosition_before;
        Vector3 playerPosition_after;
        public static bool moved = false;

        GameObject gameManagerObj;
        [SerializeField] static bool isRunninng = false;//コルーチンの重複起動を防止
        [SerializeField] public static GameObject adjacentObjectF;
        [SerializeField] GameObject adjacentObjectU;
        [SerializeField] GameObject adjacentObjectD;
        [SerializeField] GameObject adjacentObjectR;
        [SerializeField] GameObject adjacentObjectL;
        [SerializeField] GameObject breakingObject;
        [SerializeField] GameObject breakingObject2nd;
        [SerializeField] GameObject breakingObject3rd;
        [SerializeField] int fStoneListNum = 0;//二次元リストfallingStoneの要素数
        [SerializeField] GameObject upperStoneObj;//ブロックの上に載っているブロック(落下の候補)
        [SerializeField] string adjacentNameU;//上に隣接しているオブジェクトの名前
        [SerializeField] string adjacentNameD = "dummy";//下に〃
        [SerializeField] string adjacentNameF = "dummy";//前に〃(プレーヤーが押しているオブジェクト)

        [SerializeField] string adjacentNameR = "dummy";//右に〃
        [SerializeField] string adjacentNameL = "dummy";//左に〃

        GameManager gameManager;
        string keyHistory;

        [SerializeField] public static bool touchingR; [SerializeField] public static bool touchingL;
        [SerializeField]public static bool touchingU; [SerializeField]public static bool touchingD;//プレーヤーがオブジェクト(非生物)に触れている方向
        [SerializeField] bool playerDead = false;//プレイヤーがミスした時にtrue

        bool colCheckSwitch;    //一歩分だけ歩いた後にonになる o-c-stayの処理で必要
        [SerializeField] bool objLR_CheckSwitch; //プレーヤーの前後にあるオブジェクトの識別処理へ入るために必要
        [SerializeField] bool breaking;//石ブロックが壊れ始めた時にtrue
        [SerializeField] bool breaking2nd;//２個目の石ブロック
        [SerializeField] bool breaking3rd;//３個目〃
        [SerializeField] bool breakingUpperLink = false;//breakingObjectの上に石系オブジェクトががある場合にtrue
        [SerializeField] bool breakingUpperLink2nd = false;
        [SerializeField] bool upperBreakingFallingWait;//上破壊のブロックの上にブロックがある時に必要
        [SerializeField] bool destroyed;//石ブロックが破壊されて消滅した時にcollisionExitで使われる
        [SerializeField] public static bool falling;//石系オブジェクトの落下中にtrue
        bool notTouchUnderTheStone = false;
        bool fallingObjTouchedDown = false;
        public static bool breakStarted = false;

        public static bool IsBlueCrystalUnreachable = false;//青クリスタルがゴールに到達可能かどうか判定し、Agent側で報酬を与える（マイナス）際に使う

        public static Vector2 objectSize = new Vector2(1, 1);

        string objectName;  //隣接しているオブジェクトの名前
        bool notEnterCE = false; //c-stayの後にc-enterに入るのを防止
        bool CEprocessed = false; //c-enterで２回以上連続した処理を行うのを防止(一度の歩行で複数のオブジェクトと衝突した場合)
        bool touchFlagR = false; bool touchFlagL = false;
        bool touchFlagU = false; bool touchFlagD = false;
        [SerializeField] bool dualTouchingD = false;//ブロックが下に２つ接しているかどうか
        bool touchingWall = true;
        public bool touchingObjectAtTheBottom = false;//石ブロックがオブジェクトの上に載っているかどうか
        [SerializeField] bool notBreak1st = false; [SerializeField] bool notBreak2nd = false;
        [SerializeField] bool blockAbovePlayer = false;
        bool checkflagPlayerOverhead = false;

        private static float wait = 0.1f; //操作不能時間
        Camera cam;
        public static Vector3 center;//メインカメラにおける中心の座標
        public static Vector3 upperLeftCorner;//左上角の壁ブロックの座標
        public static Vector3 upperRightCorner;//右上〃
        public static Vector3 lowerLeftCorner;//左下〃
        public static Vector3 lowerRightCorner;//右下〃

        TransitionManager tManager;

        public static int inputNum;//学習エージェントが行動を決定したときに用いる
        [SerializeField] List<FallingObjectList> fallingObjectListList = new List<FallingObjectList>();

        public static int phase = 0;
        public float blueCrystalDistanceToGoal_before = 0;
        public float blueCrystalDistanceToGoal_after = 0;

        [System.Serializable]
        public class FallingObjectList
        {
            public List<GameObject> fallingObjectListChild = new List<GameObject>();
        }
        public Sprite pressedImage;
        SpriteRenderer sr;

        void Start()
        {
            InitializeGame();
        }


        private IEnumerator WalkRight()
        {
            if (isRunninng) { yield break; }
            isRunninng = true;

            if (phase == 1)
            {
                CheckTouching();    
            }

            playerPosition_before = player.transform.position;
            blueCrystalDistanceToGoal_before = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);

            Debug.Log("phaseFlappy = " + phase++);

            notTouchUnderTheStone = false;
            bool notEnter1 = false; bool notEnter2 = false;
            this.animator.SetBool("turnedL", false);
            this.animator.SetBool("turnedU", false);
            this.animator.SetBool("turnedD", false);
            InitializeTouchFlag();
            CEprocessed = false;
            bool notFall = false;

            if (adjacentNameF == "")
            {
                adjacentNameF = dummy;
            }
            else if (adjacentNameF != dummy && adjacentObjectF == null
                && player.transform.position.y != GameObject.Find(adjacentNameF).transform.position.y)
            {
                Debug.Log("AdjFNameNotDummy429");
                adjacentNameF = dummy;
            }
            else if (adjacentNameF == dummy && touchingR)
            {
                adjacentObjectF = GetAdjacentObjectR();
                if (adjacentObjectF != null)
                    adjacentNameF = adjacentObjectF.name;
            }

            if (falling || !touchingR)
            {
                Debug.Log("untouched208WalkR");
                if (IsPlayerTouchingObjectR())
                {
                    touchingR = true;
                }
            }

            if (touchingR == false)
            {
                gameManager.CallInoperable(wait);
                this.animator.SetBool("walkingR", true);
                CheckFallingAndDropStone();
                player.transform.position += moveX / 2;
                operationTimeList.Add(Field.time);

                yield return new WaitForSeconds(wait / 2);

                CheckFallingAndDropStone();

                BreakStoneInCoroutine();
                player.transform.position += moveX / 2;

                this.animator.SetBool("walkingR", false);
                if (falling)
                {
                    yield return new WaitForSeconds(wait / 2);
                }
                colCheckSwitch = true;
                Debug.Log("172walkR");
            }
            else if (touchingR && adjacentNameF != "dummy")
            {
                Debug.Log("169pr");
                if (breaking && breakingObject.name == adjacentNameF)
                {
                    if (breaking2nd)
                        yield return new WaitForSeconds(wait / 64);
                    CheckAdjacent_F_statusInBreakingR(breakingObject);
                    Debug.Log("break180");
                }
                if (breaking2nd && breakingObject2nd.name == adjacentNameF)
                {
                    CheckAdjacent_F_statusInBreakingR(breakingObject2nd);
                    Debug.Log("break2nd184");
                }
                else
                    adjacentObjectF = GameObject.Find(adjacentNameF);

                if (adjacentNameR != dummy)
                {
                    GameObject tentativeObj;
                    tentativeObj = GameObject.Find(adjacentNameR);
                    if (IsPlayerTouchingObjectR(tentativeObj))
                    {
                        adjacentNameF = adjacentNameR;
                        adjacentObjectF = tentativeObj;
                    }
                }


                if (!IsObjectUnmovable(adjacentNameF))
                {
                    if (!IsObjectAdjacentedR(adjacentNameF))
                    {//ブロックがプレイヤー以外のオブジェクトに対して前後に隣接していない場合
                        if (!GetFallingStatus(adjacentObjectF)
                            || (IsObjectBreaking(adjacentObjectD) && !adjacentObjectD.GetComponent<StoneBehaviour>().breakingProcess2nd))
                        {
                            Debug.Log("notAdj187");
                            gameManager.CallInoperable(wait);
                            this.animator.SetBool("walkingR", true);
                            player.transform.position += moveX / 2;
                            adjacentObjectF.transform.position += moveX;
                            pushedObjectHistory = adjacentObjectF;
                            CheckFallingAndDropStone();

                            notEnter2 = true;

                            if (!IsObjectTouchingUnderTheStone(adjacentObjectF))
                            {
                                MakeListOfFallingObject(adjacentObjectF);
                                notTouchUnderTheStone = true;
                                Debug.Log("fallScript226");
                            }
                            GameObject tentativeObj = adjacentObjectF;
                            yield return new WaitForSeconds(wait / 2);
                            player.transform.position += moveX / 2;
                            checkflagPlayerOverhead = true;
                            this.animator.SetBool("turnedR", true);

                            adjacentObjectU = GetAdjacentObjectU();
                            if (IsBlockReadyToFallOnPlayer(adjacentObjectU, tentativeObj))
                                playerDead = true;
                            else
                                adjacentObjectU = null;
                            if (!playerDead)
                                CheckFallingAndDropStone();

                            this.animator.SetBool("walkingR", false);

                            if (playerDead)
                            {
                                notTouchUnderTheStone = false;
                                yield return PressingProcessInCoroutine();
                            }

                            if (falling)
                            {
                                notFall = true;
                                notEnter2 = true;
                                yield return new WaitForSeconds(wait / 2);
                            }
                            colCheckSwitch = true;
                            if (breaking || breaking2nd)
                                BreakStoneInCoroutine();
                        }
                    }
                    else if (!breaking)
                    {
                        this.animator.SetBool("turnedR", true);
                        breakingObject = adjacentObjectF;//プレイヤーの正面にあるオブジェクトが破壊対象に
                        breaking = true;//ブロックを破壊
                    }
                    else if (breaking && breakingObject.name != adjacentNameF)
                    {
                        this.animator.SetBool("turnedR", true);
                        Debug.Log("wrightbreak2ndObj232before" + breakingObject2nd + "   adjF" + adjacentObjectF);
                        breakingObject2nd = adjacentObjectF;
                        breaking2nd = true;
                    }
                }
                else
                {
                    gameManager.CallInoperable(wait / 4 + wait / 8);
                    yield return new WaitForSeconds(wait / 4);
                    if (!this.animator.GetBool("turnedR"))
                    {
                        this.animator.SetBool("turnedR", true);
                        BreakStoneInCoroutine();
                        SwitchOn_NotBreak();
                    }

                }

                Debug.Log("R =" + touchingR + "  " + "L =" + touchingL + "  "
                    + "U =" + touchingU + "  " + "D =" + touchingD + "    objectName =" + adjacentNameF);

            }
            else if (adjacentNameR != "dummy")
            {
                adjacentObjectF = GameObject.Find(adjacentNameR);
                this.animator.SetBool("turnedR", true);
                Debug.Log("adjacentNameR NotDummy407");

                if (player.gameObject.transform.position.y == adjacentObjectF.transform.position.y)
                {
                    if ((!IsObjectUnbreakable(adjacentObjectF) && !GetFallingStatus(adjacentObjectF))
                            || (IsObjectBreaking(adjacentObjectD) && !adjacentObjectD.GetComponent<StoneBehaviour>().breakingProcess2nd))
                    {
                        if (IsObjectAdjacentedF(adjacentNameR))
                        {
                            if (!breaking)
                            {
                                breakingObject = adjacentObjectF;
                                breaking = true;
                            }
                            else if (!breaking2nd)
                            {
                                breakingObject2nd = adjacentObjectF;
                                breaking2nd = true;
                            }
                        }
                        else
                        {
                            Debug.Log("process225");
                            gameManager.CallInoperable(wait);
                            this.animator.SetBool("walkingR", true);
                            player.transform.position += moveX / 2;
                            adjacentObjectF.transform.position += moveX;
                            pushedObjectHistory = adjacentObjectF;
                            CheckFallingAndDropStone();

                            operationTimeList.Add(Field.time);

                            if (!IsObjectTouchingUnderTheStone(adjacentObjectF) && !GetFallingStatus(adjacentObjectF))
                            {
                                MakeListOfFallingObject(adjacentObjectF);
                                notTouchUnderTheStone = true;
                            }
                            GameObject tentativeObj = adjacentObjectF;
                            yield return new WaitForSeconds(wait / 2);
                            player.transform.position += moveX / 2;
                            checkflagPlayerOverhead = true;
                            adjacentObjectU = GetAdjacentObjectU();

                            if (IsBlockReadyToFallOnPlayer(adjacentObjectU, tentativeObj))
                            {
                                playerDead = true;
                                notTouchUnderTheStone = false;
                                Debug.Log("playerDeadWalkingR406 notTouchUStone = " + notTouchUnderTheStone);
                            }
                            else
                                adjacentObjectU = null;
                            Debug.Log("adjacentObjectU404 = " + adjacentObjectU + "playerPosition" + player.transform.position);
                            Debug.Log("368falling = " + falling);

                            if (!playerDead)
                                CheckFallingAndDropStone();

                            if (breaking || breaking2nd)
                                BreakStoneInCoroutine();
                            this.animator.SetBool("walkingR", false);

                            if (playerDead)
                            {
                                yield return PressingProcessInCoroutine();
                            }

                            if (falling)
                            {
                                yield return new WaitForSeconds(wait / 2);
                                notEnter2 = true;
                            }

                        }
                    }
                    adjacentNameF = adjacentNameR;
                    if (adjacentObjectF != null && !IsObjectAdjacentedF(adjacentNameF) && (adjacentObjectF.tag != halfBlock && adjacentObjectF.tag != wall))
                    {
                        adjacentNameL = "dummy";
                        adjacentNameR = "dummy";//左右に隣接するブロックを識別する必要は無くなったので初期化
                    }
                    colCheckSwitch = true;
                }

                adjacentObjectF = GetAdjacentObjectR();

                if (adjacentObjectF != null && adjacentObjectF.tag == stone && IsObjectAdjacentedF(adjacentNameR) && breaking && breakingObject.name != adjacentNameR)
                {
                    breakingObject2nd = GameObject.Find(adjacentNameR);
                    breaking2nd = true;
                    Debug.Log("breaking2nd269");
                }

            }
            if (touchingR)
            {
                GameObject tentativeObject = GetPlayerTouchingObjectR_checkDualTouch();
                Debug.Log("tentativeObjR475 = " + tentativeObject);
                if (!this.animator.GetBool("turnedR") && !notEnter1 && tentativeObject != null)
                {
                    this.animator.SetBool("turnedR", true);
                    notEnter1 = true;

                    if (!falling)
                    {
                        gameManager.CallInoperable(wait / 4 + wait / 8);
                        yield return new WaitForSeconds(wait / 4);
                    }
                    else
                    {
                        if (!notFall)
                            CheckFallingAndDropStone();
                        gameManager.CallInoperable(wait / 4 + wait / 8);
                        yield return new WaitForSeconds(wait / 4);
                    }
                }
                else if (this.animator.GetBool("turnedR") && !notEnter2 && falling)
                {
                    if (!IsPlayerTouchingObjectR() && tentativeObject == null)
                    {
                        CheckFallingAndDropStone();
                        touchingR = false;
                        gameManager.CallInoperable(wait);
                        this.animator.SetBool("walkingR", true);
                        player.transform.position += moveX / 2;
                        yield return new WaitForSeconds(wait / 2);
                        BreakStoneInCoroutine();
                        CheckFallingAndDropStone();
                        player.transform.position += moveX / 2;
                        this.animator.SetBool("walkingR", false);
                        yield return new WaitForSeconds(wait / 2);
                    }
                    else if (IsPlayerTouchingObjectR() && tentativeObject != null)
                    {
                        Debug.Log("tentativeObjWalkR517 = " + tentativeObject);
                        if (!IsObjectUnmovable(tentativeObject))
                        {
                            if (player.transform.position.y + 0.5f == tentativeObject.transform.position.y)//空中ブロック押しの処理
                            {
                                CheckFallingAndDropStone();
                                if (!IsObjectAdjacentedL(tentativeObject))
                                {
                                    gameManager.CallInoperable(wait);
                                    this.animator.SetBool("walkingR", true);
                                    player.transform.position += moveX / 2;
                                    tentativeObject.transform.position += moveX;
                                    yield return new WaitForSeconds(wait / 2);
                                    player.transform.position += moveX / 2;
                                    adjacentObjectU = GetAdjacentObjectU();
                                    adjacentObjectF = tentativeObject;

                                    if (adjacentObjectU != null && !IsObjectUnmovable(adjacentObjectU))
                                        playerDead = true;
                                    else
                                        adjacentObjectU = null;
                                    if (!playerDead)
                                        CheckFallingAndDropStone();
                                    this.animator.SetBool("walkingR", false);
                                    if (playerDead)
                                    {
                                        yield return PressingProcessInCoroutine();
                                    }
                                }
                                else
                                {
                                    gameManager.CallInoperable(wait / 2);
                                    breaking = true;
                                    breakingObject = tentativeObject;
                                    adjacentObjectF = tentativeObject;
                                    adjacentNameF = adjacentObjectF.name;
                                    breakingObject.GetComponent<StoneBehaviour>().SwitchOnBreakingNotFallFlag();
                                }
                            }
                            else if (player.transform.position.y - 0.5f == tentativeObject.transform.position.y)
                            {
                                CheckFallingAndDropStone();
                                touchingR = false;
                                gameManager.CallInoperable(wait);
                                this.animator.SetBool("walkingR", true);
                                player.transform.position += moveX / 2;
                                yield return new WaitForSeconds(wait / 2);
                                BreakStoneInCoroutine();
                                CheckFallingAndDropStone();
                                player.transform.position += moveX / 2;
                                this.animator.SetBool("walkingR", false);
                                yield return new WaitForSeconds(wait / 2);
                            }
                            else
                            {
                                CheckFallingAndDropStone();
                                gameManager.CallInoperable(wait / 4 + wait / 8);
                                yield return new WaitForSeconds(wait / 4);
                            }
                        }
                        else
                        {
                            CheckFallingAndDropStone();
                            if (adjacentObjectF != null && IsObjectUnmovable(adjacentObjectF))
                            {
                                gameManager.CallInoperable(wait / 4 + wait / 8);
                                yield return new WaitForSeconds(wait / 4);
                            }
                        }
                    }
                    else
                    {
                        gameManager.CallInoperable(wait / 2);
                        yield return new WaitForSeconds(wait / 2);
                    }
                }



                if (falling && !IsPlayerTouchingObjectR())
                {
                    Debug.Log("untouchedR433");
                    touchingR = false;
                }
            }

            if (!IsObjectUnmovable(adjacentObjectF))
                adjacentToMovable = true;

            Debug.Log("walkright " + "R =" + touchingR + "  " + "L =" + touchingL + "  "
           + "U =" + touchingU + "  " + "D =" + touchingD + "    objectName =" + adjacentNameF + " position = " + player.transform.position);

            playerPosition_after = player.transform.position;
            if (playerPosition_before == playerPosition_after)
                GamePlayAgents.unmoved = true;

            blueCrystalDistanceToGoal_after = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);

            if (blueCrystalDistanceToGoal_after < blueCrystalDistanceToGoal_before)
                GamePlayAgents.approached = true;
            else if (blueCrystalDistanceToGoal_after > blueCrystalDistanceToGoal_before)
                GamePlayAgents.backed = true;

                GamePlayAgents.moved = true;

            isRunninng = false;
        }

        private IEnumerator WalkLeft()
        {
            if (isRunninng) { yield break; }
            isRunninng = true;

            if (phase == 1)
            {
                CheckTouching();    
            }

            playerPosition_before = player.transform.position;
            blueCrystalDistanceToGoal_before = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);
            Debug.Log("phaseFlappy = " + phase++);

            notTouchUnderTheStone = false;
            bool notEnter1 = false; bool notEnter2 = false;
            this.animator.SetBool("turnedR", false);
            this.animator.SetBool("turnedU", false);
            this.animator.SetBool("turnedD", false);
            InitializeTouchFlag();
            CEprocessed = false;
            bool notFall = false;

            Debug.Log("currentAdjObjF = " + adjacentObjectF + "  name = " + adjacentNameF);

            if (adjacentNameF == "")
            {
                Debug.Log("changeNameDummy");
                adjacentNameF = dummy;
            }
            else if (adjacentNameF != dummy && adjacentObjectF == null
                && player.transform.position.y != GameObject.Find(adjacentNameF).transform.position.y)
            {
                Debug.Log("AdjFNameNotDummy429");
                adjacentNameF = dummy;
            }
            else if (adjacentNameF == dummy && touchingL)
            {
                adjacentObjectF = GetAdjacentObjectL();
                if (adjacentObjectF != null)
                    adjacentNameF = adjacentObjectF.name;
            }

            if (falling || !touchingL)
            {
                if (IsPlayerTouchingObjectL())
                {
                    touchingL = true;
                }
            }

            if (touchingL == false)
            {
                gameManager.CallInoperable(wait);
                this.animator.SetBool("walkingL", true);
                CheckFallingAndDropStone();
                player.transform.position -= moveX / 2;
                yield return new WaitForSeconds(wait / 2);
                BreakStoneInCoroutine();
                CheckFallingAndDropStone();
                player.transform.position -= moveX / 2;
                this.animator.SetBool("walkingL", false);
                if (falling)
                {
                    yield return new WaitForSeconds(wait / 2);
                }
                colCheckSwitch = true;
            }
            else if (touchingL == true && adjacentNameF != "dummy")
            {
                if (breaking && breakingObject.name == adjacentNameF)
                {
                    if (breaking2nd)
                        yield return new WaitForSeconds(wait / 64);

                    Debug.Log("stucking646 adjNameF = " + adjacentNameF);
                    CheckAdjacent_F_statusInBreakingL(breakingObject);
                    Debug.Log("stucking384 adjNameF = " + adjacentNameF);
                }
                if (breaking2nd && breakingObject2nd.name == adjacentNameF)
                {
                    CheckAdjacent_F_statusInBreakingL(breakingObject2nd);
                }
                else
                    adjacentObjectF = GameObject.Find(adjacentNameF);

                if (adjacentNameL != dummy)
                {
                    GameObject tentativeObj;
                    tentativeObj = GameObject.Find(adjacentNameL);
                    if (IsPlayerTouchingObjectL(tentativeObj))
                    {
                        adjacentNameF = adjacentNameL;
                        adjacentObjectF = tentativeObj;
                    }
                }

                if (!IsObjectUnmovable(adjacentNameF))
                {
                    Debug.Log("adjFStone688");
                    if (!IsObjectAdjacentedL(adjacentNameF))
                    {
                        if (!GetFallingStatus(adjacentObjectF)
                            || (IsObjectBreaking(adjacentObjectD) && !adjacentObjectD.GetComponent<StoneBehaviour>().breakingProcess2nd))
                        {
                            operationTimeList.Add(Field.time);
                            Debug.Log("adjF.position = " + GameObject.Find(adjacentNameF).transform.position);
                            gameManager.CallInoperable(wait);
                            this.animator.SetBool("walkingL", true);
                            player.transform.position -= moveX / 2;
                            adjacentObjectF.transform.position -= moveX;
                            pushedObjectHistory = adjacentObjectF;
                            CheckFallingAndDropStone();
                            notEnter2 = true;

                            if (!IsObjectTouchingUnderTheStone(adjacentObjectF))
                            {
                                MakeListOfFallingObject(adjacentObjectF);
                                Debug.Log("notTouchUstone = true693");
                                notTouchUnderTheStone = true;
                            }
                            GameObject tentativeObj = adjacentObjectF;
                            Debug.Log("WaitBeforeWalkLeft698");
                            yield return new WaitForSeconds(wait / 2);
                            Debug.Log("waitEndedWalkLeft699");
                            player.transform.position -= moveX / 2;
                            checkflagPlayerOverhead = true;
                            this.animator.SetBool("turnedL", true);

                            adjacentObjectU = GetAdjacentObjectU();
                            if (IsBlockReadyToFallOnPlayer(adjacentObjectU, tentativeObj))
                                playerDead = true;
                            else
                                adjacentObjectU = null;

                            if (!playerDead)
                                CheckFallingAndDropStone();

                            this.animator.SetBool("walkingL", false);

                            if (playerDead)
                            {
                                notTouchUnderTheStone = false;
                                yield return PressingProcessInCoroutine();
                            }

                            if (falling)
                            {
                                notFall = true;
                                notEnter2 = true;
                                yield return new WaitForSeconds(wait / 2);
                            }
                            colCheckSwitch = true;
                            if (breaking || breaking2nd)
                                BreakStoneInCoroutine();
                        }
                    }

                    else if (!breaking)
                    {
                        this.animator.SetBool("turnedL", true);
                        breaking = true;
                        breakingObject = adjacentObjectF;
                        Debug.Log("breakingWL427");
                        Debug.Log("IOA_bool =" + IsObjectAdjacentedF(adjacentNameF));
                    }
                    else if (breaking && breakingObject.name != adjacentNameF)
                    {
                        Debug.Log("turnedL470");
                        this.animator.SetBool("turnedL", true);
                        breakingObject2nd = adjacentObjectF;
                        breaking2nd = true;
                    }
                }
                else
                {
                    gameManager.CallInoperable(wait / 4 + wait / 8);
                    yield return new WaitForSeconds(wait / 4);
                    Debug.Log("displayAdjNameF432 " + adjacentNameF);
                    if (!this.animator.GetBool("turnedL"))
                    {
                        this.animator.SetBool("turnedL", true);
                        BreakStoneInCoroutine();
                        SwitchOn_NotBreak();
                    }
                }

                Debug.Log("R =" + touchingR + "  " + "L =" + touchingL + "  "
                    + "U =" + touchingU + "  " + "D =" + touchingD + "    objectName =" + adjacentNameF);
            }
            else if (adjacentNameL != "dummy")
            {
                adjacentObjectF = GameObject.Find(adjacentNameL);
                this.animator.SetBool("turnedL", true);

                if (player.gameObject.transform.position.y == adjacentObjectF.transform.position.y)
                {
                    if ((adjacentObjectF.tag == "Stone" && !GetFallingStatus(adjacentObjectF))
                            || (IsObjectBreaking(adjacentObjectD) && !adjacentObjectD.GetComponent<StoneBehaviour>().breakingProcess2nd))
                    {
                        if (IsObjectAdjacentedF(adjacentNameL))
                        {
                            if (!breaking)
                            {
                                breakingObject = adjacentObjectF;
                                breaking = true;
                                Debug.Log("breakingWL460");
                            }
                            else if (!breaking2nd)
                            {
                                breakingObject2nd = adjacentObjectF;
                                breaking2nd = true;
                            }
                        }
                        else
                        {
                            Debug.Log("moving337");
                            gameManager.CallInoperable(wait);
                            this.animator.SetBool("walkingL", true);
                            player.transform.position -= moveX / 2;
                            adjacentObjectF.transform.position -= moveX;
                            pushedObjectHistory = adjacentObjectF;
                            CheckFallingAndDropStone();

                            if (!IsObjectTouchingUnderTheStone(adjacentObjectF) && !GetFallingStatus(adjacentObjectF))
                            {
                                MakeListOfFallingObject(adjacentObjectF);
                                notTouchUnderTheStone = true;
                            }

                            GameObject tentativeObj = adjacentObjectF;
                            yield return new WaitForSeconds(wait / 2);
                            player.transform.position -= moveX / 2;
                            checkflagPlayerOverhead = true;
                            adjacentObjectU = GetAdjacentObjectU();

                            Debug.Log("adjObjU801 = " + adjacentObjectU + "tentativeObj = " + tentativeObj);
                            if (IsBlockReadyToFallOnPlayer(adjacentObjectU, tentativeObj))
                                playerDead = true;
                            else
                                adjacentObjectU = null;
                            if (!playerDead)
                                CheckFallingAndDropStone();

                            if (breaking || breaking2nd)
                                BreakStoneInCoroutine();
                            this.animator.SetBool("walkingL", false);

                            if (playerDead)
                            {
                                yield return PressingProcessInCoroutine();
                            }

                            if (falling)
                            {
                                yield return new WaitForSeconds(wait / 2);
                                notEnter2 = true;
                            }
                        }
                    }
                    adjacentNameF = adjacentNameL;
                    if (adjacentObjectF != null && adjacentObjectF.tag == stone && IsObjectAdjacentedF(adjacentNameL) && breaking && breakingObject.name != adjacentNameL)
                    {
                        adjacentNameL = "dummy";
                        adjacentNameR = "dummy";//左右に隣接するブロックを識別する必要は無くなったので初期化
                    }
                    colCheckSwitch = true;
                }

                Debug.Log("IsBreaking842 = " + breaking);
                adjacentObjectF = GetAdjacentObjectL();
                Debug.Log("adjObjF844 =" + adjacentObjectF);
                if (adjacentObjectF != null && adjacentObjectF.tag == stone && IsObjectAdjacentedF(adjacentNameL) && breaking && breakingObject.name != adjacentNameL
                    && breakingObject != adjacentObjectF)
                {
                    Debug.Log("adjLbreak2nd842");
                    breakingObject2nd = GameObject.Find(adjacentNameL);
                    breaking2nd = true;
                }
            }
            if (touchingL)
            {
                Debug.Log("626notEnter2 = " + notEnter2);
                GameObject tentativeObject = GetPlayerTouchingObjectL_checkDualTouch();

                if (!this.animator.GetBool("turnedL") && !notEnter1 && tentativeObject != null)
                {
                    this.animator.SetBool("turnedL", true);
                    notEnter1 = true;

                    if (!falling)
                    {
                        gameManager.CallInoperable(wait / 4 + wait / 8);
                        yield return new WaitForSeconds(wait / 4);
                    }
                    else
                    {
                        if (!notFall)
                            CheckFallingAndDropStone();
                        gameManager.CallInoperable(wait / 4 + wait / 8);
                        yield return new WaitForSeconds(wait / 4);
                    }
                }
                else if (this.animator.GetBool("turnedL") && !notEnter2 && falling)
                {
                    Debug.Log("fallProcessEntered880walkL");
                    if (!IsPlayerTouchingObjectL() && tentativeObject == null)
                    {
                        CheckFallingAndDropStone();
                        touchingL = false;
                        gameManager.CallInoperable(wait);
                        this.animator.SetBool("walkingL", true);
                        player.transform.position -= moveX / 2;
                        yield return new WaitForSeconds(wait / 2);
                        BreakStoneInCoroutine();
                        CheckFallingAndDropStone();
                        player.transform.position -= moveX / 2;
                        this.animator.SetBool("walkingL", false);
                        yield return new WaitForSeconds(wait / 2);
                    }
                    else if (IsPlayerTouchingObjectL() && tentativeObject != null)
                    {
                        Debug.Log("PTOL tentativeNotNull893");
                        if (!IsObjectUnmovable(tentativeObject))
                        {
                            if (player.transform.position.y + 0.5f == tentativeObject.transform.position.y)//空中ブロック押しの処理
                            {
                                CheckFallingAndDropStone();
                                if (!IsObjectAdjacentedL(tentativeObject))
                                {
                                    gameManager.CallInoperable(wait);
                                    this.animator.SetBool("walkingL", true);
                                    player.transform.position -= moveX / 2;
                                    tentativeObject.transform.position -= moveX;
                                    yield return new WaitForSeconds(wait / 2);
                                    player.transform.position -= moveX / 2;
                                    adjacentObjectU = GetAdjacentObjectU();
                                    adjacentObjectF = tentativeObject;
                                    if (adjacentObjectU != null && !IsObjectUnmovable(adjacentObjectU))
                                        playerDead = true;
                                    else
                                        adjacentObjectU = null;
                                    if (!playerDead)
                                        CheckFallingAndDropStone();
                                    this.animator.SetBool("walkingL", false);
                                    if (playerDead)
                                    {
                                        yield return PressingProcessInCoroutine();
                                    }
                                }
                                else
                                {
                                    gameManager.CallInoperable(wait / 2);
                                    Debug.Log("breakingMode928");
                                    breaking = true;
                                    breakingObject = tentativeObject;
                                    adjacentObjectF = tentativeObject;
                                    adjacentNameF = adjacentObjectF.name;
                                    breakingObject.GetComponent<StoneBehaviour>().SwitchOnBreakingNotFallFlag();
                                }
                            }
                            else if (player.transform.position.y - 0.5f == tentativeObject.transform.position.y)
                            {
                                CheckFallingAndDropStone();
                                touchingL = false;
                                gameManager.CallInoperable(wait);
                                this.animator.SetBool("walkingL", true);
                                player.transform.position -= moveX / 2;
                                yield return new WaitForSeconds(wait / 2);
                                BreakStoneInCoroutine();
                                CheckFallingAndDropStone();
                                player.transform.position -= moveX / 2;
                                this.animator.SetBool("walkingL", false);
                                yield return new WaitForSeconds(wait / 2);
                            }
                            else
                            {
                                CheckFallingAndDropStone();
                                if (adjacentObjectF != null && IsObjectUnmovable(adjacentObjectF))
                                {
                                    gameManager.CallInoperable(wait / 4 + wait / 8);
                                    yield return new WaitForSeconds(wait / 4);
                                }
                            }
                        }
                        else
                        {
                            CheckFallingAndDropStone();
                            gameManager.CallInoperable(wait / 4 + wait / 8);
                            yield return new WaitForSeconds(wait / 4);
                        }
                    }
                    else
                    {
                        gameManager.CallInoperable(wait / 2);
                        yield return new WaitForSeconds(wait / 2);
                    }
                }


                if (falling && !IsPlayerTouchingObjectL())
                {
                    touchingL = false;
                }
            }

            if (!IsObjectUnmovable(adjacentObjectF))
                adjacentToMovable = true;

            playerPosition_after = player.transform.position;
            if (playerPosition_before == playerPosition_after)
                GamePlayAgents.unmoved = true;

            blueCrystalDistanceToGoal_after = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);

            if (blueCrystalDistanceToGoal_after < blueCrystalDistanceToGoal_before)
                GamePlayAgents.approached = true;
            else if (blueCrystalDistanceToGoal_after > blueCrystalDistanceToGoal_before)
                GamePlayAgents.backed = true;


            GamePlayAgents.moved = true;

            isRunninng = false;

            Debug.Log("adjObjF_walkL.end981 = " + adjacentObjectF + "name = " + adjacentNameF);
        }
        private IEnumerator WalkUp()
        {
            if (isRunninng) { yield break; }
            isRunninng = true;

            if (phase == 1)
            {
                CheckTouching();    
            }

            playerPosition_before = player.transform.position;
            blueCrystalDistanceToGoal_before = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);
            Debug.Log("phaseFlappy = " + phase++);

            Debug.Log("moveToUp643");
            this.animator.SetBool("turnedR", false);
            this.animator.SetBool("turnedL", false);
            this.animator.SetBool("turnedD", false);
            InitializeTouchFlag();
            Debug.Log("658R =" + touchingR + "  " + "L =" + touchingL + "  "
                           + "U =" + touchingU + "  " + "D =" + touchingD);
            bool notFall = false;

            if (!falling && blockAbovePlayer)
                blockAbovePlayer = false;

            if ((adjacentObjectU != null && !adjacentObjectU.GetComponent<StoneBehaviour>().breakingProcess3rd)
                || adjacentObjectU == null)
            {
                if (upperStoneObj == null && adjacentObjectU != null && this.animator.GetBool("turnedU")
                        && (adjacentObjectU == breakingObject || adjacentObjectU == breakingObject2nd)
                        && adjacentObjectU.GetComponent<StoneBehaviour>().breakingProcess1st
                        && !adjacentObjectU.GetComponent<StoneBehaviour>().breakingProcess2nd)
                {
                    upperStoneObj = GetTouchingObjectUpperTheStone(adjacentObjectU);
                }

                CheckFallingAndDropStone();
                if (adjacentObjectU != null)
                {
                    if (!breaking)
                    {
                        breaking = true;
                        breakingObject = adjacentObjectU;
                    }
                    else if (breaking && breaking2nd)
                    {
                        breaking2nd = true;
                        breakingObject2nd = adjacentObjectU;
                    }
                }
            }

            if (!touchingU && (IsPlayerTouchingObjectU() || blockAbovePlayer))
            {
                touchingU = true;
            }
            else if (!blockAbovePlayer)
                notFall = true;

            CEprocessed = false;

            if (!touchingU)
            {
                Debug.Log("waitBrfore501");
                gameManager.CallInoperable(wait);
                Debug.Log("waitAfter503");
                fStepSwitch = !fStepSwitch;
                this.animator.SetBool("walkingU", true);
                if (!notFall)
                    CheckFallingAndDropStone();
                player.transform.position += moveY / 2;

                operationTimeList.Add(Field.time);

                yield return new WaitForSeconds(wait / 2);
                CheckFallingAndDropStone();
                player.transform.position += moveY / 2;
                this.animator.SetBool("walkingU", false);
                this.animator.SetBool("firstStep", fStepSwitch);
                colCheckSwitch = true;
                BreakStoneInCoroutine();
            }
            else
            {
                Debug.Log("displayAdjObjU706 = " + adjacentObjectU);
                bool isTouchingU = false;
                Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);

                if (adjacentObjectU == null)
                    adjacentObjectU = GetAdjacentObjectU(collisions);
                if (IsObjectUnmovable(adjacentObjectU))
                    adjacentObjectU = null;

                if (adjacentObjectU != null)
                {
                    Debug.Log("displayAdjObjU1073 = " + adjacentObjectU + "  position = " + adjacentObjectU.transform.position);

                    foreach (Collider2D checkObj in collisions)
                    {
                        if (checkObj != player)
                        {
                            if (falling)
                            {
                                if (IsPlayerTouchingObjectU(checkObj.gameObject))
                                    isTouchingU = true;
                            }
                            if (checkObj != null && checkObj.gameObject != player)
                            {
                                Debug.Log("objNameWalkUp694 = " + checkObj);
                                if (IsPlayerTouchingObjectU(checkObj.gameObject) && checkObj.tag != wall && checkObj.tag != halfBlock)
                                {
                                    Debug.Log("ObjTouhingU_entered1087");
                                    isTouchingU = true;
                                    this.animator.SetBool("turnedU", true);
                                    Debug.Log("adjU680");
                                    if (adjacentObjectU == null)
                                    {
                                        adjacentObjectU = checkObj.gameObject;
                                    }
                                    bool breakSwitch1st = false;
                                    bool breakSwitch2nd = false;
                                    if (!breaking && !breaking2nd)
                                    {
                                        breaking = true;
                                        breakingObject = adjacentObjectU;
                                        CheckFallingAndDropStone();
                                        breakSwitch1st = true;
                                        Debug.Log("upperObjIsBreaking1103");
                                    }
                                    else if (!breaking2nd && breakingObject != adjacentObjectU)//二個目のオブジェクトを破壊する時の動作 １つ目の破壊オブジェクトと一致していないかどうか                
                                    {
                                        breaking2nd = true;
                                        breakingObject2nd = adjacentObjectU;
                                        CheckFallingAndDropStone();
                                        breakSwitch2nd = true;
                                    }
                                    if (breakSwitch1st || breakSwitch2nd)
                                    {
                                        upperStoneObj = GetTouchingObjectUpperTheStone(checkObj.gameObject);
                                        Debug.Log("upperStoneObj1111 = " + upperStoneObj);
                                        if (upperStoneObj != null && !IsObjectUnmovable(upperStoneObj) && !GetFallingStatus(upperStoneObj))
                                        {
                                            MakeListOfFallingObject(upperStoneObj);
                                        }
                                        if (breakSwitch1st)
                                            breakingUpperLink = true;
                                        else if (breakSwitch2nd)
                                            breakingUpperLink2nd = true;
                                    }
                                    Debug.Log("1122breakingObect = " + breakingObject + " breakingObject2nd = " + breakingObject2nd + " adjObjU = " + adjacentObjectU);
                                    if ((breakingObject != null && breakingObject == adjacentObjectU && breakingObject.GetComponent<StoneBehaviour>().breakingProcess2nd
                                            && !breakingObject.GetComponent<StoneBehaviour>().breakingProcess3rd)
                                                || (breakingObject2nd != null && breakingObject2nd == adjacentObjectU && breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess2nd
                                                    && !breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess3rd))
                                    {
                                        Debug.Log("breakProcess2ndWUp729");
                                        upperStoneObj = GetTouchingObjectUpperTheStone(checkObj.gameObject);
                                        if (upperStoneObj != null && !IsObjectUnmovable(upperStoneObj))
                                        {
                                            Debug.Log("breakProcess2ndfallObj736");
                                            if ((breakingObject != null && !breakingObject.GetComponent<StoneBehaviour>().breakingProcess3rd)
                                                || (breakingObject2nd != null && !breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess3rd))
                                            {
                                                MakeListOfFallingObject(upperStoneObj);
                                            }
                                        }
                                        if ((breakingObject == adjacentObjectU && breakingObject.GetComponent<StoneBehaviour>().breakingProcess3rd)
                                            || (breakingObject2nd == adjacentObjectU && breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess3rd))
                                        {
                                            Debug.Log("bprocess3rd742");
                                            gameManager.CallInoperable(wait / 2);
                                            yield return new WaitForSeconds(wait / 2);
                                        }

                                    }

                                }
                                if (!this.animator.GetBool("turnedU") && IsPlayerTouchingObjectU(checkObj.gameObject))
                                {
                                    gameManager.CallInoperable(wait / 4 + wait / 8);
                                    yield return new WaitForSeconds(wait / 4);
                                    this.animator.SetBool("turnedU", true);
                                    isTouchingU = true;
                                    break;
                                }

                            }
                        }
                    }
                    Debug.Log("IsBreaking1st = " + breaking + "  adjObjU1166 = " + adjacentObjectU);
                    if (adjacentObjectU != null && (breaking || breaking2nd))
                    {
                        Debug.Log("adjObjUBreakingMode1168");
                        if (adjacentObjectU.GetComponent<StoneBehaviour>().breakingProcess3rd)
                        {
                            gameManager.CallInoperable(wait / 4 + wait / 8);
                            yield return new WaitForSeconds(wait / 4);
                        }
                        else if (IsObjectBreaking(adjacentObjectU))
                        {
                            Debug.Log("adjObjUBreakingWait1175");
                            gameManager.CallInoperable(wait / 2);
                            yield return new WaitForSeconds(wait / 2);
                        }
                    }
                }
                else
                {
                    Debug.Log("entered866WalkD");
                    bool enterFlag1 = false;
                    bool enterFlag2 = false;
                    foreach (Collider2D checkObj in collisions)
                    {
                        if (this.animator.GetBool("turnedU") && !IsPlayerTouchingObjectU(checkObj.gameObject))
                        {
                            enterFlag1 = true;
                        }
                        else if (this.animator.GetBool("turnedU") && IsPlayerTouchingObjectU(checkObj.gameObject))
                        {
                            enterFlag1 = false;
                            enterFlag2 = true;
                            break;
                        }
                    }

                    if (enterFlag1)
                    {
                        Debug.Log("turnedUwait816");
                        CheckFallingAndDropStone();
                        gameManager.CallInoperable(wait / 2);
                        yield return new WaitForSeconds(wait / 2);
                    }
                    else if (enterFlag2)
                    {
                        Debug.Log("enterFlag2 Process892");
                        gameManager.CallInoperable(wait / 2);
                        yield return new WaitForSeconds(wait / 2);
                    }
                    else
                    {
                        this.animator.SetBool("turnedU", true);
                        gameManager.CallInoperable(wait / 4 + wait / 8);
                        yield return new WaitForSeconds(wait / 4);
                    }
                }

                if (falling && !isTouchingU)
                {
                    if (breaking || breaking2nd)
                    {
                        if (adjacentObjectU == breakingObject || adjacentObjectU == breakingObject2nd)
                            touchingU = true;
                    }
                    else
                    {
                        Debug.Log("touchingU_false783");
                        touchingU = false;
                    }
                }
            }
            objLR_CheckSwitch = true;

            playerPosition_after = player.transform.position;
            if (playerPosition_before == playerPosition_after)
                GamePlayAgents.unmoved = true;

            playerPosition_after = player.transform.position;
            if (playerPosition_before == playerPosition_after)
                GamePlayAgents.unmoved = true;

                GamePlayAgents.moved = true;

            isRunninng = false;
        }

        private IEnumerator WalkDown()
        {
            if (isRunninng) { yield break; }
            isRunninng = true;

            if (phase == 1)
            {
                CheckTouching();
            }

            playerPosition_before = player.transform.position;
            blueCrystalDistanceToGoal_before = Vector2.Distance(blueArea.transform.position, blueCrystal.transform.position);
            Debug.Log("phaseFlappy = " + phase++);

            this.animator.SetBool("turnedR", false);
            this.animator.SetBool("turnedL", false);
            this.animator.SetBool("turnedU", false);
            InitializeTouchFlag();
            CEprocessed = false;

            if (touchingD && falling && !IsPlayerTouchingObjectD())
            {
                touchingD = false;
            }

            if (!touchingD)
            {
                Debug.Log("walkD631");
                gameManager.CallInoperable(wait);
                fStepSwitch = !fStepSwitch;
                this.animator.SetBool("walkingD", true);
                CheckFallingAndDropStone();
                player.transform.position -= moveY / 2;
                yield return new WaitForSeconds(wait / 2);
                CheckFallingAndDropStone();
                player.transform.position -= moveY / 2;
                this.animator.SetBool("walkingD", false);
                this.animator.SetBool("firstStep", fStepSwitch); colCheckSwitch = true; BreakStoneInCoroutine();
            }
            else if (touchingD && !IsObjectUnmovable(GameObject.Find(adjacentNameD)) && IsObjectAdjacentedD(adjacentNameD))
            {
                Debug.Log("touchingDwalkD484  " + adjacentNameD + "playerPos =" + player.transform.position);
                this.animator.SetBool("turnedD", true);
                if (adjacentNameD != "dummy")
                {
                    adjacentObjectD = GameObject.Find(adjacentNameD);
                    if (adjacentObjectD.tag == stone)
                    {
                        Debug.Log("enteredStonetagZone1108");
                        if (!breaking && !breaking2nd)
                        {
                            breaking = true;
                            breakingObject = adjacentObjectD;
                            GameObject tentativeObj = GetTouchingObjectUpperTheStone_prioritizingUnmovable(adjacentObjectD);
                            if (tentativeObj != null && !IsObjectUnmovable(tentativeObj))
                            {
                                upperStoneObj = tentativeObj;
                                MakeListOfFallingObject(upperStoneObj);
                            }
                            CheckFallingAndDropStone();
                        }
                        else if (!breaking2nd && breakingObject != null && breakingObject.name != adjacentNameD)//二個目のオブジェクトを破壊する時の動作 １つ目の破壊オブジェクトと一致していないかどうか                
                        {
                            breaking2nd = true;
                            breakingObject2nd = adjacentObjectD;
                            GameObject tentativeObj = GetTouchingObjectUpperTheStone_prioritizingUnmovable(adjacentObjectD);
                            if (tentativeObj != null && !IsObjectUnmovable(tentativeObj))
                            {
                                upperStoneObj = tentativeObj;
                                MakeListOfFallingObject(upperStoneObj);
                            }
                            CheckFallingAndDropStone();
                        }
                        else if ((breakingObject == adjacentObjectD && breakingObject.GetComponent<StoneBehaviour>().breakingProcess2nd)
                                 || (breakingObject2nd == adjacentObjectD && breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess2nd))
                        {
                            adjacentObjectU = GetAdjacentObjectU_CheckFalling();
                            if (adjacentObjectU != null && !IsObjectUnmovable(adjacentObjectU) && !IsObjectBreaking(adjacentObjectU))
                            {
                                playerDead = true;
                            }

                            if (playerDead)
                            {
                                Debug.Log("playerDeadWalkUpBreakingStone1285");
                                yield return PressingProcessInCoroutine();
                            }

                            Debug.Log("breakingwalkD499  adjNameL = " + adjacentNameL);
                            gameManager.CallInoperable(wait);
                            fStepSwitch = !fStepSwitch;
                            this.animator.SetBool("walkingD", true);
                            player.transform.position -= moveY / 2;

                            if (adjacentNameL != dummy && !adjacentNameL.Contains("wall")) //壁と半ブロックは除外
                            {
                                adjacentObjectL = GameObject.Find(adjacentNameL);
                                Debug.Log("notContainsWall755");
                            }
                            else if (adjacentNameR != dummy && !adjacentNameR.Contains("wall")) //壁と半ブロックは除外
                            {
                                adjacentObjectR = GameObject.Find(adjacentNameR);
                            }
                            CheckFallingAndDropStone();
                            yield return new WaitForSeconds(wait / 2);
                            CheckFallingAndDropStone();
                            BreakStoneInCoroutine();
                            player.transform.position -= moveY / 2;

                            this.animator.SetBool("walkingD", false);
                            this.animator.SetBool("firstStep", fStepSwitch);
                        }
                        else if (this.animator.GetBool("turnedD"))
                        {
                            Debug.Log("turnedUfallStone1161");
                            CheckFallingAndDropStone();
                        }
                    }
                }
                if (dualTouchingD)
                {
                    Debug.Log("breakDouble556 breakingObj = " + breakingObject + "breakingObj2nd = " + breakingObject2nd);
                    Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
                    foreach (Collider2D checkObj in collisions)
                    {
                        if (checkObj.tag == stone && !touchingWall)
                        {
                            if (!breaking)
                            {
                                breaking = true;
                                breakingObject = checkObj.gameObject;
                            }
                            else if (!breaking2nd && checkObj.gameObject != breakingObject)
                            {
                                breaking2nd = true;
                                breakingObject2nd = checkObj.gameObject;
                            }
                        }
                    }
                }
            }
            else
            {
                Debug.Log("lastProcessWalkD1026");
                bool enterFlag1 = false;
                bool enterFlag2 = false;
                bool isTouchingD = false;
                Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
                foreach (Collider2D checkObj in collisions)
                {
                    if (falling)
                    {
                        if (IsPlayerTouchingObjectD(checkObj.gameObject))
                            isTouchingD = true;
                    }

                    if (!this.animator.GetBool("turnedD") && IsPlayerTouchingObjectD(checkObj.gameObject))
                    {
                        gameManager.CallInoperable(wait / 4 + wait / 8);
                        yield return new WaitForSeconds(wait / 8);
                        CheckFallingAndDropStone();
                        this.animator.SetBool("turnedD", true);
                        break;
                    }
                    else if (this.animator.GetBool("turnedD"))
                    {
                        if (!IsPlayerTouchingObjectD(checkObj.gameObject))
                        {
                            enterFlag1 = true;
                        }
                        else
                        {
                            enterFlag1 = false;
                            enterFlag2 = true;
                            break;
                        }
                    }
                }
                if (enterFlag1)
                {
                    CheckFallingAndDropStone();
                    gameManager.CallInoperable(wait / 2);
                    yield return new WaitForSeconds(wait / 2);
                    Debug.Log("faceToObjectD1065");
                }
                else if (enterFlag2)
                {
                    CheckFallingAndDropStone();
                    gameManager.CallInoperable(wait / 2);
                    yield return new WaitForSeconds(wait / 2);
                }
                if (falling && !isTouchingD)
                    touchingD = false;
            }
            playerPosition_after = player.transform.position;
            if (playerPosition_before == playerPosition_after)
                GamePlayAgents.unmoved = true;

            playerPosition_after = player.transform.position;
            if (playerPosition_before == playerPosition_after)
                GamePlayAgents.unmoved = true;

                GamePlayAgents.moved = true;

            objLR_CheckSwitch = true;
            isRunninng = false;
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if (!notEnterCE)
            {
                if (CEprocessed)
                    CEprocessed = false;
                else
                {
                    if (!objLR_CheckSwitch)
                    {
                        Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
                        bool adjacentedF = false;//隣接しているブロックが見つかればtrue
                        Debug.Log("FalseA");
                        CheckFlagAndInitializeTouching();

                        foreach (Collider2D checkObj in collisions)
                        {
                            if (!objLR_CheckSwitch && IsBlockAdjacentFB(checkObj.gameObject))
                            {

                                if (checkObj.gameObject.transform.position.x == this.transform.position.x + 1)
                                {
                                    touchingR = true; touchFlagR = true;
                                    if (this.animator.GetBool("turnedR"))
                                    {
                                        adjacentNameF = checkObj.gameObject.name;
                                    }
                                    else
                                        adjacentNameR = checkObj.gameObject.name;
                                }
                                else if (checkObj.gameObject.transform.position.x + 1 == this.transform.position.x)
                                {
                                    touchingL = true; touchFlagL = true;
                                    if (this.animator.GetBool("turnedL"))
                                        adjacentNameF = checkObj.gameObject.name;
                                    else
                                        adjacentNameL = checkObj.gameObject.name;
                                }
                            }
                            else if (IsPlayerTouchingObjectD(checkObj.gameObject))
                            {
                                VallidationOfAdjacentNameD(checkObj);
                                touchingD = true; touchFlagD = true;
                            }
                            else if (checkObj.tag == halfBlock && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f)
                            {
                                if (checkObj.gameObject.transform.position.x == this.transform.position.x + 1)
                                {
                                    touchingR = true; touchFlagR = true;
                                }
                                else if (checkObj.gameObject.transform.position.x == this.transform.position.x - 1)
                                {
                                    touchingL = true; touchFlagL = true;
                                }
                            }
                            else
                            {
                                CheckTouching(adjacentedF, checkObj);
                            }
                            Debug.Log("after" + adjacentNameF);
                            colCheckSwitch = false;
                            CEprocessed = true;
                        }
                    }
                }

                if (objLR_CheckSwitch)
                {
                    int dualCounter = 0;
                    touchingWall = false;
                    Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
                    foreach (Collider2D checkObj in collisions)
                    {
                        int count = 0;//前後隣接のオブジェクトを検出したら終了
                        if (player.gameObject.transform.position.y == checkObj.transform.position.y && count != 3)
                        {
                            if (player.gameObject.transform.position.x + 1 == checkObj.transform.position.x)
                            {
                                adjacentNameL = checkObj.name;
                                touchingL = true; touchFlagL = true;
                                count++;
                                Debug.Log("tL");
                            }
                            else if (player.gameObject.transform.position.x - 1 == checkObj.transform.position.x)
                            {
                                adjacentNameR = checkObj.name;
                                touchingR = true; touchFlagR = true;
                                count++;
                                Debug.Log("tR");
                            }
                        }
                        if (IsPlayerTouchingObjectD(checkObj.gameObject))
                        {
                            if (checkObj.tag == wall || checkObj.tag == halfBlock)
                                touchingWall = true;
                            dualCounter = CheckDual(dualCounter);
                            VallidationOfAdjacentNameD(checkObj);
                            touchingD = true; touchFlagD = true;
                            Debug.Log("TouchingDonCE645");
                        }
                        if (IsPlayerTouchingObjectU(checkObj.gameObject))
                        {
                            touchingU = true; touchFlagU = true;
                        }
                    }
                    objLR_CheckSwitch = false;
                    CEprocessed = true;
                }
            }
        }

        void OnCollisionStay2D(Collision2D other)
        {
            if (!isRunninng)
            {
                if (colCheckSwitch)
                {
                    int dualCounter = 0;
                    touchingWall = false;
                    Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
                    Debug.Log("colCheck");
                    bool adjacentedF = false;//隣接しているブロックが見つかればtrue
                    CheckFlagAndInitializeTouching();

                    foreach (Collider2D checkObj in collisions)
                    {
                        Debug.Log("checkObjOCSFirstProcess1540 + " + checkObj);
                        if (checkObj.gameObject != player)
                        {
                            if (!adjacentedF && !objLR_CheckSwitch && IsBlockAdjacentFB(checkObj.gameObject))
                            {
                                Debug.Log("collidingObjAdjFB1545 = " + checkObj + "  OBjPosition = "
                                            + checkObj.transform.position + "playerPosition = " + player.transform.position);
                                if (IsBlockAdjacentR(player, checkObj.gameObject))
                                {
                                    touchingR = true; touchingR = true;
                                    if (keyHistory == "right")
                                        adjacentNameF = checkObj.gameObject.name;

                                    adjacentedF = true;
                                }
                                else if (IsBlockAdjacentL(player, checkObj.gameObject))
                                {
                                    touchingL = true; touchFlagL = true;
                                    if (keyHistory == "left")
                                        adjacentNameF = checkObj.gameObject.name;
                                    adjacentedF = true;
                                }
                            }
                            else if (IsPlayerTouchingObjectD(checkObj.gameObject))
                            {

                                if (checkObj.tag == wall || checkObj.tag == halfBlock)
                                    touchingWall = true;
                                dualCounter = CheckDual(dualCounter);
                                touchingD = true; touchFlagD = true;
                                VallidationOfAdjacentNameD(checkObj);
                            }
                            else if (checkObj.tag == halfBlock && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f)
                            {
                                if (checkObj.gameObject.transform.position.x == this.transform.position.x + 1)
                                {
                                    adjacentNameR = checkObj.name;
                                    touchingR = true; touchFlagR = true;
                                }
                                else if (checkObj.gameObject.transform.position.x == this.transform.position.x - 1)
                                {
                                    adjacentNameL = checkObj.name;
                                    touchingL = true; touchFlagL = true;
                                }
                            }
                            else
                            {
                                Debug.Log("checkObjOCS_else1588 = " + checkObj + "playerPos = " + player.transform.position);
                                CheckTouching(adjacentedF, checkObj);//接しているブロックがあるかどうかの判別とTouching変数の更新
                            }
                        }
                    }
                    colCheckSwitch = false;

                    GamePlayAgents.moved = true;
                    Debug.Log("moved1827");

                }

                if (objLR_CheckSwitch)
                {
                    int dualCounter = 0;
                    touchingWall = false;
                    Debug.Log("objLRProcessNotEnter709");
                    Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
                    adjacentNameF = "dummy";
                    adjacentNameD = "dummy";
                    bool adjacentedR = false; bool adjacentedL = false;
                    foreach (Collider2D checkObj in collisions)
                    {
                        int count = 0;//前後隣接+上下のいずれか１のオブジェクトを検出したら終了
                        if (checkObj.gameObject != player && (player.gameObject.transform.position.y == checkObj.transform.position.y
                            || (checkObj.tag == "HalfBlock" && Mathf.Abs(checkObj.gameObject.transform.position.y - player.transform.position.y) <= 0.25f)) && count != 3)
                        {
                            Debug.Log("objLRProcess719 checkObj = " + checkObj);
                            if (checkObj.tag != "HalfBlock" || player.gameObject.transform.position.y <= checkObj.transform.position.y)
                            {
                                if (player.gameObject.transform.position.x + 1 == checkObj.transform.position.x)
                                {
                                    adjacentNameR = checkObj.gameObject.name;
                                    touchingR = true; touchFlagR = true;
                                    Debug.Log("touchingR");
                                    count++;
                                    adjacentedR = true;
                                }
                                else if (player.gameObject.transform.position.x - 1 == checkObj.transform.position.x)
                                {
                                    adjacentNameL = checkObj.gameObject.name;
                                    touchingL = true; touchFlagL = true;
                                    count++;
                                    Debug.Log("touchingL");
                                    adjacentedL = true;
                                }
                            }
                        }
                        if (!adjacentedR && adjacentNameR != "dummy" && GameObject.Find(adjacentNameR)?.tag != halfBlock)
                        {
                            adjacentNameR = "dummy";
                            Debug.Log("dummiedAdjNameR758");
                        }
                        if (!adjacentedL && adjacentNameL != "dummy" && GameObject.Find(adjacentNameL)?.tag != halfBlock)
                            adjacentNameL = "dummy";

                        if (Mathf.Abs(checkObj.gameObject.transform.position.x - player.transform.position.x) <= 0.5f
                            && (player.gameObject.transform.position.y - 1 == checkObj.transform.position.y
                            || (checkObj.tag == "HalfBlock" && player.transform.position.y > checkObj.gameObject.transform.position.y)))
                        {
                            if (checkObj.tag == wall || checkObj.tag == halfBlock)
                                touchingWall = true;
                            dualCounter = CheckDual(dualCounter);
                            VallidationOfAdjacentNameD(checkObj);
                            touchingD = true; touchFlagD = true;
                            Debug.Log("onColStayTouchD806");
                        }
                    }
                    objLR_CheckSwitch = false;

                    if (adjacentObjectF != null)
                        adjacentObjectF = null;

                    if (phase > 0)
                        GamePlayAgents.moved = true;
                    Debug.Log("moved1893");

                }

                if (breakingObject2nd == null)
                    notEnterCE = true;

                if (fallingObjTouchedDown)
                {
                    CheckTouching();
                    fallingObjTouchedDown = false;
                }
                if (b_CrystalGenerator.blueCrystal != null &&
                    ((b_areaGenerator.blueArea != null && IsBlockAdjacentD(b_CrystalGenerator.blueCrystal, b_areaGenerator.blueArea))
                        || (b_area_halfGenerator.blueArea_half != null && IsBlockAdjacentD(b_CrystalGenerator.blueCrystal, b_area_halfGenerator.blueArea_half))
                       ) && solvedTrueCount < 1
                    )
                {
                    GamePlayAgents.solved = true;
                    solvedTrueCount++;
                }
            }
        }

        int CheckDual(int dualCounter)
        {
            dualCounter++;
            if (dualCounter == 2)
                dualTouchingD = true;
            else
                dualTouchingD = false;
            return dualCounter;
        }

        bool IsBlockAdjacentFB(GameObject go)//ブロックが左右に隣接しているかを判定
        {
            if ((player.gameObject.transform.position.x + 1 == go.gameObject.transform.position.x
                || player.gameObject.transform.position.x - 1 == go.gameObject.transform.position.x)
                    && player.gameObject.transform.position.y == go.gameObject.transform.position.y)
                return true;
            else
                return false;
        }

        bool IsBlockTouchingFB(GameObject go)
        {
            if ((player.gameObject.transform.position.x + 1 == go.gameObject.transform.position.x
               || player.gameObject.transform.position.x - 1 == go.gameObject.transform.position.x)
                   && Mathf.Abs(player.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f)
                return true;
            else
                return false;
        }

        bool IsBlockAdjacentUD(GameObject go)//ブロックが上下に隣接しているか判定
        {
            if ((player.gameObject.transform.position.y + 1 == go.gameObject.transform.position.y
                || player.gameObject.transform.position.y - 1 == go.gameObject.transform.position.y)
                    && Mathf.Abs(go.gameObject.transform.position.x - player.transform.position.x) <= 0.5f)
                return true;
            else
                return false;
        }

        void OnCollisionExit2D(Collision2D other)
        {
            Debug.Log("Destroyed = " + destroyed + "  exitObj = " + other.gameObject);
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            if (collisions.Length == 1)
            {//プレイヤーもcollisionsに含まれるため
                touchingR = false; touchingL = false;
                touchingU = false; touchingD = false;
                adjacentNameF = "dummy";
                notEnterCE = false;
                Debug.Log("exit");
            }
            if (destroyed)
            {
                touchingR = false; touchingL = false;
                touchingU = false; touchingD = false;
                foreach (Collider2D checkObj in collisions)
                {
                    Debug.Log("checkObj1059 = " + checkObj);
                    CheckTouching(checkObj);
                }
                Debug.Log("Destroyed");
                destroyed = false;
            }
            if (falling && adjacentObjectF != null)
            {
                Debug.Log("getBoolTurnedR1724 = " + this.animator.GetBool("turnedR"));
                if (!IsBlockAdjacentFB(adjacentObjectF) && this.animator.GetBool("turnedR"))
                {
                    adjacentNameF = dummy;
                    adjacentObjectF = null;
                    adjacentNameR = dummy;
                }
                else if (!IsBlockAdjacentFB(adjacentObjectF) && this.animator.GetBool("turnedL"))
                {
                    adjacentNameF = dummy;
                    adjacentObjectF = null;
                    adjacentNameL = dummy;
                }
            }

            if (!IsExistAtThePosition(player.transform.position - 2 * moveY)
                || !IsExistInTheRangeX(moveX.x))
            {
                adjacentObjectD = null;
                adjacentNameD = "dummy";
            }
        }

        bool IsObjectAdjacentedF(string objName)
        {//プレーヤー以外を除いたオブジェクトに隣接しているオブジェクトがあればtrue
            if (objName == "dummy")
            {
                Debug.Log("dummy707");
                return false;
            }
            else
            {
                GameObject go = GameObject.Find(objName);

                Debug.Log("process712,go:" + go.name);
                Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
                if (collisions == null)
                {
                    Debug.Log("icFD_null");
                    return false;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    Debug.Log("checkObj : " + checkObj);
                    if (checkObj.gameObject != player && (checkObj.gameObject.transform.position.x + 1 == go.transform.position.x
                        || checkObj.gameObject.transform.position.x - 1 == go.transform.position.x)
                        && (Mathf.Abs(checkObj.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f
                        || (checkObj.tag == halfBlock && Mathf.Abs(checkObj.gameObject.transform.position.y - go.transform.position.y) <= 0.25f)))
                        return true;
                }
            }
            Debug.Log("ioa_false");
            return false;
        }

        bool IsObjectAdjacentedR(string objName)
        {//プレーヤー以外を除いたオブジェクトに隣接しているオブジェクトがあればtrue
            if (objName == "dummy")
            {
                return false;
            }
            else
            {
                GameObject go = GameObject.Find(objName);
                Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
                if (collisions == null)
                {
                    return false;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    if (checkObj.gameObject != player && checkObj.gameObject.transform.position.x == go.transform.position.x + 1
                        && (Mathf.Abs(checkObj.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f
                        || (checkObj.tag == halfBlock && Mathf.Abs(checkObj.gameObject.transform.position.y - go.transform.position.y) <= 0.25f)))
                        return true;
                }
            }
            return false;
        }

        bool IsObjectAdjacentedR(GameObject go)
        {//プレーヤー以外を除いたオブジェクトに隣接しているオブジェクトがあればtrue
            if (go == null)
            {
                return false;
            }
            else
            {
                Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
                if (collisions == null)
                {
                    return false;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    if (checkObj.gameObject != player && checkObj.gameObject.transform.position.x == go.transform.position.x + 1
                        && Mathf.Abs(checkObj.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f
                        || (checkObj.tag == halfBlock && Mathf.Abs(checkObj.gameObject.transform.position.y - go.transform.position.y) <= 0.25f))
                        return true;
                }
            }
            return false;
        }

        bool IsObjectAdjacentedL(string objName)
        {//プレーヤー以外を除いたオブジェクトに隣接しているオブジェクトがあればtrue
            if (objName == "dummy")
            {
                return false;
            }
            else
            {
                GameObject go = GameObject.Find(objName);
                Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
                if (collisions == null)
                {
                    return false;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    if (checkObj.gameObject != player && checkObj.gameObject.transform.position.x == go.transform.position.x - 1
                        && Mathf.Abs(checkObj.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f
                        || (checkObj.tag == halfBlock && Mathf.Abs(checkObj.gameObject.transform.position.y - go.transform.position.y) <= 0.25f))
                        return true;
                }
            }
            return false;
        }

        bool IsObjectAdjacentedL(GameObject go)
        {//プレーヤー以外を除いたオブジェクトに隣接しているオブジェクトがあればtrue
            if (go == null)
            {
                return false;
            }
            else
            {
                Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
                if (collisions == null)
                {
                    return false;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    if (checkObj.gameObject != player && checkObj.gameObject.transform.position.x == go.transform.position.x - 1
                        && Mathf.Abs(checkObj.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f
                        || (checkObj.tag == halfBlock && Mathf.Abs(checkObj.gameObject.transform.position.y - go.transform.position.y) <= 0.25f))
                        return true;
                }
            }
            return false;
        }


        bool IsObjectAdjacentedD(string objName)
        {//プレーヤー以外を除いたオブジェクトに隣接しているオブジェクトがあればtrue
            Debug.Log("IOAD875 " + adjacentNameD);
            if (objName == "dummy")
            {
                return false;
            }
            else
            {
                GameObject go = GameObject.Find(objName);
                if (go.tag != wall || go.tag != halfBlock)
                {
                    Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
                    if (collisions == null)
                    {
                        return false;
                    }
                    foreach (Collider2D checkobj in collisions)
                    {
                        if (((checkobj.gameObject != player && checkobj.gameObject.transform.position.y == go.transform.position.y - 1)
                            || (checkobj.tag == halfBlock && checkobj.gameObject.transform.position.y == go.transform.position.y - 0.75))
                            && Mathf.Abs(checkobj.gameObject.transform.position.x - go.transform.position.x) <= 0.5f)
                        {
                            Debug.Log("IOAD checkObj.pos = " + checkobj.transform.position + "  player.pos = " + player.transform.position);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        bool IsObjectAdjacentedU(GameObject go)
        {//プレーヤー以外を除いたオブジェクトに隣接しているオブジェクトがあればtrue
            Debug.Log("IOAD875 " + adjacentNameD);
            if (go == null)
            {
                return false;
            }
            else
            {
                if (go.tag != wall || go.tag != halfBlock)
                {
                    Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
                    if (collisions == null)
                    {
                        return false;
                    }
                    foreach (Collider2D checkobj in collisions)
                    {
                        if (((checkobj.gameObject != player && checkobj.gameObject.transform.position.y == go.transform.position.y + 1)
                            || (checkobj.tag == halfBlock && checkobj.gameObject.transform.position.y == go.transform.position.y + 0.75))
                            && Mathf.Abs(checkobj.gameObject.transform.position.x - go.transform.position.x) <= 0.5f)
                            return true;
                    }
                }
            }
            return false;
        }



        string GetAdjacentObjectNameD()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            if (collisions == null)
            {
                return "dummy";
            }
            foreach (Collider2D checkobj in collisions)
            {
                if (IsBlockAdjacentD(player, checkobj.gameObject))
                    return checkobj.gameObject.name;
            }
            return "dummy";
        }

        GameObject GetAdjacentObjectD(GameObject go)
        {
            if (go == null)
            {
                return null;
            }
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                Debug.Log("checkObjInGetAdjObjU1496" + checkObj);
                if (IsPlayerTouchingObjectU(checkObj.gameObject) && checkObj.gameObject != player)
                {
                    return checkObj.gameObject;
                }
            }
            return null;

        }


        string GetAdjacentObjectNameL()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            if (collisions == null)
            {
                return "dummy";
            }
            foreach (Collider2D checkobj in collisions)
            {
                if (IsBlockAdjacentL(player, checkobj.gameObject))
                    return checkobj.gameObject.name;
            }
            return "dummy";
        }

        string GetAdjacentObjectNameR()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            if (collisions == null)
            {
                return "dummy";
            }
            foreach (Collider2D checkobj in collisions)
            {
                if (IsBlockAdjacentR(player, checkobj.gameObject))
                    return checkobj.gameObject.name;
            }
            return "dummy";
        }

        GameObject GetAdjacentObjectU(Collider2D[] collisions)
        {
            GameObject tentativeObj = null;
            if (collisions == null)
            {
                return null;
            }
            foreach (Collider2D checkObj in collisions)
            {
                Debug.Log("checkObjInGetAdjObjU1496" + checkObj);
                if (IsPlayerTouchingObjectU(checkObj.gameObject) && checkObj.gameObject != player)
                {
                    tentativeObj = checkObj.gameObject;
                    if (IsObjectUnmovable(checkObj.gameObject))
                        return checkObj.gameObject;
                }
            }
            return tentativeObj;
        }

        GameObject GetAdjacentObjectU()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            if (collisions == null)
            {
                return null;
            }
            foreach (Collider2D checkObj in collisions)
            {
                if (IsPlayerTouchingObjectU(checkObj.gameObject) && checkObj.gameObject != player)
                {
                    return checkObj.gameObject;
                }
            }
            return null;
        }

        GameObject GetTouchingObjectU_walking()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            if (collisions == null)
            {
                return null;
            }
            foreach (Collider2D checkObj in collisions)
            {
                if (IsPlayerTouchingObjectU_walking(checkObj.gameObject) && checkObj.gameObject != player)
                {
                    return checkObj.gameObject;
                }
            }
            return null;
        }

        GameObject GetAdjacentObjectU_CheckFalling()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkobj in collisions)
            {
                if (collisions == null)
                {
                    return null;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    Debug.Log("checkObjInGetAdjObjU1496" + checkObj);
                    if (IsPlayerTouchingObjectU(checkObj.gameObject) && checkObj.gameObject != player
                           && !IsObjectUnmovable(checkObj.gameObject) && GetFallingStatus(checkObj.gameObject))
                    {
                        return checkObj.gameObject;
                    }
                }
            }
            return null;
        }


        GameObject GetAdjacentObjectU_CheckAdjacent()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkobj in collisions)
            {
                if (collisions == null)
                {
                    return null;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    if (IsBlockAdjacentU(player, checkObj.gameObject) && checkObj.gameObject != player)
                    {
                        return checkObj.gameObject;
                    }
                }
            }
            return null;
        }

        GameObject GetAdjacentObjectR()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkobj in collisions)
            {
                if (collisions == null)
                {
                    return null;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    if (IsBlockAdjacentR(player, checkObj.gameObject) && checkObj.gameObject != player)
                    {
                        return checkObj.gameObject;
                    }
                }
            }
            return null;
        }

        GameObject GetPlayerTouchingObjectR_checkDualTouch()//空中ブロック押しの判定に使う
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            GameObject tentativeObj = null;
            foreach (Collider2D checkObj in collisions)
            {
                if (collisions == null)
                {
                    return null;
                }
                if (IsPlayerTouchingObjectR(checkObj.gameObject))
                {
                    if (tentativeObj == null)
                        tentativeObj = checkObj.gameObject;
                    if (tentativeObj != null && tentativeObj != checkObj.gameObject)
                    {
                        if (checkObj.transform.position.y > tentativeObj.transform.position.y)
                        {
                            return checkObj.gameObject;
                        }
                        else
                        {
                            return tentativeObj;
                        }
                    }
                }
            }
            if (tentativeObj != null)
            {
                return tentativeObj;
            }
            else
                return null;
        }


        GameObject GetPlayerTouchingObjectL_checkDualTouch()//空中ブロック押しの判定に使う
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            GameObject tentativeObj = null;
            foreach (Collider2D checkObj in collisions)
            {
                if (collisions == null)
                {
                    return null;
                }
                if (IsPlayerTouchingObjectL(checkObj.gameObject))
                {
                    if (tentativeObj == null)
                        tentativeObj = checkObj.gameObject;
                    if (tentativeObj != null && tentativeObj != checkObj.gameObject)
                    {
                        if (checkObj.transform.position.y > tentativeObj.transform.position.y)
                            return checkObj.gameObject;
                        else
                            return tentativeObj;
                    }
                }
            }
            if (tentativeObj != null)
                return tentativeObj;
            else
                return null;
        }

        GameObject GetAdjacentObjectL()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkobj in collisions)
            {
                if (collisions == null)
                {
                    return null;
                }
                foreach (Collider2D checkObj in collisions)
                {
                    if (IsBlockAdjacentL(player, checkObj.gameObject) && checkObj.gameObject != player)
                    {
                        return checkObj.gameObject;
                    }
                }
            }
            return null;
        }


        void BreakStone()
        {
            if (breakingObject != null && IsObjectUnbreakable(breakingObject))
            {
                breakingObject = null;
                breaking = false;
            }
            else if (breaking && !notBreak1st && breakingObject != null)
            {
                breakingObject.GetComponent<StoneBehaviour>().BreakingProcess(breakingObject);

                if (breakingObject.GetComponent<StoneBehaviour>().breakingProcessFinal)
                {
                    Debug.Log("destroyObj918");
                    EraseName(breakingObject.name);
                    breaking = false;
                    destroyed = true;
                    if (breakingObject == adjacentObjectF)
                    {
                        adjacentObjectF = null;
                        Debug.Log("adjObj=null959");
                    }
                    else if (breakingObject == adjacentObjectU)
                    {
                        adjacentObjectU = null;
                    }
                    else if (breakingObject == upperStoneObj)
                        upperStoneObj = null;

                    breakingObject.SetActive(false);
                    breakingObject = null;
                    Debug.Log("elased");
                    Debug.Log("1254R =" + touchingR + "  " + "L =" + touchingL + "  "
                        + "U =" + touchingU + "  " + "D =" + touchingD + "    objectName =" + adjacentNameF);
                }
                else if (breakingObject.GetComponent<StoneBehaviour>().breakingProcess2nd && upperStoneObj != null)
                {
                    Debug.Log("upperDeleted1873");
                    upperStoneObj = null;
                }
                if ((breakingObject == adjacentObjectF || breakingObject == adjacentObjectD || breakingObject == adjacentObjectU)
                     && breakingObject != null)
                {
                    Debug.Log("adjFInBreakC1251 adjNameF = " + adjacentNameF);
                    if ((Input.GetKey("up") && touchingU && ((adjacentObjectU != null && !IsObjectUnbreakable(adjacentObjectU))
                                                                || (GetAdjacentObjectU() != null && !IsObjectUnbreakable(GetAdjacentObjectU())))
                        || (Input.GetKey("down") && touchingD && IsObjectAdjacentedD(adjacentNameD) && !breakingObject.GetComponent<StoneBehaviour>().breakingProcess3rd)
                        || (Input.GetKey("right") && touchingR && (IsObjectAdjacentedF(adjacentNameF) || IsObjectAdjacentedF(adjacentNameR)))
                        || (Input.GetKey("left") && touchingL && IsObjectAdjacentedF(adjacentNameF))))
                    {
                        gameManager.CallInoperable(wait / 2);
                        Debug.Log("move861");
                    }
                    else if (breakingObject != adjacentObjectU)
                    {
                        gameManager.CallInoperable(wait);
                    }
                }
            }
            if (breakingObject2nd != null && IsObjectUnbreakable(breakingObject2nd))
            {
                breakingObject2nd = null;
                breaking2nd = false;
            }
            else if (breaking2nd && !notBreak2nd && breakingObject2nd != null)
            {
                breakingObject2nd.GetComponent<StoneBehaviour>().BreakingProcess(breakingObject2nd);
                if (breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcessFinal)
                {
                    EraseName(breakingObject2nd.name);
                    breaking2nd = false;
                    destroyed = true;
                    if (breakingObject2nd == adjacentObjectF)
                        adjacentObjectF = null;
                    else if (breakingObject2nd == adjacentObjectU)
                        adjacentObjectU = null;
                    breakingObject2nd.SetActive(false);
                    breakingObject2nd = null;
                    Debug.Log("elased989");
                }
                else if (breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess2nd && upperStoneObj != null)
                {
                    Debug.Log("upperDeleted1873");
                    upperStoneObj = null;
                }

                if ((breakingObject2nd == adjacentObjectF || breakingObject2nd == adjacentObjectD || breakingObject2nd == adjacentObjectU)
                     && breakingObject2nd != null)
                {
                    if ((Input.GetKey("up") && touchingU) && IsObjectAdjacentedU(adjacentObjectU)
                        || (Input.GetKey("down") && touchingD && IsObjectAdjacentedD(adjacentNameD) && !breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess3rd)
                        || (Input.GetKey("right") && touchingR && (IsObjectAdjacentedF(adjacentNameF) || IsObjectAdjacentedF(adjacentNameR)))
                        || (Input.GetKey("left") && touchingL && IsObjectAdjacentedF(adjacentNameF)))
                        gameManager.CallInoperable(wait / 2);
                    else if (breakingObject != adjacentObjectU)
                    {
                        gameManager.CallInoperable(wait);
                    }
                }
            }
            notBreak1st = false;
            notBreak2nd = false;
        }

        void SwitchOn_NotBreak()
        {
            if (breaking)
                notBreak1st = true;
            if (breaking2nd)
                notBreak2nd = true;
        }

        void BreakStoneInCoroutine()
        {
            if (breakingObject != null && breaking && breakingObject != null)
            {
                breakingObject.GetComponent<StoneBehaviour>().BreakingProcess(breakingObject);
                if (breakingObject.GetComponent<StoneBehaviour>().breakingProcessFinal)
                {
                    adjacentNameF = "dummy";
                    breaking = false;
                    destroyed = true;
                    if (adjacentObjectD == breakingObject)
                        adjacentObjectD = null;
                    if (adjacentObjectU == breakingObject)
                        adjacentObjectU = null;
                    breakingObject.SetActive(false);
                    breakingObject = null;
                    CheckTouchingAfterBreaked();
                    Debug.Log("elased " + touchingL);
                }
                else if (breakingObject.GetComponent<StoneBehaviour>().breakingProcess2nd && upperStoneObj != null)
                {
                    Debug.Log("upperDeleted1959");
                    upperStoneObj = null;
                }

            }
            if (breakingObject2nd != null && breaking2nd && breakingObject2nd != null)
            {
                breakingObject2nd.GetComponent<StoneBehaviour>().BreakingProcess(breakingObject2nd);
                if (breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcessFinal)
                {
                    adjacentNameF = "dummy";
                    breaking2nd = false;
                    destroyed = true;
                    if (adjacentObjectD == breakingObject2nd)
                        adjacentObjectD = null;
                    if (adjacentObjectU == breakingObject2nd)
                        adjacentObjectU = null;
                    breakingObject2nd.SetActive(false);
                    breakingObject2nd = null;
                    Debug.Log("elased1036");
                }
                else if (breakingObject2nd.GetComponent<StoneBehaviour>().breakingProcess2nd && upperStoneObj != null)
                {
                    Debug.Log("upperDeleted1959");
                    upperStoneObj = null;
                }

            }
        }

        void FirstBreak()
        {
            gameManager.CallInoperable(wait / 2);
            adjacentObjectF.GetComponent<StoneBehaviour>().BreakingProcess(breakingObject);
            breaking = true;
        }

        void CheckTouching(bool adjacentedF, Collider2D checkObj)//ブロックがプレイヤーの周囲にあるかどうかを判断
        {
            if (!adjacentedF)
            {
                adjacentNameF = "dummy"; //前後隣接ブロック無し
                Debug.Log("DummiedAdjF1413");
            }
            if ((checkObj.gameObject.transform.position.y - 1 == this.transform.position.y ||
                (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y - 0.75f == this.transform.position.y))
                    && Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f)
            {
                touchingU = true;
                touchFlagU = true;
            }
            else if ((checkObj.gameObject.transform.position.y + 1 == this.transform.position.y ||
                (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y + 0.75f == this.transform.position.y))
                    && Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f)
            {
                touchingD = true;
                touchFlagD = true;
                Debug.Log("touchedD1078" + this.transform.position + checkObj);
            }
            else if (((checkObj.tag != "HalfBlock" && checkObj.gameObject.transform.position.x == this.transform.position.x + 1) ||
                    (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.x == this.transform.position.x + 1
                    && checkObj.gameObject.transform.position.y > this.transform.position.y))
                    && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.5f)
            {
                touchingR = true;
                touchFlagR = true;
            }

            else if (((checkObj.tag != "HalfBlock" && checkObj.gameObject.transform.position.x + 1 == this.transform.position.x) ||
                    (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.x + 1 == this.transform.position.x
                    && checkObj.gameObject.transform.position.y > this.transform.position.y))
                    && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.5f)
            {
                touchingL = true;
                touchFlagL = true;
                Debug.Log("TouchingL1434");
            }
        }

        void CheckTouching(Collider2D checkObj)
        {
            if (Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f && ((checkObj.tag != "HalfBlock"
                    && checkObj.gameObject.transform.position.y - 1 == this.transform.position.y)
                        || (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y - 0.75f == this.transform.position.y)))
            {
                touchingU = true;
                Debug.Log("touchingU = true2481");
            }
            else if (Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f && ((checkObj.tag != "HalfBlock"
                    && checkObj.gameObject.transform.position.y + 1 == this.transform.position.y)
                        || (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y + 0.75f == this.transform.position.y)))
                touchingD = true;
            else if (checkObj.gameObject.transform.position.x - 1 == this.transform.position.x && ((checkObj.tag != "HalfBlock"
                    && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.5f)
                        || (checkObj.tag == "HalfBlock" && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f
                            && !CheckWalkingUorD())))
            {
                touchingR = true;
                Debug.Log("touchingR_CheckT1051 checkObj = " + checkObj);
                Debug.Log("1052pos" + this.transform.position);
            }
            else if (checkObj.gameObject.transform.position.x + 1 == this.transform.position.x && ((checkObj.tag != "HalfBlock"
                    && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.5f)
                        || (checkObj.tag == "HalfBlock" && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f
                            && !CheckWalkingUorD())))
            {
                Debug.Log("touchedL1403");
                touchingL = true;
            }
        }

        void CheckTouching()
        {
            touchingR = false; touchingL = false;
            touchingU = false; touchingD = false;
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                Debug.Log("checkObj = " + checkObj + "  playerPos = " + player.transform.position);
                if (Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f && ((checkObj.tag != "HalfBlock"
                        && checkObj.gameObject.transform.position.y - 1 == this.transform.position.y)
                            || (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y - 0.75f == this.transform.position.y)))
                    touchingU = true;
                else if (Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f && ((checkObj.tag != "HalfBlock"
                        && checkObj.gameObject.transform.position.y + 1 == this.transform.position.y)
                            || (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y + 0.75f == this.transform.position.y)))
                    touchingD = true;
                else if (checkObj.gameObject.transform.position.x - 1 == this.transform.position.x && ((checkObj.tag != "HalfBlock"
                        && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.5f)
                            || (checkObj.tag == "HalfBlock" && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f
                                && !CheckWalkingUorD())))
                {
                    touchingR = true;
                    Debug.Log("touchingR_CheckT1051 checkObj = " + checkObj);
                    Debug.Log("1052pos" + this.transform.position);
                }
                else if (checkObj.gameObject.transform.position.x + 1 == this.transform.position.x && ((checkObj.tag != "HalfBlock"
                        && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.5f)
                            || (checkObj.tag == "HalfBlock" && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f
                                && !CheckWalkingUorD())))
                {
                    Debug.Log("touchedL1403");
                    touchingL = true;
                }
            }
        }

        void CheckTouchingAfterBreaked()
        {
            touchingR = false; touchingL = false;
            touchingU = false; touchingD = false;
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                Debug.Log("checkObj = " + checkObj + "  playerPos = " + player.transform.position);
                if (Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f && ((checkObj.tag != "HalfBlock"
                        && checkObj.gameObject.transform.position.y - 1 == this.transform.position.y)
                            || (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y - 0.75f == this.transform.position.y)))
                    touchingU = true;
                else if (Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.5f && ((checkObj.tag != "HalfBlock"
                        && checkObj.gameObject.transform.position.y + 1 == this.transform.position.y)
                            || (checkObj.tag == "HalfBlock" && checkObj.gameObject.transform.position.y + 0.75f == this.transform.position.y)))
                    touchingD = true;
                else if (checkObj.gameObject.transform.position.x - 1 == this.transform.position.x && ((checkObj.tag != "HalfBlock"
                        && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.75f)
                            || (checkObj.tag == "HalfBlock" && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f
                                )))
                {
                    touchingR = true;
                }
                else if (checkObj.gameObject.transform.position.x + 1 == this.transform.position.x && ((checkObj.tag != "HalfBlock"
                        && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.75f)
                            || (checkObj.tag == "HalfBlock" && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) <= 0.25f
                                )))
                {
                    Debug.Log("touchedL1403");
                    touchingL = true;
                }
            }
        }

        void EraseName(string name)
        {
            if (adjacentNameR == name)
                adjacentNameR = "dummy";
            if (adjacentNameL == name)
                adjacentNameL = "dummy";
            if (adjacentNameU == name)
                adjacentNameU = "dummy";
            if (adjacentNameD == name)
                adjacentNameD = "dummy";
            if (adjacentNameF == name)
                adjacentNameF = "dummy";
        }

        bool IsExistAtThePosition(Vector3 pos)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkobj in collisions)
            {
                if (checkobj.gameObject.transform.position == pos)
                {
                    if (pos.x == player.transform.position.x + 1 && pos.y == player.transform.position.y)
                        adjacentNameR = checkobj.name;
                    else if (pos.x == player.transform.position.x - 1 && pos.y == player.transform.position.y)
                        adjacentNameL = checkobj.name;
                    return true;
                }
            }
            return false;
        }

        bool IsExistInTheRangeX(double vx)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkobj in collisions)
            {
                if (IsBlockAdjacentD(player, checkobj.gameObject))
                {
                    Debug.Log("1019 " + checkobj);
                    if (Mathf.Abs(checkobj.gameObject.transform.position.x - player.transform.position.x) <= vx)
                        return true;
                }
            }
            return false;
        }

        bool IsExistInTheRangeY(double vy)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkobj in collisions)
            {
                if (Mathf.Abs(checkobj.gameObject.transform.position.y - player.transform.position.y) <= vy)
                    return true;
            }
            return false;
        }

        public static bool IsBlockAdjacentR(GameObject goCenter, GameObject go2)
        {//goCenter...中心となるオブジェクト　goCenterに対して右に接しているかどうか
            if (goCenter.gameObject.transform.position.x + 1 == go2.gameObject.transform.position.x
                && (goCenter.gameObject.transform.position.y == go2.gameObject.transform.position.y
                    || (goCenter.gameObject.transform.position.y + 0.25 == go2.gameObject.transform.position.y
                        || goCenter.gameObject.transform.position.y - 0.25 == go2.gameObject.transform.position.y)))
                return true;
            return false;
        }

        public static bool IsBlockAdjacentL(GameObject goCenter, GameObject go2)
        {
            if (goCenter.gameObject.transform.position.x - 1 == go2.gameObject.transform.position.x
                 && (goCenter.gameObject.transform.position.y == go2.gameObject.transform.position.y
                     || (goCenter.gameObject.transform.position.y + 0.25 == go2.gameObject.transform.position.y
                         || goCenter.gameObject.transform.position.y - 0.25 == go2.gameObject.transform.position.y)))
                return true;
            return false;
        }

        public static bool IsBlockAdjacentU(GameObject goCenter, GameObject go2)
        {
            if (goCenter.gameObject.transform.position.x == go2.gameObject.transform.position.x
                && goCenter.gameObject.transform.position.y + 1 == go2.gameObject.transform.position.y)
                return true;
            return false;
        }

        public static bool IsBlockAdjacentD(GameObject goCenter, GameObject go2)
        {
            if (goCenter.gameObject.transform.position.x == go2.gameObject.transform.position.x
                && (goCenter.gameObject.transform.position.y - 1 == go2.gameObject.transform.position.y
                    || goCenter.gameObject.transform.position.y - 0.75f == go2.gameObject.transform.position.y))
                return true;
            return false;
        }

        public static bool IsBlockTouchingR(GameObject goCenter, GameObject go2)
        {
            if (goCenter.gameObject.transform.position.x + 1 == go2.gameObject.transform.position.x
                && Mathf.Abs(goCenter.gameObject.transform.position.y - go2.gameObject.transform.position.y) <= 0.5f)
                return true;
            return false;
        }

        public static bool IsBlockTouchingL(GameObject goCenter, GameObject go2)
        {
            if (goCenter.gameObject.transform.position.x - 1 == go2.gameObject.transform.position.x
                && Mathf.Abs(goCenter.gameObject.transform.position.y - go2.gameObject.transform.position.y) <= 0.5f)
                return true;
            return false;
        }

        bool IsPlayerTouchingObjectR()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                Debug.Log("IPTOR_checkObj1894 = " + checkObj);
                if (player.gameObject.transform.position.x + 1 == checkObj.gameObject.transform.position.x
                && Mathf.Abs(player.gameObject.transform.position.y - checkObj.gameObject.transform.position.y) <= 0.5f)
                    return true;
            }
            return false;
        }

        bool IsPlayerTouchingObjectR(GameObject go)
        {
            if (player.gameObject.transform.position.x + 1 == go.gameObject.transform.position.x
            && Mathf.Abs(player.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f)
                return true;
            return false;
        }

        bool IsPlayerTouchingObjectL()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                Debug.Log("IPTOL_checkObj2889 = " + checkObj);
                if (player.gameObject.transform.position.x - 1 == checkObj.gameObject.transform.position.x
                && Mathf.Abs(player.gameObject.transform.position.y - checkObj.gameObject.transform.position.y) <= 0.5f)
                    return true;
            }
            return false;
        }
        bool IsPlayerTouchingObjectL(GameObject go)
        {
            if (player.gameObject.transform.position.x - 1 == go.gameObject.transform.position.x
            && Mathf.Abs(player.gameObject.transform.position.y - go.gameObject.transform.position.y) <= 0.5f)
                return true;
            return false;
        }

        bool IsPlayerTouchingObjectU(GameObject checkGameObj)
        {
            if (Mathf.Abs(player.gameObject.transform.position.x - checkGameObj.gameObject.transform.position.x) <= 0.5f
                && (player.gameObject.transform.position.y + 1 == checkGameObj.transform.position.y
                    || player.gameObject.transform.position.y + 0.75 == checkGameObj.transform.position.y))
                return true;
            return false;
        }

        bool IsPlayerTouchingObjectU_walking(GameObject checkGameObj)
        {
            if (Mathf.Abs(player.gameObject.transform.position.x - checkGameObj.gameObject.transform.position.x) <= 0.75f
                && (player.gameObject.transform.position.y + 1 == checkGameObj.transform.position.y
                    || player.gameObject.transform.position.y + 0.75 == checkGameObj.transform.position.y))
                return true;
            return false;
        }

        bool IsPlayerTouchingObjectU()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (Mathf.Abs(this.gameObject.transform.position.x - checkObj.gameObject.transform.position.x) <= 0.5f
                && (player.gameObject.transform.position.y + 1 == checkObj.transform.position.y
                    || player.gameObject.transform.position.y + 0.75 == checkObj.transform.position.y))
                    return true;
            }
            return false;
        }

        bool IsPlayerTouchingObjectD(GameObject checkGameObj)
        {
            if (Mathf.Abs(this.gameObject.transform.position.x - checkGameObj.gameObject.transform.position.x) <= 0.5f
                && (player.gameObject.transform.position.y - 1 == checkGameObj.transform.position.y
                    || player.gameObject.transform.position.y - 0.75 == checkGameObj.transform.position.y))
                return true;
            return false;
        }

        bool IsPlayerTouchingObjectD()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (Mathf.Abs(this.gameObject.transform.position.x - checkObj.gameObject.transform.position.x) <= 0.5f
                && (player.gameObject.transform.position.y - 1 == checkObj.transform.position.y
                    || player.gameObject.transform.position.y - 0.75 == checkObj.transform.position.y))
                    return true;
            }
            return false;
        }

        public static bool IsBlockTouchingD(GameObject goCenter, GameObject go2)
        {
            if (Mathf.Abs(goCenter.gameObject.transform.position.x - go2.gameObject.transform.position.x) <= 0.5f
                && (goCenter.gameObject.transform.position.y - 1 == go2.gameObject.transform.position.y
                    || goCenter.gameObject.transform.position.y - 0.75f == go2.transform.position.y))
                return true;
            return false;
        }

        public static bool IsBlockTouchingU(GameObject goCenter, GameObject go2)
        {
            if (Mathf.Abs(goCenter.gameObject.transform.position.x - go2.gameObject.transform.position.x) <= 0.5f
                && (goCenter.gameObject.transform.position.y + 1 == go2.gameObject.transform.position.y
                    || goCenter.gameObject.transform.position.y + 0.75f == go2.transform.position.y))
                return true;
            return false;
        }

        bool IsBlockTouchingD(GameObject go)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (Mathf.Abs(go.gameObject.transform.position.x - checkObj.gameObject.transform.position.x) <= 0.5f
                   && (go.gameObject.transform.position.y - 1 == checkObj.gameObject.transform.position.y
                       || go.gameObject.transform.position.y - 0.75f == checkObj.transform.position.y))
                    return true;
            }
            return false;
        }

        bool IsStoneTouchingStoneU(GameObject goCenter, GameObject go2)
        {
            if (Mathf.Abs(goCenter.gameObject.transform.position.x - go2.gameObject.transform.position.x) <= 0.5f
                && goCenter.gameObject.transform.position.y + 1 == go2.gameObject.transform.position.y)
                return true;
            return false;
        }

        bool IsObjectTouchingHalfBlockD(GameObject go)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (checkObj.transform.position.y < go.transform.position.y && checkObj.tag == halfBlock)
                    return true;
            }
            return false;
        }

        bool IsObjectTouchingWallOrHalfBlockD(GameObject go)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (IsBlockTouchingD(go, checkObj.gameObject) && IsObjectUnmovable(checkObj.gameObject))
                    return true;
            }
            return false;
        }

        bool IsBlockReadyToFallOnPlayer(GameObject upperObj, GameObject frontObj)
        {
            if (upperObj != null && frontObj != null && !IsObjectUnmovable(upperObj) && !IsObjectBreaking(upperObj))
            {
                Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
                foreach (Collider2D checkObj in collisions)
                {
                    if (IsBlockAdjacentD(upperObj, checkObj.gameObject) && checkObj.gameObject == player)
                        return true;
                }
                return false;
            }
            else
                return false;
        }

        void ResetAfterDestroy(bool destroyed, Collider2D[] collisions)
        {
            touchingR = false; touchingL = false;
            touchingU = false; touchingD = false;
            foreach (Collider2D checkObj in collisions)
            {
                CheckTouching(checkObj);
            }
            Debug.Log("Destroyed");
        }

        void CheckAdjacent_F_statusInBreakingR(GameObject breakingObj)
        {
            {
                if (adjacentNameR != "dummy")
                    adjacentObjectF = GameObject.Find(adjacentNameR);
                if (adjacentNameL == "dummy")
                    adjacentNameR = "dummy";//両側にブロックが無ければadjNameL,Rは不必要
                if (breakingObj.GetComponent<StoneBehaviour>().breakingProcess3rd)
                {
                    gameManager.CallInoperable(wait / 2);
                    Debug.Log("wait1092");
                }
            }
            if (adjacentNameL != "dummy" && touchingL)
                adjacentNameF = adjacentNameR;

        }

        void CheckAdjacent_F_statusInBreakingL(GameObject breakingObj)
        {
            {
                if (adjacentNameL != "dummy")
                    adjacentObjectF = GameObject.Find(adjacentNameL);
                if (adjacentNameR == "dummy")
                    adjacentNameL = "dummy";//両側にブロックが無ければadjNameL,Rは不必要
                if (breakingObj.GetComponent<StoneBehaviour>().breakingProcess3rd)
                    gameManager.CallInoperable(wait / 2);
            }
            if (adjacentNameR != "dummy" && touchingR)
                adjacentNameF = adjacentNameL;
        }

        bool CheckWalkingUorD()
        {
            if (this.animator.GetBool("walkingU") || this.animator.GetBool("walkingD"))
                return true;
            return false;
        }

        void TouchingValidation()
        {
            if (touchingR)
                touchFlagR = true;
            if (touchingL)
                touchingL = true;
            if (touchingU)
                touchingU = true;
            if (touchingD)
                touchingD = true;
        }

        void CheckFlagAndInitializeTouching()
        {
            if (!touchFlagR)
                touchingR = false;
            if (!touchFlagL)
                touchingL = false;
            if (!touchFlagU)
                touchingU = false;
            if (!touchFlagD)
                touchingD = false;
        }

        void InitializeTouchFlag()
        {
            touchFlagR = false; touchFlagL = false;
            touchFlagU = false; touchFlagD = false;
        }

        public static bool IsObjectUnmovable(GameObject checkGameObj)
        {
            if (checkGameObj == null)
                return false;
            else if (checkGameObj.tag == "Wall" || checkGameObj.tag == "HalfBlock" || checkGameObj.tag == "BlueArea")
                return true;
            return false;
        }

        public static bool IsObjectUnbreakable(GameObject checkGameObj)
        {
            if (checkGameObj.tag != stone && checkGameObj.tag != "cracked")
            {
                return true;
            }
            else
                return false;
        }


        public static bool IsObjectUnmovable(String checkGameObjName)
        {
            if (checkGameObjName == dummy)
                return false;
            else
            {
                GameObject tentativeobj = GameObject.Find(checkGameObjName);
                if (tentativeobj.tag == wall || tentativeobj.tag == halfBlock || tentativeobj.tag == BlueArea)
                    return true;
            }
            return false;
        }

        void VallidationOfAdjacentNameD(Collider2D checkObj)
        {
            bool unchanged = false;
            if (adjacentNameD != dummy)

                if (adjacentNameD != dummy && IsObjectUnmovable(GameObject.Find(adjacentNameD))
                    && IsPlayerTouchingObjectD(GameObject.Find(adjacentNameD)))
                {
                    unchanged = true;
                }
            if (!unchanged)
                adjacentNameD = checkObj.name;
        }

        bool IsObjectTouchingUnderTheStone(GameObject blockObj)//プレイヤーがブロックを押した後、落下挙動の判定に使うメソッド
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blockObj.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (IsBlockTouchingD(blockObj, checkObj.gameObject))
                {
                    Debug.Log("TouchDObjFind1964");
                    return true;
                }
            }
            return false;
        }

        bool IsObjectTouchingUpperTheStone(GameObject blockObj)//プレイヤーがブロックを押した後、落下挙動の判定に使うメソッド
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blockObj.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                Debug.Log("touchingObjInIOTUTS1976");
                if (IsBlockTouchingU(blockObj, checkObj.gameObject))
                {
                    Debug.Log("TouchDObjFind1978");
                    return true;
                }
            }
            return false;
        }

        GameObject GetTouchingObjectUpperTheStone(GameObject blockObj)//プレイヤーがブロックを押した後、落下挙動の判定に使うメソッド
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blockObj.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (checkObj.gameObject != player)
                {
                    if (IsBlockTouchingU(blockObj, checkObj.gameObject))
                    {
                        return checkObj.gameObject;
                    }
                    else if (blockObj.GetComponent<StoneBehaviour>().breakingProcess3rd && blockObj.transform.position.y + 0.5f == checkObj.transform.position.y)
                    {
                        return checkObj.gameObject;
                    }
                }
            }
            return null;
        }

        GameObject GetTouchingObjectUpperTheStone_prioritizingUnmovable(GameObject blockObj)//プレイヤーがブロックを押した後、落下挙動の判定に使うメソッド
        {
            Debug.Log("enteredProcessGTOUSP2962");
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blockObj.transform.position, objectSize, 0f);
            GameObject tentativeObj = null;

            foreach (Collider2D checkObj in collisions)
            {
                if (checkObj.gameObject != player)
                {
                    if (IsBlockTouchingU(blockObj, checkObj.gameObject))
                    {
                        tentativeObj = checkObj.gameObject;
                    }
                    else if (blockObj.GetComponent<StoneBehaviour>().breakingProcess3rd && blockObj.transform.position.y + 0.5f == checkObj.transform.position.y)
                    {
                        tentativeObj = checkObj.gameObject;
                    }
                }
            }
            if (tentativeObj != null)
            {
                Collider2D[] collisionsUpper = Physics2D.OverlapBoxAll(tentativeObj.transform.position, objectSize, 0f);
                foreach (Collider2D checkObject in collisionsUpper)
                {
                    if (checkObject != player && IsBlockTouchingD(tentativeObj, checkObject.gameObject) && IsObjectUnmovable(checkObject.gameObject))
                    {
                        Debug.Log("upperStoneNotFall2985");
                        return null;
                    }
                }
                return tentativeObj;
            }

            return null;
        }

        GameObject GetTouchingObjectUnderTheStone(GameObject blockObj)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blockObj.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (checkObj.gameObject != player && IsBlockTouchingD(blockObj, checkObj.gameObject))
                {
                    return checkObj.gameObject;
                }
            }
            return null;
        }

        GameObject GetTouchingObjectUnderTheStone_CheckDual(GameObject blockObj)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blockObj.transform.position, objectSize, 0f);
            GameObject tentativeObj = null;
            foreach (Collider2D checkObj in collisions)
            {
                if (checkObj.gameObject != player && IsBlockTouchingD(blockObj, checkObj.gameObject))
                {
                    if (tentativeObj != null && checkObj == player)
                        tentativeObj = checkObj.gameObject;
                }
            }
            if (tentativeObj != null)
                return tentativeObj.gameObject;
            return null;
        }


        bool IsStoneTouchingUpperTheStone_AddList(GameObject blockObj)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blockObj.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (checkObj.gameObject != blockObj && checkObj.tag != "Wall" && checkObj.tag != "halfBlock")
                {
                    Debug.Log("1459" + checkObj + "  " + checkObj.tag);
                    if (IsStoneTouchingStoneU(blockObj, checkObj.gameObject) && !IsObjectTouchingWallOrHalfBlockD(checkObj.gameObject))
                    {
                        Debug.Log("addListUpper1461");
                        fallingObjectListList[fStoneListNum].fallingObjectListChild.Add(checkObj.gameObject);
                        checkObj.gameObject.GetComponent<StoneBehaviour>().SwitchOnFallingStatus();

                        if (IsStoneTouchingUpperTheStone_AddList(checkObj.gameObject))//再帰的に実行
                            return true;
                    }
                }
            }
            return false;
        }

        void MakeListOfFallingObject(GameObject go)
        {
            GameObject upperObj = null;
            bool TouchigObjU = false;
            fallingObjectListList.Add(new FallingObjectList());
            go.GetComponent<StoneBehaviour>().SwitchOnFallingStatus();

            fallingObjectListList[fStoneListNum].fallingObjectListChild.Add(go);
            falling = true;
            Collider2D[] collisions = Physics2D.OverlapBoxAll(go.transform.position, objectSize, 0f);

            GamePlayAgents.firstFall = true;

            foreach (Collider2D checkObj in collisions)
            {
                if (IsStoneTouchingStoneU(go, checkObj.gameObject) && !IsObjectTouchingWallOrHalfBlockD(checkObj.gameObject)
                    && checkObj.tag != wall && checkObj.tag != halfBlock)
                {
                    TouchigObjU = true;
                    upperObj = checkObj.gameObject;
                    fallingObjectListList[fStoneListNum].fallingObjectListChild.Add(upperObj);//adjacentObjectFの上に載っているブロックもリストに追加
                    upperObj.GetComponent<StoneBehaviour>().SwitchOnFallingStatus();
                }
            }
            if (TouchigObjU)
            {
                Debug.Log("EnterISTUSA_1491 upper" + upperObj);
                IsStoneTouchingUpperTheStone_AddList(upperObj);
            }
            fStoneListNum++;
            Debug.Log("fallChild = " + fallingObjectListList[0].fallingObjectListChild.Count);
        }

        void CheckFallingAndDropStone()
        {
            bool removedFlag = false;
            Debug.Log("dropProcess2462");
            if (falling)
            {
                blockAbovePlayer = false;
                touchingObjectAtTheBottom = false;
                Debug.Log("dropProcess2465 upperStoneObj = " + upperStoneObj + "  IsBreakingFirst = " + breaking + "  brakingObjFirst = " + breakingObject);
                if (breaking || breaking2nd)
                {
                    GameObject tentativeObj = GetTouchingObjectU_walking();
                    Debug.Log("tentativeObj3142 = " + tentativeObj + "  upperStoneObj = " + upperStoneObj);
                    if (tentativeObj != null && IsObjectBreaking(tentativeObj))
                    {
                        if (!(tentativeObj.GetComponent<StoneBehaviour>().breakingProcess2nd && !tentativeObj.GetComponent<StoneBehaviour>().breakingProcess3rd))
                        {
                            upperStoneObj = GetTouchingObjectUpperTheStone(tentativeObj);
                            Debug.Log("upperStoneChanged3056");
                        }
                        else
                            upperStoneObj = null;
                    }
                }
                else
                    upperStoneObj = null;

                for (int i = 0; i < fStoneListNum; i++)
                {
                    removedFlag = false;
                    if (upperStoneObj == null || (upperStoneObj != null && !fallingObjectListList[i].fallingObjectListChild.Contains(upperStoneObj)))
                    {
                        Debug.Log("upperStoneContains = " + fallingObjectListList[i].fallingObjectListChild.Contains(upperStoneObj) + "upperStoneObj = " + upperStoneObj);
                        if (fallingObjectListList.Count == 1 && fallingObjectListList[0].fallingObjectListChild.Count == 0)
                        {
                            fallingObjectListList.Remove(fallingObjectListList[0]);
                            break;
                        }
                        GameObject fallObjBottom = fallingObjectListList[i].fallingObjectListChild[0];
                        GameObject underObj = GetTouchingObjectUnderTheStone(fallObjBottom);
                        bool brokenInFalling = false;

                        Debug.Log("underObj2890 = " + underObj + "  fallObjBottom = " + fallObjBottom + "  adjObjF = " + adjacentObjectF);
                        if (!this.animator.GetBool("playerDead") && IsBlockTouchingD(fallObjBottom) &&
                            (underObj != null) && ((underObj != breakingObject && underObj != breakingObject2nd)
                                                       && !(IsObjectUnmovable(underObj) && IsObjectBreaking(fallObjBottom))))
                        {
                            foreach (GameObject fallObj in fallingObjectListList[i].fallingObjectListChild)
                            {
                                if (fallObj.tag == BlueCrustal && IsBlueCrystalUnreachableToGoal(fallObj))
                                    IsBlueCrystalUnreachable = true;

                                fallObj.GetComponent<StoneBehaviour>().SwitchOffFallingStatus();
                            }
                            Debug.Log("fallObjRemoveProcess3172");
                            fallingObjectListList[i].fallingObjectListChild.Clear();
                            fallingObjectListList.Remove(fallingObjectListList[0]);
                            touchingObjectAtTheBottom = true;//落下しているブロックが着地した場合



                            fStoneListNum--;
                            fallingObjTouchedDown = true;
                            removedFlag = true;
                            if (!dualTouchingD)
                            {
                                Debug.Log("DualCheckProcess1988");
                                Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
                                int dualCounter = 0;
                                foreach (Collider2D checkObj in collisions)
                                {
                                    Debug.Log("TouchingObj1993 = " + checkObj + "  position = " + checkObj.transform.position);

                                    if (checkObj.tag != halfBlock && checkObj != player)
                                    {
                                        if (Mathf.Abs(checkObj.gameObject.transform.position.x - player.transform.position.x) <= 0.5f
                                            && (player.gameObject.transform.position.y - 1 == checkObj.transform.position.y))
                                        {
                                            dualCounter = CheckDual(dualCounter);
                                            VallidationOfAdjacentNameD(checkObj);
                                        }
                                        else if (((player.transform.position.x - 0.75 == checkObj.gameObject.transform.position.x && this.animator.GetBool("turnedL"))
                                                    || (player.transform.position.x + 0.75 == checkObj.gameObject.transform.position.x && this.animator.GetBool("turnedR")))
                                                && player.transform.position.y - 1 == checkObj.gameObject.transform.position.y)
                                        {
                                            dualCounter = CheckDual(dualCounter);
                                            VallidationOfAdjacentNameD(checkObj);
                                        }
                                    }
                                    else
                                    {
                                        if (checkObj.tag == "HalfBlock" && player.transform.position.y > checkObj.gameObject.transform.position.y
                                            && IsPlayerTouchingObjectD(checkObj.gameObject))
                                        {
                                            dualCounter = CheckDual(dualCounter);
                                            VallidationOfAdjacentNameD(checkObj);
                                        }
                                    }
                                    if (dualTouchingD)
                                        break;
                                }
                            }
                            if (i > 0)
                                i--;
                        }
                        else if (adjacentObjectF != null && !this.animator.GetBool("playerDead") && IsBlockTouchingD(adjacentObjectF)
                                    && fallingObjectListList[i].fallingObjectListChild.Contains(adjacentObjectF))
                        {
                            int fallingAdjFObjIndex = 0;
                            foreach (GameObject go in fallingObjectListList[i].fallingObjectListChild)
                            {
                                if (go == adjacentObjectF)
                                {
                                    fallingAdjFObjIndex = fallingObjectListList[i].fallingObjectListChild.IndexOf(go);
                                    fallingObjectListList[i].fallingObjectListChild[fallingAdjFObjIndex].GetComponent<StoneBehaviour>().SwitchOffFallingStatus();
                                }
                            }
                            int fallObjCount = fallingObjectListList[i].fallingObjectListChild.Count;
                            Debug.Log("fallObjCount3314 = " + fallObjCount);
                            if (fallingAdjFObjIndex < fallObjCount - 1)
                            {
                                for (int n = fallingAdjFObjIndex; n < fallObjCount; n++)
                                {
                                    fallingObjectListList[i].fallingObjectListChild[n].GetComponent<StoneBehaviour>().SwitchOffFallingStatus();
                                    fallingObjectListList[i].fallingObjectListChild.RemoveAt(n);
                                }
                            }
                            else
                                fallingObjectListList[i].fallingObjectListChild.RemoveAt(fallingAdjFObjIndex);

                        }
                        if (fallingObjectListList.Count != 0 && (breaking || breaking2nd))
                        {
                            List<GameObject> copiedFallList = new List<GameObject>();

                            foreach (GameObject go in fallingObjectListList[i].fallingObjectListChild)
                                copiedFallList.Add(go);

                            foreach (GameObject fallObj in copiedFallList)
                            {
                                if (fallObj == breakingObject)
                                {
                                    fallingObjectListList[i].fallingObjectListChild.Remove(fallObj);
                                    brokenInFalling = true;
                                }
                                else if (fallObj == breakingObject2nd)
                                {
                                    fallingObjectListList[i].fallingObjectListChild.Remove(fallObj);
                                    brokenInFalling = true;
                                }
                            }
                        }
                        if (!touchingObjectAtTheBottom)
                        {
                            foreach (GameObject fallObj in fallingObjectListList[i].fallingObjectListChild)
                            {
                                if (!playerDead && IsPlayerTouchingObjectU(fallObj))
                                {
                                    playerDead = true;
                                    adjacentObjectU = fallObj;
                                    break;
                                }
                                Debug.Log("fallingObj2560 = " + fallObj);
                                fallObj.transform.position -= moveY;
                            }
                            if (fallObjBottom.transform.position.y == player.transform.position.y + 1
                                && Math.Abs(fallObjBottom.transform.position.x - player.transform.position.x) <= 0.5f)
                            {
                                Debug.Log("blockAboveTrue2215");
                                adjacentObjectU = fallObjBottom;
                                blockAbovePlayer = true;
                            }
                            if (brokenInFalling && fallingObjectListList[i].fallingObjectListChild.Count == 0)
                            {
                                fallingObjectListList.Remove(fallingObjectListList[i]);
                                fStoneListNum--;
                                Debug.Log("ListListRemoved3265");
                            }
                        }
                    }
                    if (removedFlag)
                    {
                        touchingObjectAtTheBottom = false;
                        if (i == 0)
                            i--;
                        else
                            removedFlag = false;
                    }
                }

                if (fallingObjectListList.Count == 0)
                {
                    falling = false;
                    Debug.Log("noFallObj");
                }
            }
        }

        IEnumerator PressingProcess()
        {
            if (falling && !isRunninng && adjacentObjectU != null && GetFallingStatus(adjacentObjectU))
            {
                if (adjacentObjectU != null && (breaking || breaking2nd) && IsObjectBreaking(adjacentObjectU))
                    adjacentObjectU.GetComponent<StoneBehaviour>().SwitchOffFallingStatus();
                if (GetFallingStatus(adjacentObjectU))
                {
                    playerDead = true;
                    isRunninng = true;
                    gameManager.CallInoperable(4 * wait);
                    this.animator.SetBool("playerDead", true);
                    CheckFallingAndDropStone();
                    yield return new WaitForSeconds(2 * wait);
                    CheckFallingAndDropStone();
                    var renderer = gameObject.GetComponent<Renderer>();
                    renderer.enabled = false;
                    yield return new WaitForSeconds(wait);
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }

        IEnumerator PressingProcessInCoroutine()
        {
            gameManager.CallInoperable(4 * wait);
            this.animator.SetBool("playerDead", true);
            if (!GetFallingStatus(adjacentObjectU))
                MakeListOfFallingObject(adjacentObjectU);
            if (!notTouchUnderTheStone)
                CheckFallingAndDropStone();
            yield return new WaitForSeconds(2 * wait);
            CheckFallingAndDropStone();
            var renderer = gameObject.GetComponent<Renderer>();
            renderer.enabled = false;
            yield return new WaitForSeconds(wait);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public bool IsBlueCrystalPushable(GameObject blueCrystal)//青クリスタルが押せるかどうか判定（強化学習用メソッド）
        {
            bool pushable = true;
            Collider2D[] collisions = Physics2D.OverlapBoxAll(blueCrystal.transform.position, objectSize, 0f);

            foreach (Collider2D checkObj in collisions)
            {
                if (IsBlockAdjacentR(blueCrystal, checkObj.gameObject) && IsObjectUnbreakable(checkObj.gameObject))
                    return false;
                else if (IsBlockAdjacentL(blueCrystal, checkObj.gameObject) && IsObjectUnbreakable(checkObj.gameObject))
                    return false;
                if (IsBlockAdjacentU(blueCrystal, checkObj.gameObject) && IsObjectUnbreakable(checkObj.gameObject))
                    return false;
                if (IsBlockAdjacentD(blueCrystal, checkObj.gameObject) && IsObjectUnbreakable(checkObj.gameObject))
                    return false;
            }


            return pushable;
        }

        IEnumerator deadSlimely()
        {
            if (wait != 0.5f)
                setAnimationSpeedRatio(0.5f / wait);
            Debug.Log("slimeMode3362");
            isRunninng = true;
            animator.Play("slime");
            gameManager.CallInoperable(7 * wait);
            yield return new WaitForSeconds(7 * wait);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        IEnumerator SolvedThisPuzzle()
        {
            if (IsBlockAdjacentD(b_CrystalGenerator.blueCrystal, b_areaGenerator.blueArea))
            {
                Debug.Log("ClearFlag3325");
                animator.Play("frisk");
                gameManager.CallInoperable(10 * wait);
                yield return new WaitForSeconds(8 * wait);
                animator.enabled = false;
                animator.Play("jumping");
                yield return new WaitForSeconds(2 * wait);
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        bool CheckPlayerNotWalking()
        {
            if (!this.animator.GetBool("walkingR") || !this.animator.GetBool("walkingL")
                || !this.animator.GetBool("walkingU") || !this.animator.GetBool("walkingD"))
                return true;
            return false;
        }

        bool GetFallingStatus(GameObject stoneObj)
        {
            return stoneObj.GetComponent<StoneBehaviour>().fallingStatus;
        }

        void checkPlayerOverhead()
        {
            if (checkflagPlayerOverhead)
            {
                if (IsPlayerTouchingObjectU())
                {
                    playerDead = true;
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }

        bool getBreakingStatus(GameObject stoneObj)
        {
            return stoneObj.GetComponent<StoneBehaviour>().breakingNotFallFlag;
        }


        bool IsObjectBreaking(GameObject go)
        {
            if (go != null && (go == breakingObject || go == breakingObject2nd))
                return true;
            return false;
        }

        public static Vector3 x_Vector(int n)
        {
            return new Vector3(n, 0, 0);
        }

        public static Vector3 y_Vector(int n)
        {
            return new Vector3(0, n, 0);
        }

        public static void DestroyBlueAreaAndHalf()
        {
            Destroy(b_areaGenerator.blueArea);
            Destroy(b_area_halfGenerator.blueArea_half);
        }

        public static void setWaitTime(float time)
        {
            wait = time;
        }

        public static float getWaitTime()
        {
            return wait;
        }

        public void setAnimationSpeedRatio(float speed)
        {
            animator.SetFloat("Speed", speed);
        }

        public static bool GetIsRunning()
        {
            return isRunninng;
        }

        public bool IsBlueCrystalUnreachableToGoal(GameObject b_crystal)
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(b_crystal.transform.position, objectSize, 0f);
            foreach (Collider2D checkObj in collisions)
            {
                if (IsBlockAdjacentR(b_crystal, checkObj.gameObject) && IsObjectUnbreakable(checkObj.gameObject))
                    return true;
                else if (IsBlockAdjacentL(b_crystal, checkObj.gameObject) && IsObjectUnbreakable(checkObj.gameObject))
                    return true;
            }
            return false;


        }

        public void WalkRight_execute()
        {
            if (CheckPlayerNotWalking())
            {
                Debug.Log("walkRight3736" + " phase = " + phase);
                keyHistory = "right";
                StartCoroutine(PressingProcess());
                StartCoroutine(WalkRight());
                BreakStone();
                GamePlayAgents.actionHistory = 3;
                inputNum = 0;
            }
        }
        public void WalkLeft_execute()
        {
            if (CheckPlayerNotWalking())
            {
                Debug.Log("walkLeft3736" + " phase = " + phase);
                keyHistory = "left";
                StartCoroutine(PressingProcess());
                StartCoroutine(WalkLeft());
                BreakStone();
                GamePlayAgents.actionHistory = 4;
                inputNum = 0;
            }
        }
        public void WalkUp_execute()
        {
            if (CheckPlayerNotWalking())
            {
                Debug.Log("walkUp3736" + " phase = " + phase);
                keyHistory = "up";
                StartCoroutine(PressingProcess());
                StartCoroutine(WalkUp());
                BreakStone();//石が壊れている最中の動作の記述
                GamePlayAgents.actionHistory = 1;
                inputNum = 0;
            }
        }
        public void WalkDown_execute()
        {
            if (CheckPlayerNotWalking())
            {
                Debug.Log("walkDown3736" + " phase = " + phase);
                keyHistory = "down";
                StartCoroutine(PressingProcess());
                StartCoroutine(WalkDown());
                BreakStone();
                GamePlayAgents.actionHistory = 2;
                inputNum = 0;
            }
        }
        public void ResetGame()
        {
            inputNum = 0;
            Debug.Log("resetGame3631");
            b_CrystalGenerator.InitializeBlueCrystalPos();
            InitializeGame();
        }
        public void ClearedProcess()
        {
            if (GamePlayAgents.tManager.enteredFromMapLoad)
            {
                tManager = GamePlayAgents.tManager;
                Debug.Log("stageCount = " + tManager.mapStageCount);
                if (tManager.mapStageCount != tManager.MapStageList.Count - 1 && GamePlayAgents.clearCount == GamePlayAgents.clearMaxNum)
                {
                    tManager.mapStageCount++;
                    inputNum = 0;
                    GamePlayAgents.solved = false;
                    GamePlayAgents.clearCount = 0;
                    GamePlayAgents.loadedNextStage = true;
                    LoadedData.StageFileLoader.ReadStageDataFile("Assets/Field Parts/LoadingScenePatrs/StageTextFiles"
                    + "/" + tManager.MapStageList[tManager.mapStageCount] + ".txt");
                    SceneManager.LoadScene("PlayLoadedStageScene");
                }
                else
                {
                    inputNum = 0;
                    GamePlayAgents.solved = false;
                    ResetGame();
                    Debug.Log("clearStage273");
                }
            }
            else
            {
                Debug.Log("stageClear!1808");
                inputNum = 0;
                GamePlayAgents.solved = false;
                ResetGame();
            }

        }
        
        public void CheckPlayerTouchingUnmovable()
        {
            Collider2D[] collisions = Physics2D.OverlapBoxAll(player.transform.position, objectSize, 0f);
            touchingUnmovableR = false;
            touchingUnmovableL = false;
            touchingUnmovableU = false;
            touchingUnmovableD = false;
            foreach (Collider2D checkObj in collisions)
            {
                if (IsBlockTouchingR(player, checkObj.gameObject))
                {
                    if (IsObjectUnmovable(checkObj.gameObject))
                        touchingUnmovableR = true;
                    else if (Mathf.Abs(checkObj.gameObject.transform.position.y - player.transform.position.y) == 0.5f)
                        touchingUnmovableR = true;
                }
                if (IsBlockTouchingL(player, checkObj.gameObject))
                {
                    if (IsObjectUnmovable(checkObj.gameObject))
                        touchingUnmovableL = true;
                    else if (Mathf.Abs(checkObj.gameObject.transform.position.y - player.transform.position.y) == 0.5f)
                        touchingUnmovableL = true;
                }
                if (IsBlockTouchingU(player, checkObj.gameObject))
                {
                    if (IsObjectUnmovable(checkObj.gameObject) && checkObj.tag != stone)
                        touchingUnmovableU = true;
                    else if (Mathf.Abs(checkObj.gameObject.transform.position.x - player.transform.position.x) == 0.5f)
                        touchingUnmovableU = true;
                }
                if (IsBlockTouchingD(player, checkObj.gameObject))
                {
                    if (IsObjectUnmovable(checkObj.gameObject))
                        touchingUnmovableD = true;
                    else if (Mathf.Abs(checkObj.gameObject.transform.position.x - player.transform.position.x) == 0.5f)
                        touchingUnmovableD = true;
                }
            }
        }
        public void InitializeGame()
        {

            isRunninng = false;
            phase = 0;
            tManager = LoadedMap.LoadButtonClickedScript.transitionManager;
            Debug.Log("enteredFromMap = " + tManager.enteredFromMapLoad + "  " + LoadedMap.LoadButtonClickedScript.transitionManager.enteredFromMapLoad);
            cam = Camera.main;
            center = cam.transform.position;
            upperLeftCorner = new Vector3(center.x - StageFileLoader.stageSize[1] / 2, center.y + StageFileLoader.stageSize[0] / 2, 0);
            upperRightCorner = upperLeftCorner + x_Vector(StageFileLoader.stageSize[1] - 1);
            lowerLeftCorner = upperLeftCorner - y_Vector(StageFileLoader.stageSize[0] - 1);
            lowerRightCorner = upperRightCorner - y_Vector(StageFileLoader.stageSize[0] - 1);

            sr = gameObject.GetComponent<SpriteRenderer>();

            startPosition = StageFileLoader.playerPos;

            moveX = new Vector3(0.5f, 0, 0);
            moveY = new Vector3(0, 0.5f, 0);
            this.speedTime = 0.5f;

            this.transform.position = startPosition;
            this.animator = GetComponent<Animator>();
            gameManagerObj = GameObject.Find("GameManager");
            gameManager = gameManagerObj.GetComponent<GameManager>(); //スクリプトを取得
            Debug.Log("colSw = " + colCheckSwitch);

            blueCrystal = b_CrystalGenerator.blueCrystal;
            blueArea = b_areaGenerator.blueArea;
            GamePlayAgents.loadedNextStage = false;

            this.animator.SetBool("turnedL", false);
            this.animator.SetBool("turnedU", false);
            this.animator.SetBool("turnedR", false);
            this.animator.SetBool("turnedD", true);

            Collider2D[] collisions = Physics2D.OverlapBoxAll(this.transform.position, objectSize, 0f);
            if (collisions != null)
            {

                bool adjacentedF = false;
                foreach (Collider2D checkObj in collisions)
                {
                    Debug.Log("before2" + adjacentNameF);

                    Debug.Log("before3" + adjacentNameF);
                    if (!adjacentedF && IsBlockAdjacentFB(checkObj.gameObject))
                    {
                        Debug.Log("before4" + adjacentNameF);

                        if (IsBlockAdjacentFB(checkObj.gameObject) && checkObj.gameObject.transform.position.x == this.transform.position.x + 1)
                        {
                            touchingR = true;
                        }
                        else if (IsBlockAdjacentFB(checkObj.gameObject) && checkObj.gameObject.transform.position.x + 1 == this.transform.position.x)
                        {
                            touchingL = true;
                        }
                    }
                    else if (!IsBlockAdjacentFB(checkObj.gameObject))
                    {
                        Debug.Log("entry");
                        if (!adjacentedF)
                            adjacentNameF = "dummy"; //前後隣接ブロック無し
                        if (checkObj.gameObject.transform.position.y - 1 == this.transform.position.y
                                && Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.75f)
                            touchingU = true;
                        else if (checkObj.gameObject.transform.position.y + 1 == this.transform.position.y
                                && Mathf.Abs(checkObj.gameObject.transform.position.x - this.transform.position.x) <= 0.75f)
                        {
                            touchingD = true;
                            Debug.Log("b_adjD");
                            if (checkObj.gameObject.transform.position.x == this.transform.position.x)
                            {
                                adjacentObjectD = checkObj.gameObject;
                                Debug.Log("adjD");
                            }
                        }
                        else if (checkObj.gameObject.transform.position.x == this.transform.position.x + 1
                                && Mathf.Abs(checkObj.gameObject.transform.position.y - this.transform.position.y) == 0.5f)
                            touchingR = true;

                        touchingL = true;
                    }
                    Debug.Log("after" + adjacentNameF);
                }
            }
            gameManager.CallInoperable(wait / 8);

        }

    }
}