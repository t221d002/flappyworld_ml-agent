using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class WallGenerator : MonoBehaviour
    {
        public GameObject wallPrefab;
        Camera cam;
        public static List<GameObject> wallList = new List<GameObject>();

        void Start()
        {

            int rowNum = StageFileLoader.stageSize[0];
            int columnNum = StageFileLoader.stageSize[1];
            GameObject[] goRowUp = new GameObject[rowNum];
            Debug.Log("upperLeft = " + PlayerController.upperLeftCorner);

            for (int i = 0; i < rowNum; i++)
            {
                goRowUp[i] = Instantiate(wallPrefab);
                goRowUp[i].name = "wallRowUp" + i;
                goRowUp[i].transform.position = PlayerController.upperLeftCorner + PlayerController.x_Vector(i);
                Vector3 pos = goRowUp[i].transform.position;
                pos.z = -1;
                goRowUp[i].transform.position = pos;
            }

            GameObject[] goRowDown = new GameObject[rowNum];

            bool IsB_Area_halfExistRowDown = false;
            bool IsB_Area_ExistRowDown = false;
            int existingX_pos = -1;
            if ((StageFileLoader.blueArea_halfPos.y + 0.25 == PlayerController.lowerLeftCorner.y
                || StageFileLoader.blueArea_halfPos.y - 0.25 == PlayerController.lowerLeftCorner.y) && StageFileLoader.blueArea_halfPos.z == 1)
            {
                IsB_Area_halfExistRowDown = true;
                existingX_pos = (int)StageFileLoader.blueArea_halfPos.x;
            }
            else if (StageFileLoader.blueAreaPos.y == PlayerController.lowerLeftCorner.y && StageFileLoader.blueAreaPos.z == 1)
            {
                IsB_Area_ExistRowDown = true;
                existingX_pos = (int)StageFileLoader.blueArea_halfPos.x;
            }

            Debug.Log("blueAreaHalf32 = " + StageFileLoader.blueArea_halfPos);

            for (int i = 0; i < rowNum; i++)
            {
                if (((existingX_pos != -1 && (IsB_Area_ExistRowDown || IsB_Area_halfExistRowDown)
                    && i + PlayerController.lowerLeftCorner.x != existingX_pos)) || existingX_pos == -1)
                {//最下層の壁ブロックのｙ座標に青エリアが含まれているかどうか確認
                    goRowDown[i] = Instantiate(wallPrefab);
                    goRowDown[i].name = "wallRowDown" + i;
                    goRowDown[i].transform.position = PlayerController.lowerLeftCorner + PlayerController.x_Vector(i);
                    Vector3 pos = goRowDown[i].transform.position;
                    pos.z = -1;
                    goRowDown[i].transform.position = pos;
                }
                else
                    Debug.Log("blueAreaHalf43 = " + StageFileLoader.blueArea_halfPos + "exstingX = " + existingX_pos);
            }

            GameObject[] goColumnLeft = new GameObject[columnNum - 2];//rowの時点で縦の壁ブロックを２つ分生成しているから-2する
            for (int j = 0; j < columnNum - 2; j++)
            {
                goColumnLeft[j] = Instantiate(wallPrefab);
                goColumnLeft[j].name = "wallColumnLeft" + j;
                goColumnLeft[j].transform.position = PlayerController.upperLeftCorner - PlayerController.y_Vector(1 + j);
                Vector3 pos = goColumnLeft[j].transform.position;
                pos.z = -1;
                goColumnLeft[j].transform.position = pos;

            }

            GameObject[] goColumnRight = new GameObject[columnNum - 2];//rowの時点で縦の壁ブロックを２つ分生成しているから-2する
            for (int j = 0; j < columnNum - 2; j++)
            {
                goColumnRight[j] = Instantiate(wallPrefab);
                goColumnRight[j].name = "wallColumnRight" + j;
                goColumnRight[j].transform.position = PlayerController.upperRightCorner - PlayerController.y_Vector(1 + j);
                Vector3 pos = goColumnRight[j].transform.position;
                pos.z = -1;
                goColumnRight[j].transform.position = pos;
            }

            GameObject[] exWall = new GameObject[StageFileLoader.exWallPosList.Count];
            var exWallPosList = StageFileLoader.exWallPosList;
            foreach (var exWallPos in exWallPosList)
            {
                Debug.Log("exWallPos = " + exWallPos);
            }
            for (int i = 0; i < StageFileLoader.exWallPosList.Count; i++)
            {
                exWall[i] = Instantiate(wallPrefab);
                exWall[i].name = "exWall" + i;
                exWall[i].transform.position = exWallPosList[i];
                Vector3 pos = exWall[i].transform.position;
                pos.z = -1;
                exWall[i].transform.position = pos;
            }

            wallList.AddRange(goRowUp);
            wallList.AddRange(goRowDown);
            wallList.AddRange(goColumnRight);
            wallList.AddRange(goColumnLeft);
            wallList.AddRange(exWall);
        }
    }
}
