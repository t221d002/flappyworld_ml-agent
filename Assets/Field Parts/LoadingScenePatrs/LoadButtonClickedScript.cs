using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LoadedData
{
    public class LoadButtonClickedScript : MonoBehaviour
    {
        public void OnClickLoadButton()
        {
            bool readError = IsLoadedDataContainsError(DisplayLoadedData.GetSelectedDataNameFirst());

            if (readError)
                DisplayLoadedData.ErrorDialogContainer.SetActive(true);
            else
            {
                SceneManager.LoadScene("PlayLoadedStageScene");
            }
        }
        public static bool IsLoadedDataContainsError(string dataName)
        {
            string folderPass = DisplayLoadedData.folderPass;
            if (folderPass.Contains("/"))
                StageFileLoader.ReadStageDataFile(DisplayLoadedData.folderPass + "/" + dataName + ".txt");
            else
                StageFileLoader.ReadStageDataFile(DisplayLoadedData.folderPass + "\\" + dataName + ".txt");

            return StageFileLoader.readError;
        }
    }
}