using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Unity.VisualScripting;
using System.Linq.Expressions;

namespace LoadedData
{
    public class StageFileLoader
    {
        static string surroundingWallPattern = "@+";
        public static int[] stageSize = new int[2];//行、列の順に格納
        public static Vector3 playerPos;
        public static Vector3 blueCrystalPos;
        public static Vector3 blueAreaPos;
        public static Vector3 blueArea_halfPos;
        public static List<Vector3> stonePosList;
        public static List<Vector3> metalPosList;
        public static List<Vector3> wall_halfPosList;
        public static List<Vector3> exWallPosList;
        static public bool readError = false;
        static Vector3 center = new Vector3(0, 5, -10);//PlayLoadedScene側のメインカメラの座標
        static Vector3 upperLeftCorner;

        public static void ReadStageDataFile(string fileNamePass)
        {
            stonePosList = new List<Vector3>();
            wall_halfPosList = new List<Vector3>();
            exWallPosList = new List<Vector3>();
            blueAreaPos = new Vector3();
            blueArea_halfPos = new Vector3();
            metalPosList = new List<Vector3>();

            List<string> textsLineList = new List<string>();
            readError = false;

            try
            {
                using (StreamReader sr = new StreamReader(fileNamePass, Encoding.GetEncoding("utf-8")))
                {
                    while (0 <= sr.Peek())
                    {
                        textsLineList.Add(sr.ReadLine());
                    }
                }
            }
            catch (Exception ex)
            {
                readError = true;
            }
            if (!readError)
            {
                if (CheckStageSize(textsLineList))
                {
                    int stageSizeRow = stageSize[0];
                    int stageSizeColumn = stageSize[1];
                    upperLeftCorner = new Vector3(center.x - stageSize[1] / 2, center.y + stageSize[0] / 2, 0);
                    int textLineIndex = 0;
                    foreach (string lineText in textsLineList)
                    {
                        if (lineText.Length > 1)
                        {
                            if (textsLineList.IndexOf(lineText) > 3)
                            {
                                Debug.Log("lineText = " + lineText + "index = " + (textsLineList.IndexOf(lineText) - 3));
                                for (int lineIndex = 1; lineIndex < stageSizeColumn - 1; lineIndex++)
                                {
                                    switch (lineText[lineIndex])//半ブロックは大文字が上付き、小文字が下付きで区別
                                    {
                                        case '@':
                                            exWallPosList.Add(TextVectorPosition(lineIndex, textLineIndex - 3));
                                            Debug.Log("x = " + lineIndex + "  y = " + textLineIndex);
                                            break;
                                        case 'P':
                                            playerPos = TextVectorPosition(lineIndex, textLineIndex - 3);
                                            Debug.Log("playerPos = " + playerPos + "  textsLineListIndex = " + textsLineList.IndexOf(lineText) + "  upperLeft = " + upperLeftCorner);
                                            break;
                                        case 'B':
                                            blueCrystalPos = TextVectorPosition(lineIndex, textLineIndex - 3);
                                            break;
                                        case 'G':
                                            blueAreaPos = TextVectorPosition(lineIndex, textLineIndex - 3);
                                            break;
                                        case 'A':
                                            blueArea_halfPos = TextVectorPosition(lineIndex, textLineIndex - 3) + halfUpToY();
                                            break;
                                        case 'a':
                                            blueArea_halfPos = TextVectorPosition(lineIndex, textLineIndex - 3) + halfDownToY();
                                            break;
                                        case 'H':
                                            wall_halfPosList.Add(TextVectorPosition(lineIndex, textLineIndex - 3) + halfUpToY());
                                            Debug.Log("96Loader   " + "x = " + lineIndex + "  y = " + textLineIndex);
                                            break;
                                        case 'h':
                                            wall_halfPosList.Add(TextVectorPosition(lineIndex, textLineIndex - 3) + halfDownToY());
                                            break;
                                        case 'O':
                                            stonePosList.Add(TextVectorPosition(lineIndex, textLineIndex - 3));
                                            break;
                                        case 'M':
                                            metalPosList.Add(TextVectorPosition(lineIndex, textLineIndex - 3));
                                            break;
                                    }
                                }
                            }
                        }
                        textLineIndex++;
                    }
                }
                else
                {
                    Debug.Log("mismatched size!");
                    readError = true;
                }
            }
        }

        //テキストデータのシンボル(@)の盤面サイズがStage-Sizeの数字を満たすかどうか判定
        static bool CheckStageSize(List<string> textsLineList)
        {
            int stageSizeRow = 0;
            int stageSizeColumn = 0;
            int tentativeStageSize_row = 0;
            int tentativeStageSize_column = 0;
            if (textsLineList.Count >= 4 && textsLineList[1].Contains("stage-Size"))
            {
                string[] sizeDiscribedText = textsLineList[1].Split('_');
                if (sizeDiscribedText.Length == 3)
                {
                    stageSizeRow = int.Parse(sizeDiscribedText[1]);
                    stageSizeColumn = int.Parse(sizeDiscribedText[2]);
                }
                else
                {
                    readError = true;
                }
                foreach (string lineText in textsLineList)
                {
                    if (textsLineList.IndexOf(lineText) == 3 && lineText.Contains("@"))
                    {
                        tentativeStageSize_column = lineText.Length;
                        tentativeStageSize_row++;
                        if (!Regex.IsMatch(lineText, surroundingWallPattern))
                        {
                            Console.WriteLine("errorData");
                            return false;
                        }
                    }
                    else if (textsLineList.IndexOf(lineText) > 3 && lineText.Contains("@"))
                    {
                        if (lineText.Length > stageSizeColumn)
                            return false;
                        if (lineText[0] == '@' && lineText[stageSizeColumn - 1] == '@')
                            tentativeStageSize_row++;
                        else
                            return false;
                        if (tentativeStageSize_row > stageSizeRow || lineText.Length > stageSizeColumn)
                            return false;
                    }
                }
                if (tentativeStageSize_row == stageSizeRow && tentativeStageSize_column == stageSizeColumn)
                {
                    stageSize[0] = stageSizeRow;
                    stageSize[1] = stageSizeColumn;
                    return true;
                }
                else
                    return false;
            }
            else
            {
                Console.WriteLine("stage size data is not contained!");
                readError = true;
                return false;
            }
        }

        public static string[] ReadFolderFiles(string folderPass)
        {
            string[] fileNames;
            try
            {
                return fileNames = Directory.GetFiles(@folderPass, "*.txt");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Debug.Log("NodataLoaded");
                return null;
            }
        }

        static Vector3 TextVectorPosition(int x, int y)
        {
            Debug.Log("upperLeft = " + upperLeftCorner);
            return upperLeftCorner + PlayerController.x_Vector(x) - PlayerController.y_Vector(y) + new Vector3(0, 0, 1);//初期値と区別するためz軸に1を追加
        }

        static Vector3 halfUpToY()
        {
            return new Vector3(0, 0.25f, 0);
        }

        static Vector3 halfDownToY()
        {
            return new Vector3(0, -0.25f, 0);
        }
    }
}