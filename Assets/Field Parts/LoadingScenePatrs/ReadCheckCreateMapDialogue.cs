﻿using LoadedData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class ReadCheckCreateMapDialogue : MonoBehaviour
    {
        public enum DialogResult
        {
            OK,
            NO,
        }

        // ダイアログが操作されたときに発生するイベント
        public Action<DialogResult> FixDialog { get; set; }

        // OKボタンが押されたとき
        public void OnOk()
        {
            this.FixDialog?.Invoke(DialogResult.OK);
            bool readError = false;

            for (int n = 0; n < DisplayLoadedData.GetSelectedDataNameLength(); n++)
            {
                readError = LoadButtonClickedScript.IsLoadedDataContainsError(DisplayLoadedData.GetSelectedDataName(n));
            }

            DisplayLoadedData.CheckCreateMapDialogContainer.SetActive(false);
            if (readError)
            {
                DisplayLoadedData.ErrorDialogContainer.SetActive(true);
            }
            else
            {
                DisplayLoadedData.CheckArrangeOrderStageDialogueContainer.SetActive(true);
            }
        }
        public void OnNo()
        {
            this.FixDialog?.Invoke(DialogResult.NO);
            DisplayLoadedData.CheckCreateMapDialogContainer.SetActive(false);
        }

    }
}
