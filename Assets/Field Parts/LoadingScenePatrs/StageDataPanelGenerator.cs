using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageDataPanelGenerator : MonoBehaviour
{
    public static GameObject StageDataPanelPrefab;

    public static void GenerateStageDataPanel(string fileName, GameObject instanceObj)
    {
        instanceObj = Instantiate(StageDataPanelPrefab);
    }
}