using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LoadedData
{
    public class CreateMapButtonClickedScript : MonoBehaviour
    {

        public void OnClickCreateMapButton()
        {
            DisplayLoadedData.CheckCreateMapDialogContainer.SetActive(true);
        }
    }
}
