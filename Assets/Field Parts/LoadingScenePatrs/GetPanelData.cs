using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GetPanelData : MonoBehaviour
{
    public static string panelName;

    void Start()
    {
        panelName = name;
        GameObject panelObj = GameObject.Find(panelName);
        Debug.Log(panelName + "  position" + panelObj.GetComponent<BoxCollider2D>().size);
    }

    public static string GetPanelName()
    {
        return panelName;
    }
}
