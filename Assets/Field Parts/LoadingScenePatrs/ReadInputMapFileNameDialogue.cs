using LoadedData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadInputMapFileNameDialogue : MonoBehaviour
{
    public enum DialogResult
    {
        Cancel,
        OK,
    }

    // ダイアログが操作されたときに発生するイベント
    public Action<DialogResult> FixDialog { get; set; }

    public void OnClose()
    {
        this.FixDialog?.Invoke(DialogResult.Cancel);
        DisplayLoadedData.InputMapFileNameDialogueContainer.SetActive(false);

    }
    public void OnOK()
    {
        this.FixDialog?.Invoke(DialogResult.OK);
        CreateMap.CreateMapFile();
        DisplayLoadedData.InputMapFileNameDialogueContainer.SetActive(false);
    }

}
