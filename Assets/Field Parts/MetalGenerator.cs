using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalGenerator : MonoBehaviour
{
    public GameObject metalPrefab;

    void Start()
    {
        GameObject go = Instantiate(metalPrefab);
        go.transform.position = new Vector3(-2, 0, 0);
        go.name = "Metal1";
    }

}
