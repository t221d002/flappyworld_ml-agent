using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : MonoBehaviour
{
    public GameObject wallPrefab;

    void Start()
    {
        int rowNum = 11;
        int columnNum = 11;
        GameObject[] goRowUp = new GameObject[rowNum];

        for (int i = 0; i < rowNum; i++)
        {
            goRowUp[i] = Instantiate(wallPrefab);
            goRowUp[i].name = "wallRowUp" + i;
            goRowUp[i].transform.position = new Vector3(-6 + i, 9, 0);
        }

        GameObject[] goRowDown = new GameObject[rowNum];
        for (int i = 0; i < rowNum; i++)
        {
            goRowDown[i] = Instantiate(wallPrefab);
            goRowDown[i].name = "wallRowDown" + i;
            goRowDown[i].transform.position = new Vector3(-6 + i, 9 - (columnNum - 1), 0);
        }

        GameObject[] goColumnLeft = new GameObject[columnNum - 2];//rowの時点で縦の壁ブロックを２つ分生成しているから-2する
        for (int j = 0; j < columnNum - 2; j++)
        {
            goColumnLeft[j] = Instantiate(wallPrefab);
            goColumnLeft[j].name = "wallColumnLeft" + j;
            goColumnLeft[j].transform.position = new Vector3(-6, 9 - (1 + j), 0);
        }

        GameObject[] goColumnRight = new GameObject[columnNum - 2];//rowの時点で縦の壁ブロックを２つ分生成しているから-2する
        for (int j = 0; j < columnNum - 2; j++)
        {
            goColumnRight[j] = Instantiate(wallPrefab);
            goColumnRight[j].name = "wallColumnRight" + j;
            goColumnRight[j].transform.position = new Vector3(-6 + (columnNum - 1), 9 - (1 + j), 0);
        }

        GameObject goEx1 = Instantiate(wallPrefab);
        goEx1.name = "wallEx1";
        goEx1.transform.position = new Vector3(-1, 4, 0);

        GameObject goEx2 = Instantiate(wallPrefab);
        goEx2.name = "wallEx2";
        goEx2.transform.position = new Vector3(0, 7, 0);

        //GameObject goEx3 = Instantiate(wallPrefab);
        // goEx3.name = "wallEx3";
        //goEx3.transform.position = new Vector3(-1, 7, 0);
    }
}
