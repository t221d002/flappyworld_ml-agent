﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface IPlayer
{
    int beongings {get; set;}//キノコの所持数
    int posX { get; set; }//現在位置
    int posY { get; set; }//現在位置
    void SetPos(int x, int y);//初期位置を指定
    void Move();//操作キャラを移動
    void Push();//オブジェクトを押す
    void Crash(); 
    void Restart();
    void ThrowMush();
    void GetMush();
    void Die();
}

public class Player : IPlayer {

    public int posX { get; set; }
    public int posY { get; set; }
    public int beongings { get; set; }

    public void SetPos(int x, int y)
    {
        posX = x; posY = y;
    }

    public void Move()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            posX += 1;
        else if (Input.GetKey(KeyCode.LeftArrow))
            posX -= 1;
        else if (Input.GetKey(KeyCode.UpArrow))
            posY += 1;
        else if (Input.GetKey(KeyCode.DownArrow))
            posY -= 1;
    }

    public void Push()
    {
        
    }

    public void Crash()
    {

    }

    public void Restart()
    {

    }

    public void ThrowMush()
    {

    }

    public void GetMush()
    {

    }

    public void Die() 
    {
        
    }
}
