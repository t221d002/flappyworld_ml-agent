using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespawner : MonoBehaviour
{
    public GameObject playerPrefab;
    void Update()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null)
        {
            GameObject newPlayerObj = Instantiate(playerPrefab);
            newPlayerObj.name = "player";
        }
    }
}
