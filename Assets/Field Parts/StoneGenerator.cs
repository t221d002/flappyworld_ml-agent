
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneGenerator : MonoBehaviour
{
    public GameObject stonePrefab;

    void Start()
    {
        GameObject go = Instantiate(stonePrefab);
        GameObject go2 = Instantiate(stonePrefab);
        //GameObject go3 = Instantiate(stonePrefab);
        GameObject go4 = Instantiate(stonePrefab);
        GameObject go5 = Instantiate(stonePrefab);

        go.transform.position = new Vector3(-2, 6, 0);
        go2.transform.position = new Vector3(-2, 7, 0);
        // go3.transform.position = new Vector3(0, 8, 0);
        go4.transform.position = new Vector3(1.5f, 4, 0);
        go5.transform.position = new Vector3(-1.5f, 5, 0);

        go.name = "Stone1";
        go2.name = "Stone2";
        //go3.name = "Stone3";
        go4.name = "Stone4";
        go5.name = "Stone5";
    }
}
