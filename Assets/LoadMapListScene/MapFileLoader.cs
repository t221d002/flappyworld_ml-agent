using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Unity.VisualScripting;
using System.Linq.Expressions;

namespace LoadedMap
{
    public class MapFileLoader
    {
        static public bool readError = false;

        public static void ReadMapDataFile(string fileNamePass)
        {
            List<string> textsLineList = new List<string>();
            readError = false;

            try
            {
                using (StreamReader sr = new StreamReader(fileNamePass, Encoding.GetEncoding("utf-8")))
                {
                    while (0 <= sr.Peek())
                    {
                        textsLineList.Add(sr.ReadLine());
                    }
                }
            }
            catch (Exception ex)
            {
                readError = true;
            }
        }

        public static string[] ReadFolderFiles(string folderPass)
        {
            string[] fileNames;
            try
            {
                return fileNames = Directory.GetFiles(@folderPass, "*.map");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Debug.Log("NodataLoaded");
                return null;
            }
        }

        public static List<string> ReadStageNamesInMapFile(string folderPass, string mapName)
        {
            string mapPass = folderPass + "/" + mapName + ".map";
            List<string> stageNameListInMap = new List<string>();

            try
            {
                using (StreamReader sr = new StreamReader(mapPass, Encoding.GetEncoding("utf-8")))
                {
                    while (0 <= sr.Peek())
                    {
                        stageNameListInMap.Add(sr.ReadLine());
                    }
                }
            }
            catch (Exception ex) { }

            LoadButtonClickedScript.transitionManager.MapStageList = stageNameListInMap;
            return stageNameListInMap;
        }
    }
}