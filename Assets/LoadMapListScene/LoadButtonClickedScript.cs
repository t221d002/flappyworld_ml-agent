using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LoadedMap
{
    public class LoadButtonClickedScript : MonoBehaviour
    {
        public static List<string> stageNameList;
        public static string folderPass;
        public static TransitionManager transitionManager = new TransitionManager();

        void Start()
        {
            folderPass = DisplayLoadedData.folderPass;
            Rigidbody rBody = GetComponent<Rigidbody>();
        }

        public void OnClickLoadButton()
        {

            string fileNamePass = DisplayLoadedData.folderPass + "/" + DisplayLoadedData.GetSelectedDataNameFirst() + ".map";
            List<string> textsLineList = new List<string>();

            try
            {
                using (StreamReader sr = new StreamReader(fileNamePass, Encoding.GetEncoding("utf-8")))
                {
                    while (0 <= sr.Peek())
                    {
                        textsLineList.Add(sr.ReadLine());
                    }
                }
            }
            catch { }

            folderPass = DisplayLoadedData.folderPass;
            bool readError = IsLoadedDataContainsError(DisplayLoadedData.GetSelectedDataNameFirst());

            if (readError)
                DisplayLoadedData.ErrorDialogContainer.SetActive(true);
            else
            {
                LoadedData.StageFileLoader.ReadStageDataFile("Assets/Field Parts/LoadingScenePatrs/StageTextFiles"
                + "/" + stageNameList[0] + ".txt");

                transitionManager.MapStageList = textsLineList;
                transitionManager.enteredFromMapLoad = true;
                transitionManager.mapStageCount = 0;

                SceneManager.LoadScene("PlayLoadedStageScene");
            }
        }
        public static bool IsLoadedDataContainsError(string dataName)
        {
            List<string> stageNameListInMap = MapFileLoader.ReadStageNamesInMapFile(folderPass, dataName);
            stageNameList = stageNameListInMap;
            bool isContainsError = false;
            string mapFolderPass = "Assets/Field Parts/LoadingScenePatrs/StageTextFiles";

            foreach (string stageName in stageNameListInMap)
            {
                Debug.Log("stageName61 = " + stageName);
                if (mapFolderPass.Contains("/"))
                    isContainsError = !IsTheFileExist(mapFolderPass + "/" + stageName + ".txt");
                else
                    isContainsError = !IsTheFileExist(mapFolderPass + "\\" + stageName + ".txt");
            }

            return isContainsError;
        }
        static bool IsTheFileExist(string filePass)
        {
            if (System.IO.File.Exists(filePass))
                return true;
            else
                return false;
        }
    }
}