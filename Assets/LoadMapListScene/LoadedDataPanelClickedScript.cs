using Microsoft.VisualBasic;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace LoadedMap
{
    public class LoadedDataPanelClickedScript : MonoBehaviour
    {
        public static string panelName;
        void Start()
        {
            Debug.Log(name);
            panelName = name;
        }

        public static void OnClickDataPanel()
        {
            GameObject panel = GetClickedPanel("ListData");

            var outline = panel.GetComponent<Outline>();

            if (panel != null)
            {
                Debug.Log(panel);
                outline.enabled = !outline.enabled;
                if (outline.enabled)
                    DisplayLoadedData.AddSelectedDataName(panel.name);
                if (!outline.enabled)
                    DisplayLoadedData.RemoveSelectedDataName(panel.name);
            }
            DisplayLoadedData.ShowSelectedDataNameList();

            if (DisplayLoadedData.GetSelectedDataNameLength() == 1)
            {
                DisplayLoadedData.LoadButton.SetActive(true);
            }
            else if (DisplayLoadedData.GetSelectedDataNameLength() > 1)
            {
                DisplayLoadedData.LoadButton.SetActive(false);
            }
            else
            {
                DisplayLoadedData.LoadButton.SetActive(false);
            }
        }

        public static GameObject GetClickedPanel(string tagName)
        {
            PointerEventData pointer = new PointerEventData(EventSystem.current);
            pointer = new PointerEventData(EventSystem.current);

            GameObject panel = null;
            Vector3 mousePosition = Input.mousePosition;
            Vector2 worldPos = Camera.main.ScreenToWorldPoint(mousePosition);

            Collider2D collition2d = Physics2D.OverlapPoint(worldPos);

            List<RaycastResult> results = new List<RaycastResult>();
            pointer.position = mousePosition;
            EventSystem.current.RaycastAll(pointer, results);

            foreach (RaycastResult target in results)
            {
                if (target.gameObject.tag == tagName)
                    panel = target.gameObject;
            }
            return panel;
        }
    }

}