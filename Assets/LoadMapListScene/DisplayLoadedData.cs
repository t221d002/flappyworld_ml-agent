using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro; //TextMeshProを扱う際に必要

namespace LoadedMap
{
    public class DisplayLoadedData : MonoBehaviour
    {
        string[] fileNames;
        public static string folderPass;
        private static List<string> selectedPanelNameList;
        public static GameObject LoadButton;
        public static GameObject ErrorDialogContainer;

        public GameObject StageDataPanelPrefab;

        void Start()
        {
            selectedPanelNameList = new List<string>();
            folderPass = "Assets/Field Parts/LoadingScenePatrs/MapFiles";
            fileNames = MapFileLoader.ReadFolderFiles(folderPass);
            foreach (string fileName in fileNames)
                Debug.Log(fileName);
            LoadButton = GameObject.FindWithTag("Load");
            LoadButton.SetActive(false);
            ErrorDialogContainer = GameObject.FindWithTag("Error");
            ErrorDialogContainer.SetActive(false);

            if (fileNames != null)
            {
                GameObject[] panelObj = new GameObject[fileNames.Length];
                GameObject dataListPanel = GameObject.FindWithTag("List");
                TextMeshProUGUI panelText;

                for (int fileNamesIndex = 0; fileNamesIndex < fileNames.Length; fileNamesIndex++)
                {
                    panelObj[fileNamesIndex] = Instantiate(StageDataPanelPrefab);
                    panelObj[fileNamesIndex].transform.SetParent(dataListPanel.transform, false);
                    var panelButton = panelObj[fileNamesIndex].GetComponent<Button>();
                    panelButton.onClick.AddListener((LoadedDataPanelClickedScript.OnClickDataPanel));
                    panelText = panelObj[fileNamesIndex].GetComponentInChildren<TextMeshProUGUI>();
                    Debug.Log(panelText);

                    string replacedNameFirst = fileNames[fileNamesIndex].Replace(folderPass + "/", "");
                    replacedNameFirst = replacedNameFirst.Replace(folderPass + "\\", "");//WindowsとLinuxはパスにおけるスラッシュの向きが違う

                    string replacedNameSecond = replacedNameFirst.Replace("\\", "");
                    string panelTextName = replacedNameSecond.Replace(".map", "");

                    panelText.text = panelTextName;
                    Debug.Log("panelName = " + panelTextName);
                    panelObj[fileNamesIndex].name = panelTextName;
                }
            }
        }

        void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;//ゲームプレイ終了
#else
                Application.Quit();//ゲームプレイ終了
#endif
            }
        }

        /*static string GetCurrentDirectory()
        {
            var flag = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;
            var asm = Assembly.Load("UnityEditor.dll");
            var typeProjectBrowser = asm.GetType("UnityEditor.ProjectBrowser");
            var projectBrowserWindow = EditorWindow.GetWindow(typeProjectBrowser);
            return (string)typeProjectBrowser.GetMethod("GetActiveFolderPath", flag).Invoke(projectBrowserWindow, null);
        }
        */

        public static void AddSelectedDataName(string name)
        {
            selectedPanelNameList.Add(name);
        }

        public static void RemoveSelectedDataName(string name)
        {
            selectedPanelNameList.Remove(name);
        }

        public static void ShowSelectedDataNameList()
        {
            foreach (string selectedPanelName in selectedPanelNameList)
            {
                Debug.Log(selectedPanelName);
            }
        }

        public static int GetSelectedDataNameLength()
        {
            return selectedPanelNameList.Count;
        }

        public static string GetSelectedDataNameFirst()
        {
            return selectedPanelNameList[0];
        }

        public static string GetSelectedDataName(int index)
        {
            return selectedPanelNameList[index];
        }


    }
}
